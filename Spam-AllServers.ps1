### This script runs a given set of commands against all servers who will listen. This method works for intel collection, software distribution, and proxy actions. The methodolgy should be more efficient when using long running cmd blocks.
### Set your command in this Script Block. Returning strings works for me. 
$cmd = {

	return (Get-ChildItem -Path HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall, HKLM:\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall | Get-ItemProperty) -join ";;;"

}

###
### Place any post processing you wish to have done on the results here. $results will be a list of dicts with the server name as the key and the returned data as the value.
### Consider the body of what's in this function an example. This example is used to parse returned registry property-value pair information, and was specifically written for processing Installed Software lists. 
### You can even get creative and call other applications (eg Perl scripts) or APIs to process, log, or enrich the data collected from all the servers.
###
function PostProcessing($results) {

	### YOUR CODE STARTS HERE ###

	# converts ';;;' delimited installed software list as a string into an array of property-value pair objects similar to dicts per server
	$ret = @{}
	$i = 0; $ct = $results.keys.count;
	$results.keys | %{
		$objlist = @()
		$i += 1
		$server = $_
		write-progress -Activity "Post Processing" -Status "Processing result $i / $ct --- $_" -PercentComplete ($i / $ct * 100.0)
		try {
			$lines = $results["$server"] -split ";;;"

			$lines | %{
				$line = $_;
				$p = @{}
				try {
					# strip out invalid charachters
					$temp = $line.TrimStart("@{").Trimend("}").replace("; ","`n").replace("\","\\")

					# join comment/invalid lines
					$final = ""
					$temp.split("`n") | %{
						if ($_ -match "=") {
							$final += "`n"
						}
						$final += $_
					}
					$line = $final.substring(1) # pull out the leading newline
					try {
						$p = ConvertFrom-StringData $line
						$objlist += $p
					} catch {
						Write-Error "Unable to convert string data to an object for server '$server'.`n`n`nError: $_"
						$objlist += new-object psobject -property @{RawData = "$line"; ErrorMessage = "$_"}
					}
				} catch {
					Write-Error "Unable to prepare line data for conversion on server '$server'.`n`n`nError: $_"
					$objlist += new-object psobject -property @{RawData = "$line"; ErrorMessage = "$_"}
				}
			}
		} catch {
			Write-Error "Unable to split results by ';;;' on server '$server'.`n`n`nError: $_"
			$objlist += new-object psobject -property @{RawData = "$($results["$server"])"; ErrorMessage = "$_"}
		}
	
		$ret["$server"] = $objlist
	}
	return $ret

	### YOUR CODE ENDS HERE ###

}


### Spam Starts Here ###

# resolve dependancies. can auto correct if running as a local admin. untested. 
$admodule = Get-Module ActiveDirectory
if ($admodule -eq $null) {
	try {
		$admodule = Get-Module ActiveDirectory -ListAvailable
		if ($admodule -eq $null) {
			# must be a local admin for this to work. could attempt to escalate for this process. 
			$ignoreme = Install-WindowsFeature -Name "RSAT-AD-PowerShell" -IncludeAllSubFeature -ErrorAction Stop -Confirm:$false
			$admodule = Import-Module ActiveDirectory
			if ($admodule -eq $null -and $ignoreme -ne $null) {
				Write-Error "I believe RSAT is installed, but I was unable to import the ActiveDirectory Module. Manually resolve this then try again."
				return $null
			} else {
				Write-Verbose "RSAT Successfully installed and ActiveDirectory Module imported"
			}
		} else {
			$admodule = Import-Module ActiveDirectory
			Write-Verbose "ActiveDirectory Module imported"
		}
	} catch {
		Write-Error "The ActiveDirectory module from the RSAT Feature was not available and could not be installed. Resolve this by installing RSAT from an elevated command prompt: Install-WindowsFeature -Name 'RSAT-AD-PowerShell' -IncludeAllSubFeature -ErrorAction Stop -Confirm:$false"
		return $null
	}
}

# runas credentials. figure a way to add two and to consolidate code - default credential?
# add a mechanism to pass the desired list of OUs to run against, or a target list of computers. 
$mcreds = get-credential -message "Enter your Member Server Admin Credentials"
$dcreds = get-credential -message "Enter your Domain Controller Admin Credentials - be aware that these may "

$results = @{}
$errors = @{}
$sessions = @()
$jobs = @() 

Write-Host "Starting Up"

$memberlist = (Get-ADComputer -SearchBase 'OU=Domain Servers,dc=ad,dc=csub,dc=edu' -Filter *).dnshostname | sort-object -unique
$dclist = (Get-ADComputer -SearchBase 'OU=Domain Controllers,dc=ad,dc=csub,dc=edu' -Filter *).dnshostname | sort-object -unique

Write-Host "Got $($memberlist.count) member server(s) and $($dclist.count) DC(s)."

$i = 0
$mi = 0
$di = 0

$mct = $memberlist.count
$dct = $dclist.count
$tct = $mct + $dct

if ($mcreds -ne $null) {
	$memberlist | %{  
		$server = $_; $mi += 1; $i += 1 ; write-progress -Activity "Running against $mct Member Servers" -Status "$mi / $mct - $server" -PercentComplete ($i / $tct * 100.0) ;
		try { 
			$s = New-PSSession $server -Credential $mcreds -ErrorAction Stop 
		} catch { 
			$errors["$server"]="Unable to connect to $server : $_"; $s=$null; 
		} ; 
		if ($s) { try { 
			$jobs += Invoke-Command -Session $s -AsJob -JobName "$server" -ScriptBlock $cmd 
		} catch { 
			$errors["$server"]="Error on '$server' : $_"; 
		} ; $sessions += $s ;} 
	} ;
} else {
	Write-Verbose "Skipping member servers due to lack of credentials"
	$i += $mct;
}

# run against DCs. Be aware that access to these may be restricted. 
if ($dcreds -ne $null) {
	$dclist | %{ 
		$server = $_; $di += 1; $i += 1 ; write-progress -Activity "Running against $dct DCs" -Status "$di / $dct - $server" -PercentComplete ($i / $tct * 100.0) ; 
		try { 
			$s = New-PSSession $server -Credential $dcreds -ErrorAction Stop 
		} catch { 
			$errors["$server"]="Unable to connect to $server : $_"; $s=$null; 
		} ; 
		if ($s) { try { 
			$jobs += Invoke-Command -Session $s -AsJob -JobName $server -ScriptBlock $cmd  
		} catch { 
			$errors["$server"]="Error on $server : $_"; 
		} ; $sessions += $s; } 
	} ;
} else {
	Write-Verbose "Skipping DCs due to lack of credentials"
	$i += $dct
}

write-host "Done launching jobs. Awaiting results."

# gather results

if ($jobs.count -gt 0) {
	Write-Host "[.] Waiting for $((get-job | ?{$_.id -in $jobs.id -and $_.state -eq 'Running'}).count) / $($jobs.count) jobs to finish..."
	while ($((get-job | ?{$_.id -in $jobs.id -and $_.state -eq 'Running'}).count) -gt 0) {
		sleep 1
	}

	if ($((get-job | ?{$_.id -in $jobs.id -and $_.state -ne 'Completed'}).count) -gt 0) {
		Write-Host "[!] There was an issue with one or more jobs. The jobs variable has all the job details and the sessions variable has the current sessions. Here are the incomplete jobs:"
		get-job | ?{$_.id -in $jobs.id -and $_.state -ne 'Completed'}
		get-job | ?{$_.id -in $jobs.id -and $_.state -eq 'Completed'} |  %{ $server = $_.Name ; $ret = Receive-Job -AutoRemoveJob -Wait ; $results["$server"] = $ret ; } 
	} else {
		Write-Host "[.] All jobs completed successfully."
		get-job | ?{$_.id -in $jobs.id -and $_.state -eq 'Completed'} |  %{ $server = $_.Name ; $ret = Receive-Job $_.Id -AutoRemoveJob -Wait ; $results["$server"] = $ret ; }
	}
} else {
	Write-Host "[?] No jobs to await?"
}

# cleanup

$sessions | remove-pssession
$mcreds = $null
$dcreds = $null

# post processing

$processedResults = PostProcessing $results

Write-Host "Done. $($processedResults.count) processed results after receiving $($results.keys.count) raw results from $tct targeted hosts with $($errors.keys.count) errors."



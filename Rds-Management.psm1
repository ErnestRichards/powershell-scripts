$DefaultTenantName = "Wvd-CSUB"
$DefaultResourgeGroupName = "RG-Desktops-West"
$DefaultDeploymentUrl = "https://rdbroker.wvd.microsoft.com"
$DefaultTags=@{'Application'='Virtual Desktop';'Application Owner'='erichards1@csub.edu';'Chargeback ID'='BK001 D22310 660991';'CSUB Service'='Support Services';'Data Profile'='Level 3 - Internal';'Department'='ITS-IS';'Environment'='Development';'Service Owner'='fgorham@csub.edu';'Autoshutdown'='motuwethfrsasu,1800';'Autostartup'='motuwethfr,0700'}
$Location = 'westus'
$VnetName = 'csub-adfs'
$VnetResourceGroupName = 'CSUB-ADFS'
$LabSubnetName = 'SUB-Lab1-West'
$FacSubnetName = 'SUB-Fac1-West'
$StaffSubnetName = 'SUB-Staff1-West'
$DefaultVmSize = 'Standard_D2s_v3'
$DefaultSubscriptionId = '42b1bbd2-3677-431e-866d-a0ffa554937e'
$DefaultTenantId = 'ab5a62b4-a6bf-460e-8a86-dec0f8001704'
$DefaultKeyVaultName = 'Key-VmEncryption'
$DefaultLabImage = 'unilab-os_20200326-1200'
$DefaultDomainName = 'ad.csub.edu'
$DefaultOuPath = "OU=VDI,DC=ad,DC=csub,DC=edu"
$DefaultLocalAdminAccount = 'vm-lab2-west\vdiadmin'
$RdAgentUrl = "https://query.prod.cms.rt.microsoft.com/cms/api/am/binary/RWrmXv"
$RdAgentBlUrl = "https://query.prod.cms.rt.microsoft.com/cms/api/am/binary/RWrxrH"
$DefaultHostPoolName = "UniLab-Demo"
$global:AzureCredentials
$global:TenantCredentials
$global:DomainCredentials
$global:LocalCredentials
$global:UnusedSessionHostLog = @{}

[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

function Login-AzCsub ($Credentials = $global:AzureCredentials, $TenantId = $DefaultTenantId, $SubscriptionId = $DefaultSubscriptionId) {
	try {
		$ctx = Set-AzContext -TenantId $TenantId -SubscriptionId $SubscriptionId -ErrorAction SilentlyContinue
	} catch {
		Import-Module Az
		$ctx = Set-AzContext -TenantId $TenantId -SubscriptionId $SubscriptionId -ErrorAction SilentlyContinue
	}
	if (!$ctx) {
		if (!$Credentials) {
			$Credentials = Get-Credential -Message "Azure Admin Creds" -UserName "$Env:username@csub.onmicrosoft.com"
			$global:AzureCredentials = $Credentials
		}
		$ctx = Login-AzAccount -Credential $Credentials -TenantId $TenantId -SubscriptionId $SubscriptionId -ErrorAction SilentlyContinue
	}
	if (!$ctx) {
		throw 'Unable to login to Azure. Make sure you have installed and imported the Az Powershell module, and you are using good credentials.'
	}
	return $ctx
}

function Login-RdsCsub ($Credentials = $global:TenantCredentials, $TenantName = $DefaultTenantName, $DeploymentUrl = $DefaultDeploymentUrl) {
	if (!$Credentials) {
		$Credentials = Get-Credential -Message "RDS Tenant Admin Creds" -UserName "$Env:username@csub.onmicrosoft.com"
		$global:TenantCredentials = $Credentials
	}
	$RdsContext = Add-RdsAccount -DeploymentUrl $DeploymentUrl -Credential $Credentials
	if (!$RdsContext) {
		throw "No RDS context available for given account"
	}
	return $RdsContext
}

function Get-RdsHostPoolByVmName ($Name, $TenantName = "", $DeploymentUrl = "") {
	$hp
	if (!(Login-AzCsub)) {
		throw 'Unable to login to RDS'
	}
	foreach ($hp in (Get-RdsHostPool -TenantName $TenantName)) {
		try {
			$hosts = Get-RdsSessionHost -TenantName $TenantName -HostPoolName $hp.HostPoolName -ErrorAction SilentlyContinue
			if ($hosts) {
				$sh = $hosts | where {$_.SessionHostName -match "$Name"}
				if ($sh) {
					Write-Verbose "$sh"
					return $hp
				}
			}
		} catch {
			Write-Verbose "An error occurred on $hp : $_"
		}
	}
	return
}

function Do-AutoShutdown ($WaitTime = 90) {
	Write-Host "[.] Working"
	$dow = (get-date -Format "D").ToString().tolower()[0..1] -join ''
	$time = get-date -Format "HHmm"
	$HostPoolName = "CSUB-Desktops"
	$rgname = "RG-Desktops-West"
	$TenantName = $DefaultTenantName

	if (!(Login-AzCsub)) {
		throw 'Unable to login to AZ'
	}

	Write-Host "[.] Checking for $dow and $time in $rgname"

	$targets = get-azvm $rgname | where {'Autoshutdown' -in $_.tags.keys}
	$expired = $targets | where {$_.tags['Autoshutdown'].split(',')[0] -match $dow -and $_.tags['Autoshutdown'].split(',')[1] -le $time}

	if ($expired.Count -lt 1) {
		Write-Host "[i] No systems to shutdown. Done!"
		return $results
	} else {
		$ct = $expired.Count
		Write-Host "[.] There are $ct systems in $rgname due to shutdown"
	}

	$targets = @()
	foreach ($vm in $expired) {
		try {
			if ( ((get-azvm $vm.ResourceGroupName $vm.Name -status).statuses | where {$_.Code -match 'PowerState'}).Code -match 'running' ) {
				$targets += $vm
			}
		} catch {}
	}

	if ($targets.Count -lt 1) {
		Write-Host "[i] No systems to shutdown. Done!"
		return $results
	} else {
		$ct = $targets.Count
		Write-Host "[.] There are $ct systems in $rgname due to shutdown that ar currently running"
	}

	$tlist = $targets.name -join ', '

	read-host "About to notify any users on the following systems then shut them off after $WaitTime seconds: $tlist"

	foreach ($target in $targets) {
		$sessions = Get-RdsUserSession -TenantName $TenantName -HostPoolName $HostPoolName | where {($_.SessionHostName -replace '.ad.csub.edu', '') -eq $target}
		foreach ($session in $sessions) {
			$sessuser = $session.UserName
			Write-Host "[.] Notifying $sessuser in $HostPoolName of shutdown"
			Send-RdsUserSessionMessage $TenantName $HostPoolName -SessionHostName $target -SessionId $session.SessionId -MessageTitle "Shutdown Imminent" -MessageBody "This system will be shutdown in $WaitTime seconds. Please Save your Work."
		}
	}

	sleep $WaitTime

	$targets | %{
		$SB = {
			param($t,$tn,$hp)
			$sessions = Get-RdsUserSession -TenantName $tn -HostPoolName $hp | where {($_.SessionHostName -replace '.ad.csub.edu', '') -eq $t}
			foreach ($session in $sessions) {
				$sessuser = $session.UserName
				Write-Host "[.] Final notification sent for $sessuser in $hp of shutdown"
				Send-RdsUserSessionMessage $tn $hp -SessionHostName $t -SessionId $session.SessionId -MessageTitle "Shutting Down" -MessageBody "This system will now shutdown."
				sleep 10
				Disconnect-RdsUserSession $tn $hp -SessionHostName $t -SessionId $session.SessionId
			}
			sleep 2
			$t | Stop-AzVm -Force
		}

		$j = start-Job $SB -ArgumentList $_,$TenantName,$HostPoolName
	}

	Write-Host "[.] All shutdown tasks intiated"

	$i=0
	While (Get-Job -State "Running") {
		$i=$i+1
		if (($i % 2) -eq 0) {
			Write-Host "[.] Still awaiting tasks"
		}
		sleep 10
	}

	$results = get-job | receive-job

	Write-Host "[.] All shutdown tasks complete!"

	return $results
}

function Do-AutoStartup () {
	Write-Host "[.] Working"
	$dow = (get-date -Format "D").ToString().tolower()[0..1] -join ''
	$time = get-date -Format "HHmm"
	$HostPoolName = "CSUB-Desktops"
	$rgname = "RG-Desktops-West"
	$TenantName = $DefaultTenantName

	if (!(Login-AzCsub)) {
		throw 'Unable to login to AZ'
	}

	Write-Host "[.] Checking for $dow and $time in $rgname"

	$targets = get-azvm $rgname | where {'Autostartup' -in $_.tags.keys}
	$expired = $targets | where {$_.tags['Autostartup'].split(',')[0] -match $dow -and $_.tags['Autostartup'].split(',')[1] -le $time}

	if ($expired.Count -lt 1) {
		Write-Host "[i] No systems to startup. Done!"
		return $results
	} else {
		$ct = $expired.Count
		Write-Host "[.] There are $ct systems in $rgname due to startup"
	}

	$targets = @()
	foreach ($vm in $expired) {
		try {
			if ( ((get-azvm $vm.ResourceGroupName $vm.Name -status).statuses | where {$_.Code -match 'PowerState'}).Code -notmatch 'running' ) {
				$targets += $vm
			}
		} catch {}
	}

	if ($targets.Count -lt 1) {
		Write-Host "[i] No systems to startup. Done!"
		return $results
	} else {
		$ct = $targets.Count
		Write-Host "[.] There are $ct systems in $rgname due to startup that are not running"
	}

	$targets | %{
		$SB = {
			param($t)
			$t | Start-AzVm
		}

		$j = start-Job $SB -ArgumentList $_
	}

	Write-Host "[.] All startup tasks intiated"

	$i=0
	While (Get-Job -State "Running") {
		$i=$i+1

		if (($i % 2) -eq 0) {
			Write-Host "[.] Still awaiting tasks"
		}
		sleep 10
	}

	$results = get-job | receive-job

	Write-Host "[.] All startup tasks complete!"

	return $results
}

function Select-VmName ($Name) {
	$vmName = ""

    if (!$Name -or $Name.Length -lt 3) {
    	$vms = Get-AzVm | where {$_.Name -match "VM[0-9]+"}
    	$num = $vms.count
    	$vmName = "VM$num"
    	if ($vmName -in $vms) {
    		$num = $num + 1
	    	$vmName = "VM$num"
    		if ($vmName -in $vms) {
    			throw "Couldn't come up with a good name. Pass one next time."
    		}
    	}
    } else {
    	$vm = Get-AzVm | where {$_.Name -match "$Name"}
    	if ($vm) {
    		$vms = Get-AzVm | where {$_.Name -match "$Name[0-9]+"}
    		$num = $vms.count
    		$vmName = "$Name$num"
	    	if ($vmName -in $vms) {
	    		$num = $num + 1
		    	$vmName = "$Name$num"
	    		if ($vmName -in $vms) {
	    			throw "Couldn't come up with a good name. Pass one next time."
	    		}
	    	}
		} else {
    		$vmName = $Name
    		if ($vmName -notlike '*[0-9]+') {
    			$vmName += "0"
    		}
		}
    }
	return $vmName
}

function Start-WinRm () {
	Get-Service winrm | Set-Service -StartupType Automatic
	Get-Service winrm | Start-Service
}

function Set-TrustedHosts ($value) {
	try {
		$old = (Get-Item WSMAN:\localhost\Client\TrustedHosts -ErrorAction SilentlyContinue).Value
	} catch {
		Start-WinRm
		try {
			$old = (Get-Item WSMAN:\localhost\Client\TrustedHosts -ErrorAction SilentlyContinue).Value
		} catch {
			throw "Unable to get/set TrustedHosts"
		}
	}
	Set-Item WSMAN:\localhost\Client\TrustedHosts -Value "$value" -Force -Confirm:$false
	return $old
}

function Get-Creds () {
	if (!$global:DomainCredentials) {
		Write-Host "[.] Collecting Domain Credentials..."
		try {
			$global:DomainCredentials = Get-Credential -Message "Enter Active Directory Domain Credentials for binding" -UserName "$Env:username@csub.edu"
		} catch {
			$global:DomainCredentials = $null
		}
		if (!$global:DomainCredentials) {
			throw "I can't proceed without domain credentials"
		}
	}

	if (!$global:AzureCredentials) {
		Write-Host "[.] Collecting Azure/Tenant Credentials..."
		try {
			$global:AzureCredentials = Get-Credential -Message "Enter Azure Active Directory Domain/WVD Tenant Credentials" -UserName "$Env:username@csub.onmicrosoft.com"
		} catch {
			$global:AzureCredentials = $null
		}
		if (!$global:AzureCredentials) {
			throw "I can't proceed without Azure/Tenant credentials"
		}
	}

	if (!$global:LocalCredentials) {
		Write-Host "[.] Collecting Local Credentials..."
		try {
			$global:LocalCredentials = Get-Credential -Message "Enter default local admin credentials" -UserName "$DefaultLocalAdminAccount"
		} catch {
			$global:LocalCredentials = $null
		}
		if (!$global:LocalCredentials) {
			throw "I can't proceed without Local credentials"
		}
	}
}

function Deploy-LabVm {
	param(
		[Parameter(Mandatory=$true,ValueFromPipeline=$true)]
		[String[]] $Names,
		[Switch] $JoinDomain=$false,
		[Switch] $Encrypt=$false
	)
    Write-Host "[.] Starting up"
    $starttime = Get-Date

	if (!(Login-AzCsub)) {
    	throw 'Unable to login to AZ'
    }

    Get-Creds

    $Results = @{}
    $vmbundle = @{}
    $vmbundles = @()

    foreach ($Name in $Names) {
    	try {
		    $vmName = ""

	    	$vmName = Select-VmName $Name

		    $resourceGroupName = $DefaultResourgeGroupName
		    $vnetResourceGroupName = "CSUB-ADFS"
		    $vnetName = "csub-adfs"
		    $subnetName = "SUB-Lab1-West"
		    $location = "westus"
		    $nicName = "$($vmName)_nic"
		    $diskname = "$($vmName)_os"
		    $disksize = 128
		    $ssname = $DefaultLabImage
		    $storageType = 'Premium_LRS'
		    $vmSize = 'Standard_D4s_v3'

		    Write-Host "[.] Decided on name: $vmName"
		    Write-Host "[.] Using base image: $ssname"
		    Write-Host "[.] Using subnet: $subnetName"
		    Write-Host "[.] Using size: $vmSize"

		    $vmbundle['JoinDomain'] = $JoinDomain
		    $vmbundle['Encrypt'] = $Encrypt
		    $vmbundle['vmName'] = $vmName
		    $vmbundle['resourceGroupName'] = $resourceGroupName
		    $vmbundle['vnetResourceGroupName'] = $vnetResourceGroupName
		    $vmbundle['vnetName'] = $vnetName
		    $vmbundle['subnetName'] = $subnetName
		    $vmbundle['location'] = $location
		    $vmbundle['nicName'] = $nicName
		    $vmbundle['diskname'] = $diskname
		    $vmbundle['disksize'] = $disksize
		    $vmbundle['ssname'] = $ssname
		    $vmbundle['storageType'] = $storageType
		    $vmbundle['vmSize'] = $vmSize
		    $vmbundle['DomainName'] = $DefaultDomainName
		    $vmbundle['DomainCredentials'] = $DomainCredentials
		    $vmbundle['LocalCredentials'] = $LocalCredentials
		    $vmbundle['TenantCredentials'] = $TenantCredentials
		    $vmbundle['OuPath'] = $DefaultOuPath
		    $vmbundle['KeyVaultName'] = $DefaultKeyVaultName
		    $vmbundle['RdAgent'] = $RdAgentUrl
		    $vmbundle['RdAgentBl'] = $RdAgentBlUrl
		    $vmbundle['DeploymentUrl'] = $DefaultDeploymentUrl
		    $vmbundle['TenantName'] = $DefaultTenantName
		    $vmbundle['HostPoolName'] = $DefaultHostPoolName

		    $vmbundles += $vmbundle
	    } catch {
	    	Write-Host "[!] Unable to select/verify availability of a name for $Name"
	    }
    }

    $vmbundles | %{
		$SB = {
			param($vmbundle)

		    $JoinDomain = $vmbundle['JoinDomain']
		    $Encrypt = $vmbundle['Encrypt']
		    $vmName = $vmbundle['vmName']
		    $resourceGroupName = $vmbundle['resourceGroupName']
		    $vnetResourceGroupName = $vmbundle['vnetResourceGroupName']
		    $vnetName = $vmbundle['vnetName']
		    $subnetName = $vmbundle['subnetName']
		    $location = $vmbundle['location']
		    $nicName = $vmbundle['nicName']
		    $diskname = $vmbundle['diskname']
		    $disksize = $vmbundle['disksize']
		    $ssname = $vmbundle['ssname']
		    $storageType = $vmbundle['storageType']
		    $vmSize = $vmbundle['vmSize']
		    $DomainName = $vmbundle['DomainName']
		    $DomainCredentials = $vmbundle['DomainCredentials']
		    $LocalCredentials = $vmbundle['LocalCredentials']
		    $TenantCredentials = $vmbundle['TenantCredentials']
		    $OuPath = $vmbundle['OuPath']
		    $KeyVaultName = $vmbundle['KeyVaultName']
		    $RdAgent = $vmbundle['RdAgent']
		    $RdAgentBl = $vmbundle['RdAgentBl']
		    $TenantName = $vmbundle['TenantName']
		    $DeploymentUrl = $vmbundle['DeploymentUrl']
		    $HostPoolName = $vmbundle['HostPoolName']

		    # set up disk
		    Write-Host "[.] Setting up disk"
		    $disk = Get-AzDisk -ResourceGroupName $resourceGroupName -DiskName $diskname -ErrorAction SilentlyContinue
		    if (!$disk) {
		        $ss = Get-AzSnapshot -ResourceGroupName $resourceGroupName -SnapshotName $ssname
		        $diskconf = New-AzDiskConfig -AccountType $storageType -Location $location -CreateOption Copy -SourceResourceId $ss.Id
		        $disk = New-AzDisk -ResourceGroupName $resourceGroupName -DiskName $diskname -Disk $diskconf
		    }

		    # Set up networking resources
		    Write-Host "[.] Fetching VNet and Subnet information.."
		    if ($vnetResourceGroupName -and $vnetResourceGroupName.Length -gt 0) {
		        $vnet = Get-AzVirtualNetwork  -ResourceGroupName $vnetResourceGroupName -Name $vnetname
		    } else {
		        $vnet = Get-AzVirtualNetwork  -ResourceGroupName $resourceGroupName -Name $vnetname
		    }
		    $subnetId = (Get-AzVirtualNetworkSubnetConfig -Name $subnetName -VirtualNetwork $vnet).id

		    $nic = Get-AzNetworkInterface -Name $nicName -ResourceGroupName $resourceGroupName -ErrorAction SilentlyContinue
		    if (!$nic) {
		        Write-Host "[.] Creating a new NIC.."
	            $nic = New-AzNetworkInterface -Name $nicName -ResourceGroupName $resourceGroupName -Location $location -SubnetId $subnetId
		    }
		    $ip = $nic.IpConfigurations.PrivateIpAddress

		    # Configure Virtual Machine parameters
		    $vm = New-AzVMConfig -VMName $vmName -VMSize $vmSize
		    $vm = Add-AzVMNetworkInterface -VM $vm -Id $nic.Id
		    $vm = Set-AzVMOSDisk -VM $vm -ManagedDiskId $disk.Id -CreateOption Attach -Windows

		    # Create the new VM
		    Write-Host "[.] Creating the Virtual Machine: $vmName"
		    $res = New-AzVM -ResourceGroupName $resourceGroupName -Location $location -VM $vm -WarningAction SilentlyContinue -WarningVariable warn
		    sleep 5
		    
		    $extrem = Remove-AzVmExtension -ResourceGroupName $resourceGroupName -VMName $vmName -Name BGInfo -Force

		    Write-Host "[.] Connecting to $vmName"
		    $ict = 0
		    while ($ict -lt 11) {
			    try {
			    	$sess = New-PsSession "$ip" -Authentication Negotiate -Credential $LocalCredentials -ErrorAction SilentlyContinue -WarningAction SilentlyContinue
			    	if ($sess) { # using this as a proxy for 'system is up'
		    			Write-Host "[.] Renaming $ip to $vmName"
			    		Rename-Computer -ComputerName "$ip" -LocalCredential $LocalCredentials -NewName "$vmName" -Force -Restart
				    	break
			    	} else {
			    		sleep 5
			    	}
		    	} catch {
		    		sleep 5
		    	} finally {
		    		$ict = $ict + 1
		    	}
	    	}

	    	if ($ict -gt 10) {
	    		Write-Host "[!] Unable to rename the computer to $vmName - human intervention required. Skipping Domain Binding and Disk Encryption."
	    	} else {
			    sleep 30

			    for ($i=0;$i -lt 5; $i++) {
			    	$up = $false
				    $ict = 0
				    while ($ict -lt 11) {
					    try {
					    	$sess = New-PsSession $ip -Authentication Negotiate -Credential $LocalCredentials -ErrorAction SilentlyContinue -WarningAction SilentlyContinue
					    	if ($sess) {
					    		$up = $true
						    	break
					    	} else {
					    		sleep 3
					    	}
				    	} catch {
				    		sleep 3
				    	} finally {
				    		$ict = $ict + 1
				    	}
			    	}

			    	if ($ict -gt 10) {
			    		Write-Host "[.] Waiting for $vmName to reboot"
			    	}
			    	if ($up) { break; }
		    	}

		    	if ($up) {
		    		if ($JoinDomain) {
			    		Write-Host "[.] Binding $vmName to domain ($DomainName)"
			    		Add-Computer -ComputerName "$ip" -LocalCredential $LocalCredentials -Credential $DomainCredentials -DomainName "$DomainName" -OUPath "$OuPath" -NewName "$vmName" -Force -Restart
				    	sleep 30
			    	} else {
			    		Write-Host "[.] Skipping Binding $vmName to domain"
			    	}

				    for ($i=0;$i -lt 5; $i++) {
				    	$up = $false
					    $ict = 0
					    while ($ict -lt 11) {
						    try {
						    	$sess = New-PsSession $ip -Authentication Negotiate -Credential $LocalCredentials -ErrorAction SilentlyContinue -WarningAction SilentlyContinue
						    	if ($sess) {
						    		$up = $true
							    	break
						    	} else {
						    		sleep 3
						    	}
					    	} catch {
					    		sleep 3
					    	} finally {
					    		$ict = $ict + 1
					    	}
				    	}

				    	if ($ict -gt 10) {
				    		Write-Host "[.] Waiting for $vmName to reboot"
				    	}
				    	if ($up) { break; }
			    	}

			    	if ($up) {


					    sleep 30

					    for ($i=0;$i -lt 5; $i++) {
					    	$up = $false
						    $ict = 0
						    while ($ict -lt 11) {
							    try {
							    	$sess = New-PsSession $ip -Authentication Negotiate -Credential $LocalCredentials -ErrorAction SilentlyContinue -WarningAction SilentlyContinue
							    	if ($sess) {
							    		$up = $true
								    	break
							    	} else {
							    		sleep 3
							    	}
						    	} catch {
						    		sleep 3
						    	} finally {
						    		$ict = $ict + 1
						    	}
					    	}

					    	if ($ict -gt 10) {
					    		Write-Host "[.] Waiting for $vmName to reboot"
					    	}
					    	if ($up) { break; }
				    	}

				    	if ($up) {
				    		Write-Host "[.] Setting up Session Host"
				    		$reg = $null
							$RdsContext = Add-RdsAccount -DeploymentUrl $DeploymentUrl -Credential $TenantCredentials
				    		if ($RdsContext) {
					    		try {
					    			$reg = Export-RdsRegistrationInfo -TenantName $TenantName -HostPoolName $HostPoolName
				    			} catch {
				    				$reg = New-RdsRegistrationInfo -TenantName $TenantName -HostPoolName $HostPoolName -ExpirationHours 48
				    			}
		    				}
			    			if ($reg) {
			    				$key = $reg.Token.ToString()
					    		Invoke-Command -Session $sess -ScriptBlock { param($au,$bu,$key) 
					    			Invoke-WebRequest -Uri $au -OutFile "$Env:temp\rdagent.msi" -ErrorAction SilentlyContinue -WarningAction SilentlyContinue
					    			Invoke-WebRequest -Uri $bu -OutFile "$Env:temp\rdagentbootloader.msi" -ErrorAction SilentlyContinue -WarningAction SilentlyContinue
					    			if ((Test-Path -PathType Leaf -Path "$Env:temp\rdagent.msi") -and (Test-Path -PathType Leaf -Path "$Env:temp\rdagentbootloader.msi")) {
					    				Write-Host "[.] Both files were copied successfully"
					    				Invoke-Command -ScriptBlock {param($path,$key) cmd /c "msiexec /i $path /qn REGISTRATIONTOKEN=$key"} -ArgumentList "$Env:temp\rdagent.msi",$key
					    				Invoke-Command -ScriptBlock {param($path) cmd /c "msiexec /i $path /qn "} -ArgumentList "$Env:temp\rdagentbootloader.msi"
					    			} else {
					    				Write-Host "[!] The Session Host files were not copied as expected. This system will not be added to a host pool."
					    			}
					    		} -ArgumentList $RdAgent,$RdAgentBl,$key

							    sleep 30
						    } else {
						    	Write-Host "[!] Unable to get Registration information for $TenantName - $HostPoolName"
						    }

						    for ($i=0;$i -lt 5; $i++) {
						    	$up = $false
							    $ict = 0
							    while ($ict -lt 11) {
								    try {
								    	$sess = New-PsSession $ip -Authentication Negotiate -Credential $LocalCredentials -ErrorAction SilentlyContinue -WarningAction SilentlyContinue
								    	if ($sess) {
								    		$up = $true
									    	break
								    	} else {
								    		sleep 3
								    	}
							    	} catch {
							    		sleep 3
							    	} finally {
							    		$ict = $ict + 1
							    	}
						    	}

						    	if ($ict -gt 10) {
						    		Write-Host "[.] Waiting for $vmName to reboot"
						    	}
						    	if ($up) { break; }
					    	}

					    	if ($up) {

					    		if ($Encrypt) {
					    			Write-Host "[.] Encrypting the OS drive using key vault $KeyVaultName"
								    $vault = Get-AzKeyVault -VaultName $KeyVaultName
								    $encres = Set-AzVmDiskEncryptionExtension -ResourceGroupName $resourceGroupName -VMName $vmName -DiskEncryptionKeyVaultUrl $vault.VaultUri -DiskEncryptionKeyVaultId $vault.ResourceId -VolumeType OS -SkipVmBackup -Confirm:$false -Force
							    } else {
					    			Write-Host "[.] Skipping Encrypting the OS drive"

							    }
						    } else {
			    				Write-Host "[!] The system ($vmName) didn't come back up so far as I can tell. Human intervention required. Skipping Disk Encryption."	

						    }
				    	} else {
		    				Write-Host "[!] The system ($vmName) didn't come back up so far as I can tell. Human intervention required. Skipping WVD Config and Disk Encryption."	

				    	}


			    	} else {
			    		Write-Host "[!] The system ($vmName) didn't come back up so far as I can tell. Human intervention required. Skipping WVD Config and Disk Encryption."	
				    }
			    } else {
			    	Write-Host "[!] The system ($vmName) didn't come back up so far as I can tell. Human intervention required. Skipping Domain Binding, WVD Config, and Disk Encryption."
			    }
		    }

	    }
		
		$j = start-Job $SB -ArgumentList $_
    }
	Write-Host "[.] All creation tasks intiated. This will take at least 5 minutes, but probably longer."
	Write-Host "[.] Now awaiting tasks"
	sleep 200

	$i=0
	While (Get-Job -State "Running") {
		$i=$i+1

		if (($i % 2) -eq 0) {
			Write-Host "[.] Still awaiting tasks"
		}
		sleep 10
	}

	$Results = get-job | receive-job
    $endtime = Get-Date
    $duration = $endtime - $starttime
	Write-Host "[.] All creation tasks complete! Duration: $duration"

    return $Results
}

function Deploy-FacVm {
	param(
		[Parameter(Mandatory=$true,ValueFromPipeline=$true)]
		[String[]] $Names
	)
    Write-Host "[.] Starting up"

	if (!(Login-AzCsub)) {
    	throw 'Unable to login to AZ'
    }

    $Results = @{}
    if (!$Names -or $Names.Count -lt 1) {
    	$Names += "VM-Fac0-West"
    }

    foreach ($Name in $Names) {
    	try {
		    $vmName = ""

	    	$vmName = Select-VmName $Name

		    Write-Host "[.] Decided on name: $vmName"

		    $resourceGroupName = "RG-Desktops-West"
		    $vnetResourceGroupName = "CSUB-ADFS"
		    $vnetName = "csub-adfs"
		    $subnetName = "SUB-Fac1-West"
		    $location = "westus"
		    $nicName = "$($vmName)_nic"
		    $diskname = "$($vmName)_os"
		    $disksize = 128
		    $ssname = 'vm-lab1-west-presysprep'
		    $storageType = 'Premium_LRS'
		    $vmSize = $DefaultVmSize

		    # set up disk
		    Write-Host "[.] Setting up disk"
		    $disk = Get-AzDisk -ResourceGroupName $resourceGroupName -DiskName $diskname -ErrorAction SilentlyContinue
		    if (!$disk) {
		        $ss = Get-AzSnapshot -ResourceGroupName $resourceGroupName -SnapshotName $ssname
		        $diskconf = New-AzDiskConfig -AccountType $storageType -Location $location -CreateOption Copy -SourceResourceId $ss.Id
		        $disk = New-AzDisk -ResourceGroupName $resourceGroupName -DiskName $diskname -Disk $diskconf
		    }

		    # Set up networking resources
		    Write-Host "[.] Fetching VNet and Subnet information.."
		    if ($vnetResourceGroupName -and $vnetResourceGroupName.Length -gt 0) {
		        $vnet = Get-AzVirtualNetwork  -ResourceGroupName $vnetResourceGroupName -Name $vnetname
		    } else {
		        $vnet = Get-AzVirtualNetwork  -ResourceGroupName $resourceGroupName -Name $vnetname
		    }
		    $subnetId = (Get-AzVirtualNetworkSubnetConfig -Name $subnetName -VirtualNetwork $vnet).id

		    $nic = Get-AzNetworkInterface -Name $nicName -ResourceGroupName $resourceGroupName -ErrorAction SilentlyContinue
		    if (!$nic) {
		        Write-Host "[.] Creating a new NIC.."
		        if ($createPublicIP) {
		            $nic = New-AzNetworkInterface -Name $nicName -ResourceGroupName $resourceGroupName -Location $location -SubnetId $subnetId -PublicIpAddressId $pip.id -NetworkSecurityGroupId $nsg.id
		        } else {
		            $nic = New-AzNetworkInterface -Name $nicName -ResourceGroupName $resourceGroupName -Location $location -SubnetId $subnetId -NetworkSecurityGroupId $nsg.Id
		        }
		    }

		    # Configure Virtual Machine parameters
		    $vm = New-AzVMConfig -VMName $vmName -VMSize $vmSize
		    $vm = Add-AzVMNetworkInterface -VM $vm -Id $nic.Id
		    $vm = Set-AzVMOSDisk -VM $vm -ManagedDiskId $disk.Id -CreateOption Attach -Windows

		    # Create the new VM
		    Write-Host "[.] Creating the Virtual Machine: $vmName"
		    $res = New-AzVM -ResourceGroupName $resourceGroupName -Location $location -VM $vm -WarningAction SilentlyContinue -WarningVariable warn
		    sleep 5
		    $extrem = Remove-AzVmExtension -ResourceGroupName $resourceGroupName -VMName $vmName -Name BGInfo -Force
		    $vault = Get-AzKeyVault -VaultName $DefaultKeyVaultName
		    $encres = Set-AzVmDiskEncryptionExtension -ResourceGroupName $resourceGroupName -VMName $vmName -DiskEncryptionKeyVaultUrl $vault.VaultUri -DiskEncryptionKeyVaultId $vault.ResourceId -VolumeType OS -SkipVmBackup -Confirm:$false -Force
	    } catch {
	    	$res=$_
	    } finally {
	    	$Results[$Name]=$res
	    }
    }
    return $Results
}

function Deploy-StaffVm {
	param(
		[Parameter(Mandatory=$true,ValueFromPipeline=$true)]
		[String[]] $Names
	)
    Write-Host "[.] Starting up"

	if (!(Login-AzCsub)) {
    	throw 'Unable to login to AZ'
    }

    $Results = @{}
    if (!$Names -or $Names.Count -lt 1) {
    	$Names += "VM-Staff0-West"
    }

    foreach ($Name in $Names) {
    	try {
		    $vmName = ""

	    	$vmName = Select-VmName $Name

		    Write-Host "[.] Decided on name: $vmName"

		    $resourceGroupName = "RG-Desktops-West"
		    $vnetResourceGroupName = "CSUB-ADFS"
		    $vnetName = "csub-adfs"
		    $subnetName = "SUB-Staff1-West"
		    $location = "westus"
		    $nicName = "$($vmName)_nic"
		    $diskname = "$($vmName)_os"
		    $disksize = 128
		    $ssname = 'vm-lab1-west-presysprep'
		    $storageType = 'Premium_LRS'
		    $vmSize = $DefaultVmSize

		    # set up disk
		    Write-Host "[.] Setting up disk"
		    $disk = Get-AzDisk -ResourceGroupName $resourceGroupName -DiskName $diskname -ErrorAction SilentlyContinue
		    if (!$disk) {
		        $ss = Get-AzSnapshot -ResourceGroupName $resourceGroupName -SnapshotName $ssname
		        $diskconf = New-AzDiskConfig -AccountType $storageType -Location $location -CreateOption Copy -SourceResourceId $ss.Id
		        $disk = New-AzDisk -ResourceGroupName $resourceGroupName -DiskName $diskname -Disk $diskconf
		    }

		    # Set up networking resources
		    Write-Host "[.] Fetching VNet and Subnet information.."
		    if ($vnetResourceGroupName -and $vnetResourceGroupName.Length -gt 0) {
		        $vnet = Get-AzVirtualNetwork  -ResourceGroupName $vnetResourceGroupName -Name $vnetname
		    } else {
		        $vnet = Get-AzVirtualNetwork  -ResourceGroupName $resourceGroupName -Name $vnetname
		    }
		    $subnetId = (Get-AzVirtualNetworkSubnetConfig -Name $subnetName -VirtualNetwork $vnet).id

		    $nic = Get-AzNetworkInterface -Name $nicName -ResourceGroupName $resourceGroupName -ErrorAction SilentlyContinue
		    if (!$nic) {
		        Write-Host "[.] Creating a new NIC.."
		        if ($createPublicIP) {
		            $nic = New-AzNetworkInterface -Name $nicName -ResourceGroupName $resourceGroupName -Location $location -SubnetId $subnetId -PublicIpAddressId $pip.id -NetworkSecurityGroupId $nsg.id
		        } else {
		            $nic = New-AzNetworkInterface -Name $nicName -ResourceGroupName $resourceGroupName -Location $location -SubnetId $subnetId -NetworkSecurityGroupId $nsg.Id
		        }
		    }

		    # Configure Virtual Machine parameters
		    $vm = New-AzVMConfig -VMName $vmName -VMSize $vmSize
		    $vm = Add-AzVMNetworkInterface -VM $vm -Id $nic.Id
		    $vm = Set-AzVMOSDisk -VM $vm -ManagedDiskId $disk.Id -CreateOption Attach -Windows

		    # Create the new VM
		    Write-Host "[.] Creating the Virtual Machine: $vmName"
		    $res = New-AzVM -ResourceGroupName $resourceGroupName -Location $location -VM $vm -WarningAction SilentlyContinue -WarningVariable warn
		    sleep 5
		    $extrem = Remove-AzVmExtension -ResourceGroupName $resourceGroupName -VMName $vmName -Name BGInfo -Force
		    $vault = Get-AzKeyVault -VaultName $DefaultKeyVaultName
		    $encres = Set-AzVmDiskEncryptionExtension -ResourceGroupName $resourceGroupName -VMName $vmName -DiskEncryptionKeyVaultUrl $vault.VaultUri -DiskEncryptionKeyVaultId $vault.ResourceId -VolumeType OS -SkipVmBackup -Confirm:$false -Force
	    } catch {
	    	$res=$_
	    } finally {
	    	$Results[$Name]=$res
	    }
    }
    return $Results
}

function Delete-RdsHostPool {
	param(
		[Parameter(Mandatory=$true,ValueFromPipeline=$true)]
		[String[]] $Names
	)

	foreach ($Name in $Names) {
		try {
			Get-RdsSessionHost -TenantName $DefaultTenantName -HostPoolName $Name | Remove-RdsSessionHost
			Get-RdsAppGroup -TenantName $DefaultTenantName -HostPoolName $Name | Remove-RdsAppGroup
			Remove-RdsHostPool -TenantName $DefaultTenantName -Name $Name
		} catch {
			Write-Host "[!] Error on $Name : $_"
		}
	}

}

function Enumerate-RdsUsers ($AppGroup="Desktop Application Group") {
	try { Get-RdsHostPool -TenantName $DefaultTenantName | ForEach-Object { Get-RdsAppGroupUser -TenantName $DefaultTenantName -HostPoolName $_.HostPoolName -AppGroupName $AppGroup | select UserPrincipalName,HostPoolName }
	} catch {
		Write-Host "[!] Error on $AppGroup in $DefaultTenantName : $_"
	}
}

function Enumerate-RdsUserSessions ($HostPoolName="") {
	if ($HostPoolName -eq "") { Get-RdsHostPool -TenantName $DefaultTenantName | ForEach-Object { Get-RdsUserSession -TenantName $DefaultTenantName -HostPoolName $_.HostPoolName | select UserPrincipalName,HostPoolName,SessionHostName,SessionState }
	} else { Get-RdsUserSession -TenantName $DefaultTenantName -HostPoolName $HostPoolName | select UserPrincipalName,HostPoolName,SessionHostName,SessionState }
}

function Get-UnusedSessionHosts ($HostPoolName="") {
	$servers = @()
	try {
		if ($HostPoolName -eq "") {
			$servers = Get-RdsHostPool -TenantName $DefaultTenantName | ForEach-Object { Get-RdsSessionHost -TenantName $DefaultTenantName -HostPoolName $_.HostPoolName } | where {$_.Sessions -eq 0}
		} else {
			$servers = Get-RdsSessionHost -TenantName $DefaultTenantName -HostPoolName $_.HostPoolName | where {$_.Sessions -eq 0}
		}
	} catch {}
	return $servers
}

function Get-BrokenSessionHosts ($HostPoolName="") {
	$servers = @()
	try {
		if ($HostPoolName -eq "") {
			$servers = Get-RdsHostPool -TenantName $DefaultTenantName | ForEach-Object { Get-RdsSessionHost -TenantName $DefaultTenantName -HostPoolName $_.HostPoolName } | where {$_.Status -ne 'Available'}
		} else {
			$servers = Get-RdsSessionHost -TenantName $DefaultTenantName -HostPoolName $_.HostPoolName | where {$_.Sessions -eq 0}
		}
	} catch {}
	return $servers
}

function Get-LiveHosts {
	param(
		[Parameter(Mandatory=$true,ValueFromPipeline=$true)]
		[System.Object[]] $Things
	)

	Write-Host "[.] Starting up"
	if ($Restart) {
		Write-Host -ForegroundColor Red "[i] This will attempt to restart the computers"
		sleep 2
	}

	if (($Things.GetType().Name | sort-object -unique).count -ne 1) {
		throw "Must pass all the same type of thing"
	}
	$ttype = $Things[0].GetType().Name
	if ($ttype -eq "RdMgmtSessionHost") {
		$servers = ($Things | sort -unique -Property SessionHostname | Select SessionHostName).SessionHostName -replace '.ad.csub.edu','' -replace '.csub.edu',''
	} elseif ($ttype -eq "RdMgmtHostPool") {
		$servers = ($Things | sort -unique -Property HostPoolname | foreach-object {Get-RdsSessionHost -TenantName $_.TenantName -HostPoolName $_.HostPoolName } | select SessionHostName | sort -unique -Property SessionHostname).SessionHostName -replace '.ad.csub.edu','' -replace '.csub.edu',''
	} elseif ($ttype -eq "String") {
		$servers = ($Things | sort -unique) -replace '.ad.csub.edu','' -replace '.csub.edu',''
	} else {
		throw "Unsupported object type ($ttype)"
	}

	$ct = $servers.Count
	Write-Host "[.] Checking $ct servers"
	$i=0

	$names = @()

	foreach ($server in $servers) {
		$per = $i/$ct*100.0
		Write-Progress -Activity "Getting Live Hosts: $server" -Status "$per% complete:" -PercentComplete $per
		try {
			$vm = Get-AzVm -ResourceGroupName $DefaultResourgeGroupName -Name $server -Status -ErrorAction SilentlyContinue -WarningAction SilentlyContinue
			if ($vm.Statuses.Code -match 'PowerState/running') {
				$names += $server
			}
		} catch {
			Write-Host "[i] Error on $server : $_"
		}
		$i=$i+1
	}
	$per = $i/$ct*100.0	
	Write-Progress -Activity "Getting Live Hosts" -Status "$per% complete:" -PercentComplete $per
	Write-Host "[.] Done!"
	return $names
}

function Fix-SxsListener { # TODO: fix with local admin perms
	param(
		[Parameter(Mandatory=$true,ValueFromPipeline=$true)]
		[System.Object[]] $Things,
		[Switch] $Restart=$false
	)

	Write-Host "[.] Starting up"
	if ($Restart) {
		Write-Host -ForegroundColor Red "[i] This will attempt to restart the computers"
		sleep 2
	}

	if (($Things.GetType().Name | sort-object -unique).count -ne 1) {
		throw "Must pass all the same type of thing"
	}
	$ttype = $Things[0].GetType().Name
	if ($ttype -eq "RdMgmtSessionHost") {
		$servers = ($Things | sort -unique -Property SessionHostname | Select SessionHostName).SessionHostName -replace '.ad.csub.edu','' -replace '.csub.edu',''
	} elseif ($ttype -eq "RdMgmtHostPool") {
		$servers = ($Things | sort -unique -Property HostPoolname | foreach-object {Get-RdsSessionHost -TenantName $_.TenantName -HostPoolName $_.HostPoolName } | select SessionHostName | sort -unique -Property SessionHostname).SessionHostName -replace '.ad.csub.edu','' -replace '.csub.edu',''
	} elseif ($ttype -eq "String") {
		$servers = ($Things | sort -unique) -replace '.ad.csub.edu','' -replace '.csub.edu',''
	} else {
		throw "Unsupported object type ($ttype)"
	}

	$ct = $servers.Count
	Write-Host "[.] Working on $ct servers"
	$i=0

	foreach ($server in $servers) {
		$per = $i/$ct*100.0
		Write-Progress -Activity "Resetting Services: $server" -Status "$per% complete:" -PercentComplete $per
		try {
			Get-Service -ComputerName $server -Name RdAgent -ErrorAction SilentlyContinue | Restart-Service -ErrorAction SilentlyContinue
			Get-Service -ComputerName $server -Name RDAgentBootLoader -ErrorAction SilentlyContinue | Restart-Service -ErrorAction SilentlyContinue
			if ($Restart) {
				Restart-Computer -ComputerName $server
			}
		} catch {
			Write-Host "[i] Error on $server : $_"
		}
		$i=$i+1
	}
	$per = $i/$ct*100.0	
	Write-Progress -Activity "Resetting Services" -Status "$per% complete:" -PercentComplete $per
	Write-Host "[.] Done!"
}

function Manage-SessionHosts ($Timeout = "00:15:00.00") { # relies on an environment variable
	Write-Host "==========  Management Begin  =========="
	Write-Host "==========  Scheduled Startup  =========="
	Do-AutoStartup
	Write-Host "==========  Scheduled Shutdown  =========="
	Do-AutoShutdown
	Write-Host "==========  Enumerating Session Hosts  =========="
	$BrokenHosts = Get-LiveHosts (Get-BrokenSessionHosts)
	$UnusedHosts = Get-LiveHosts (Get-UnusedSessionHosts | where {$_ -notin $BrokenHosts})
	Write-Host "==========  Fix Session Hosts  =========="
	$Fixed = Fix-SxsListener $BrokenHosts
	$Runtime = Get-Date
	Write-Host "==========  Shutdown Unused Hosts  =========="
	$UnusedHosts | ForEach-Object { if ($global:UnusedSessionHostLog["$_"]) { if (($Runtime - $global:UnusedSessionHostLog["$_"]) -gt $Timeout) { Do-Shutdown $_ } } else {$global:UnusedSessionHostLog["$_"] = $Runtime} } 
}

Start-WinRm



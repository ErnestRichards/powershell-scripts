### Utilities.psm1
### Ernest Richards
### cause I'm tired of redefining these everywhere

# reference: https://www.powershellgallery.com/packages/Communary.PASM/1.0.38/Content/Functions%5CGet-LevenshteinDistance.ps1
function Get-LevenshteinDistance {
    <#
        .SYNOPSIS
            Get the Levenshtein distance between two strings.
        .DESCRIPTION
            The Levenshtein Distance is a way of quantifying how dissimilar two strings (e.g., words) are to one another by counting the minimum number of operations required to transform one string into the other.
        .EXAMPLE
            Get-LevenshteinDistance 'kitten' 'sitting'
        .LINK
            http://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance#C.23
            http://en.wikipedia.org/wiki/Edit_distance
            https://communary.wordpress.com/
            https://github.com/gravejester/Communary.PASM
        .NOTES
            Author: Øyvind Kallstad
            Date: 07.11.2014
            Version: 1.0
    #>
    [CmdletBinding()]
    param(
        [Parameter(Position = 0)]
        [string]$String1,

        [Parameter(Position = 1)]
        [string]$String2,

        # Makes matches case-sensitive. By default, matches are not case-sensitive.
        [Parameter()]
        [switch] $CaseSensitive,

        # A normalized output will fall in the range 0 (perfect match) to 1 (no match).
        [Parameter()]
        [switch] $NormalizeOutput
    )

    if (-not($CaseSensitive)) {
        $String1 = $String1.ToLowerInvariant()
        $String2 = $String2.ToLowerInvariant()
    }

    $d = New-Object 'Int[,]' ($String1.Length + 1), ($String2.Length + 1)

    try {
        for ($i = 0; $i -le $d.GetUpperBound(0); $i++) {
            $d[$i,0] = $i
        }

        for ($i = 0; $i -le $d.GetUpperBound(1); $i++) {
            $d[0,$i] = $i
        }

        for ($i = 1; $i -le $d.GetUpperBound(0); $i++) {
            for ($j = 1; $j -le $d.GetUpperBound(1); $j++) {
                $cost = [Convert]::ToInt32((-not($String1[$i-1] -ceq $String2[$j-1])))
                $min1 = $d[($i-1),$j] + 1
                $min2 = $d[$i,($j-1)] + 1
                $min3 = $d[($i-1),($j-1)] + $cost
                $d[$i,$j] = [Math]::Min([Math]::Min($min1,$min2),$min3)
            }
        }

        $distance = ($d[$d.GetUpperBound(0),$d.GetUpperBound(1)])

        if ($NormalizeOutput) {
            Write-Output (1 - ($distance) / ([Math]::Max($String1.Length,$String2.Length)))
        }

        else {
            Write-Output $distance
        }
    }

    catch {
        Write-Warning $_.Exception.Message
    }
}

function Write-Log ($message, $level = "Information", $eventid = 1337, $LogName = "Application", $Source = "Do-License" ) {
	$messages = $message -split '(.{2500})' | ?{$_.length -gt 0}
	$i = 0
	$messages | %{
		Write-EventLog -LogName $LogName -Source $Source -EntryType $level -EventId $eventid -Message "$_ MsgCt=$i/$($messages.count)" -ErrorAction SilentlyContinue -WarningAction SilentlyContinue -ErrorVariable err
		if ($err) {
			Register-LogSource -Source "$Source"
			Write-EventLog -LogName $LogName -Source $Source -EntryType $level -EventId $eventid -Message "$_ MsgCt=$i/$($messages.count)" -ErrorAction SilentlyContinue -WarningAction SilentlyContinue -ErrorVariable err
			if ($err) {
				Write-Error "Unable to log this error locally using source name 'SecretServer'. Please register the log source on this system by running this command as a local administrator: New-EventLog -LogName `"Application`" -Source `"SecretServer`""	
				throw $err
			}
		}
		$i += 1
	}
	return
}

function Register-LogSource ($LogName = "Application", $Source = "Do-License") {
	try {
		$ret = New-EventLog -LogName "$LogName" -Source "$Source" -ErrorAction SilentlyContinue -ErrorVariable err -WarningAction SilentlyContinue
	} catch {}
}


function Generate-Password {
    [CmdletBinding()]
	param (
		$WordFilePath = "C:\users\erichards1\desktop\eff_large_wordlist.txt", 
		$MinimumLength = 11, 
		$Delimiter = "-", 
		$TestUser = "passwordtest",
		$MaxIterations = 5,
		$Interactive=$false,
		[Switch]$AsSecureString,
		$Credentials = $null # used for testing
	)
	if ((Test-Path -PathType Leaf -Path "$WordFilePath") -eq $false) {
		if ((Test-Path -PathType Leaf -Path "$($env:temp)\eff_large_wordlist.txt") -eq $false) {
			$URL = "https://www.eff.org/files/2016/07/18/eff_large_wordlist.txt"
			$out = "$($env:temp)\eff_large_wordlist.txt"
			Invoke-WebRequest -Uri "$URL" -OutFile "$out" -UseBasicParsing
			if ((Test-Path -PathType Leaf -Path "$out") -eq $false) {
				throw "Unable to get a wordfile. Can't generate passwords."
			}
			$WordFilePath = "$($env:temp)\eff_large_wordlist.txt"
		} else {
			Write-Verbose "Defaulting to '$($env:temp)\eff_large_wordlist.txt'"
			$WordFilePath = "$($env:temp)\eff_large_wordlist.txt"
		}
	}
	$success=$false
	$iter = 0
	$retry = $true
	while ($retry -eq $true) {
		$retry = $false
		Write-Verbose "[.] Generate-Password Starting"
		Write-Debug "[.] Generate-Password Starting"
		if ($global:WordList -eq $null) {
			Write-Verbose "[.] Populating the wordlist from $WordFilePath"
			Write-Debug "[.] Populating the wordlist from $WordFilePath"
			#$global:WordList = (import-csv $WordFilePath).2
			$global:WordList = (Import-Csv "$WordFilePath" -Header @('idx','words') -Delimiter "`t").words
		} else {
			Write-Verbose "[.] Wordlist already populated"
			Write-Debug "[.] Wordlist already populated"
		}
		$WordCount = $global:WordList.count
		Write-Verbose "[.] Wordlist provided $WordCount words"
		Write-Verbose "[.] Beginning password generation loop"
		Write-Debug "[.] Wordlist provided $WordCount words"
		Write-Debug "[.] Beginning password generation loop"
		while ($success -ne $true -and $iter -lt $MaxIterations) {
			$iter += 1
			Write-Verbose "[.] Iteration $iter beginning"
			Write-Debug "[.] Iteration $iter beginning"
			try {
				$tpass = ""
				while ($tpass.length -lt $MinimumLength) {
					$tpass += (Get-Culture).TextInfo.ToTitleCase($global:WordList[$(Get-Random -Minimum 0 -Maximum $WordCount)])
					if ($Delimiter) {
						$tpass += $Delimiter
					}
				}
				$tpass += (Get-Random -Minimum 1 -Maximum 999).ToString()

				Write-Verbose "[.] Candidate password length: $($tpass.length)"
				Write-Debug "[.] Candidate password length: $($tpass.length)"
				Write-Debug "[.] Candidate password: $tpass"
				if ($tpass -in $global:BadWordList) {
					Write-Verbose "[!] Known bad password generated."
					Write-Debug "[!] Known bad password generated."
					throw "known bad password"
				}
				if ($Credentials -eq $null) {
					Set-ADAccountPassword -Identity $TestUser -Reset -NewPassword (ConvertTo-SecureString -AsPlainText "$tpass" -Force) -ErrorAction Stop -Confirm:$false
				} else {
					Set-ADAccountPassword -Identity $TestUser -Reset -NewPassword (ConvertTo-SecureString -AsPlainText "$tpass" -Force) -ErrorAction Stop -Confirm:$false -Credential $Credentials -AuthType Negotiate
				}
				$success = $true
			} catch {
				if ($_ -match 'Access is denied') {
					Write-Warning "[!] Unable to test the password due to permissions issues. The generated password has not been vetted. For vetting, pass Credentials"
					$success = $true
				} else {
					Write-Verbose "[!] Unable to use the candidate password. Presumably, this is due to the password being too guessable, or already having been compromised. Error: $_"
					Write-Debug "[!] Unable to use the candidate password. Presumably, this is due to the password being too guessable, or already having been compromised."
					Write-Debug "[!] Error Message: $_"
					if ($tpass -notin $global:BadWordList) {
						Write-Verbose "[.] Adding candidate ($tpass) to the BadWordList"
						Write-Debug "[.] Adding candidate ($tpass) to the BadWordList"
						$global:BadWordList += $tpass
					}
				}
			}
		}
		if ($success -and $iter -le $MaxIterations) {
			Write-Verbose "[.] Success!"
			Write-Debug "[.] Success!"
			if ($AsSecureString -eq $true) {
				$tpass = ConvertTo-SecureString "$tpass" -AsPlainText -Force
			}
			return $tpass
		} else {
			Write-Host "[!] Unable to generate a usable password after $iter attempts. This could be random chance, or there could be a larger problem (connectivity to an AD DC to test the passwords, perhaps?)."
			if ($Interactive -eq $true) {
				$i = Read-Host "[.] Try again? (N/y)"
				if (-not $i -or $i.length -lt 1) {
					$retry = $false
				}
				try {
					if ($i.tolower()[0] -eq 'y') {
						$retry = $true
						$iter = 0
					} else {
						$retry = $false
					}
				} catch {
					$retry = $false
				}
			} else {
				$retry = $false
			}
		}
	}
	return ""
}

function Create-AdAccount {
	[CmdletBinding()]
	param (
		[string]$netid,
		[string]$name,
		$pass=$null,
		[string]$ou="OU=Campus,DC=ad,DC=csub,DC=edu",
		[string[]]$memberof=@(),
		[int]$expiredays=365,
		[string]$domain="csub.edu",
		$runascreds=$null
	)
	$user = $null

	if (!$netid -or $netid -eq "") {
		Write-Error "Unable to proceed without at least a NetID"
		return $user
	}

	if (!$name -or $name -eq "") {
		$name = "$($netid[0]) $(($netid[1..($netid.length)] |?{$_ -notmatch "[0-9]"}) -join '')"
	}

	if (!$domain -or $domain -eq "") {
		$domain = "csub.edu"
	}

	if ($pass -eq $null -or $pass -eq "") {
		$pass = Generate-Password -AsSecureString
	} elseif ($pass.GetType().Name -eq 'String') {
		$pass = ConvertTo-SecureString $pass -AsPlainText -Force
	}

	try {
		if ($runascreds -eq $null) {
			if ($expiredays -and $expiredays -gt 0) {
				$user = New-AdUser -Name "$name" -Type User -Path "$ou" -AccountPassword $pass -Enabled $true -SamAccountName "$($netid.tolower())" -UserPrincipalName "$($netid.tolower())@$domain" -DisplayName "$name" -GivenName "$(($name -split ' ',2)[0])" -SurName "$(($name -split ' ',2)[1])" -PassThru -ErrorAction Stop -AccountExpirationDate (Get-Date).AddDays($expiredays)
			} else {
				$user = New-AdUser -Name "$name" -Type User -Path "$ou" -AccountPassword $pass -Enabled $true -SamAccountName "$($netid.tolower())" -UserPrincipalName "$($netid.tolower())@$domain" -DisplayName "$name" -GivenName "$(($name -split ' ',2)[0])" -SurName "$(($name -split ' ',2)[1])" -PassThru -ErrorAction Stop
			}
		} else {
			if ($expiredays -and $expiredays -gt 0) {
				$user = New-AdUser -Name "$name" -Type User -Path "$ou" -AccountPassword $pass -Enabled $true -SamAccountName "$($netid.tolower())" -UserPrincipalName "$($netid.tolower())@$domain" -DisplayName "$name" -GivenName "$(($name -split ' ',2)[0])" -SurName "$(($name -split ' ',2)[1])" -PassThru -ErrorAction Stop -AccountExpirationDate (Get-Date).AddDays($expiredays) -Credential $runascreds -AuthType Negotiate
			} else {
				$user = New-AdUser -Name "$name" -Type User -Path "$ou" -AccountPassword $pass -Enabled $true -SamAccountName "$($netid.tolower())" -UserPrincipalName "$($netid.tolower())@$domain" -DisplayName "$name" -GivenName "$(($name -split ' ',2)[0])" -SurName "$(($name -split ' ',2)[1])" -PassThru -ErrorAction Stop -Credential $runascreds -AuthType Negotiate
			}
		}
		sleep 3 # random sleep to ensure account exists in AD and we can act on it
		$memberof | %{
			$g = $_
			try {
				if ($runascreds -ne $null) {
					Add-AdGroupMember $_ $user -ErrorAction Stop -Credential $runascreds -AuthType Negotiate
				} else {
					Add-AdGroupMember $_ $user -ErrorAction Stop
				}
			} catch {
				Write-Error "Unable to add user to group ($g). Using RunAsCreds? $($runascreds -ne $null). Error: $_"
			}
		}
		$user | Add-Member -NotePropertyName Password -NotePropertyValue $pass -Force -ErrorAction SilentlyContinue
	} catch {
		Write-Error "Unable to create AD User account with the following details:`nName: $name`nnetid: $netid`nou=$ou`nexpiration days: $expiredays`ndomain: $domain`nError Message: $_"
		$user = $null
	}

	return $user
}

function Create-GuestAccounts ($basename = "FormsAccount", $count = 30, $ou = "OU=Guest,OU=ITSS,OU=IT,DC=ad,DC=csub,DC=edu", $days = 2) {
	$accts = @()
	$maxlen = $basename.length + $count.tostring().length
	if ($maxlen -ge 20) {
		$basename = $basename[0..$($maxlen-21)] -Join ""
		Write-Verbose "Truncating Base Name to $basename"
	}
	for ($i = 0; $i -lt $count; $i++) {
		$acct = @{"user"=$null;"pass"=""}
		$tname = ""
		$tpass = Generate-Password
		$tname = $basename + $i.ToString()
		try {
			$user = New-ADUser -Name $tname -Type User -Path $ou -AccountExpirationDate (Get-Date).AddDays($days) -AccountPassword (convertto-securestring $tpass -AsPlainText -Force) -CannotChangePassword $True -Enabled $True -SamAccountName $tname.ToLower() -UserPrincipalName ($tname.ToLower() +"@csub.edu") -DisplayName $tname -GivenName $tname -PassThru
			$acct["user"] = $user
			$acct["pass"] = $tpass
			$accts += $acct
		} catch {
			Write-Error "[!] - Failed once. Trying a new password..."
			$tpass = Generate-Password
			try {
				$user = New-ADUser -Name $tname -Type iNetOrgPerson -Path $ou -AccountExpirationDate (Get-Date).AddDays($days) -AccountPassword (convertto-securestring $tpass -AsPlainText -Force) -CannotChangePassword $True -Enabled $True -SamAccountName $tname.ToLower() -UserPrincipalName ($tname.ToLower() +"@csub.edu") -DisplayName $tname -GivenName $tname -PassThru
				$acct["user"] = $user
				$acct["pass"] = $tpass
				$accts += $acct
			} catch {
				Write-Error "[!] - Failed again. Aborting."
				return 1
			}
		}
	}
	Write-Host "Created $count Accounts!"
	return $accts
}

function md5hash($path)
{
    $ret = ""
	if (!$path -or $path -eq "") {
		return $ret
	}
    $fullPath = Resolve-Path $path -ErrorAction SilentlyContinue -WarningAction SilentlyContinue
	if (!$fullPath -or $fullPath -eq "") {
		return $ret
	}
    $md5 = new-object -TypeName System.Security.Cryptography.MD5CryptoServiceProvider
    $file = $null
    try {
    	$file = [System.IO.File]::Open($fullPath,[System.IO.Filemode]::Open, [System.IO.FileAccess]::Read)
	} catch {
		$file = $null
	}
    if (!$file) {
    	return $ret
    }
    try {
        $ret = [System.BitConverter]::ToString($md5.ComputeHash($file))
    } catch {
    	write-host "[!] Unable to hash $path"
    	$ret = ""
    } finally {
        $file.Dispose()
    }
    return $ret
}

function sha256hash($path)
{
    $ret = ""
	if (!$path -or $path -eq "") {
		return $ret
	}
    $fullPath = Resolve-Path $path -ErrorAction SilentlyContinue -WarningAction SilentlyContinue
	if (!$fullPath -or $fullPath -eq "") {
		return $ret
	}
    $sha256 = [System.Security.Cryptography.HashAlgorithm]::Create('sha256')
    $file = $null
    try {
    	$file = [System.IO.File]::Open($fullPath,[System.IO.Filemode]::Open, [System.IO.FileAccess]::Read)
	} catch {
		$file = $null
	}
    if (!$file) {
    	return $ret
    }

    try {
        $ret = [System.BitConverter]::ToString($sha256.ComputeHash($file))
    } catch {
    	write-host "[!] Unable to hash $path"
    	$ret = ""
    } finally {
        $file.Dispose()
    }
    return $ret
}

function Find-Md5 ($thash) {
	write-Host "[i] Checking for $thash"
	$thash = $thash.replace(":","").replace(".","").replace("-","").tolower()
	foreach ($target in (gci -Recurse -File | select FullName)) {
		try {
			$h = md5hash $target.FullName
			$h = $h.replace(":","").replace(".","").replace("-","").tolower()
			write-host "$h =?= $thash"
			if ($h -eq $thash) {
				Write-Host "Located Hash: $target.FullName"
				return $target.FullName
			}
		} catch {
			Write-Error "Error on $target"
		}
	}
}

function Get-WinServices () {

	$wmiServices = @{}
	Get-WmiObject -Namespace root\cimv2 -Class Win32_Service | ForEach-Object { $wmiServices.Add($_.Name,$_) }
	$s = Get-Service | ForEach-Object { $_ | Add-Member -Name Win32_Service -MemberType NoteProperty -Value $(if ($wmiServices.ContainsKey($_.ServiceName)) {$wmiServices[$_.ServiceName]} else {$null}) -PassThru } | Add-Member -Name Description -MemberType ScriptProperty -Value {if ($this.Win32_Service) { $this.Win32_Service.Description }} -PassThru | Add-Member -Name LogonAs -MemberType ScriptProperty -Value {if ($this.Win32_Service) { $this.Win32_Service.StartName }} -PassThru | Add-Member -Name StartupType -MemberType ScriptProperty -Value {if ($this.Win32_Service) { $this.Win32_Service.StartMode }} -PassThru

	return $s
}

function Get-Files {
    [CmdletBinding()]
	param (
		[string]
		$Path=".",

		[string]
		$Owner="",

		[switch]
		$Recurse
	)

	$Files = Get-ChildItem $Path -File -Recurse:$Recurse -WarningAction SilentlyContinue -ErrorAction SilentlyContinue
	$Files | Add-Member -MemberType ScriptProperty -Name Owner -Value { (Get-Acl $this.FullName).Owner }
	if ($Owner -and $Owner -ne "") { # filter based on ownership
		if ($Owner -match "|") {
			$olist = $Owner.ToLower().split("|")
			$clist = @()
			foreach ($o in $olist) {
				$clist += $o
				$clist += "$o@csub.edu"
				$clist += "csub-ad\$o"
			}
			$Files = $Files | where {$_.Owner.ToLower() -in $clist}
		} else {
			$Owner = $Owner.ToLower()
			$Files = $Files | where {$_.Owner.ToLower() -eq $Owner -or $_.Owner.ToLower() -eq "csub-ad\$Owner" -or $_.Owner.ToLower() -eq "$Owner@csub.edu"}
		}
	}
	$Files = $Files | Sort-Object -Unique # probably an expensive operation here
	return $Files
}

function Reset-Passwords {
    [CmdletBinding()]
	param (
        [Parameter(Mandatory=$true,ValueFromPipeline=$true)]
		$users,
		$newpassword=$null,
		$before=""
		)

	if (!$users -or $users.length -lt 1) {
		Write-Error "Give me something to work with here!"
		return $null
	}

	if (!$newpassword) {
		Write-Verbose "[.] Generating random password"
		$newpassword = Generate-Password
	}

	$res = @()

	Write-Host "[.] Working..."

	foreach ($user in $users) {
		try {
			$u = Get-Aduser $user -Properties memberof,enabled,lockedout,passwordlastset,lastlogondate,mail,proxyaddresses -ErrorAction SilentlyContinue -WarningAction SilentlyContinue
			if ($u) {
				if ($before -ne "" -and $u.passwordlastset -ge $before) {
					Write-Verbose "$user set their password. No need to reset"
					$res += "Complete by User"
				} else {
					$ret = Read-Host "[+] Reset password for $user (Y)"
					if ($ret -and $ret.tolower() -like "n*") {
						Write-Verbose "[-] Skipping $user..."
						$res += "Skipped"
					} else {
						Write-Verbose "[!] Reseting password for $user"
						try {
							Set-ADAccountPassword $user -Reset -NewPassword (ConvertTo-SecureString -AsPlainText "$newpassword" -Force)
							$res += "Reset"
							Write-Verbose "[i] Done for $user"
						} catch {
							$res += "Failed"
							Write-Error "[!] Failed for $user"
						}
					}
				}
			} else {
				$res += "No User"
				Write-Verbose "No Such User: $user"
			}
		} catch {
			$res += "No User"
			Write-Verbose "No Such User: $user"
		}
	}

	Write-Host "Done!"

	return $res

}

function ConvertFrom-SID {
<#
.SYNOPSIS
    Convert SID to user or computer account name
.DESCRIPTION
    Convert SID to user or computer account name
.PARAMETER SID
    One or more SIDs to convert
.EXAMPLE
    ConvertFrom-SID S-1-5-21-2139171146-395215898-1246945465-2359
.EXAMPLE 
    'S-1-5-32-580' | ConvertFrom-SID
.FUNCTIONALITY
    Active Directory
.NOTES
    SID conversion for well known SIDs from http://support.microsoft.com/kb/243330
#>
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true,ValueFromPipeline=$true)]
            [string[]]$sid
    )

    Begin{

        #well known SID to name map
        $wellKnownSIDs = @{
            'S-1-0' = 'Null Authority'
            'S-1-0-0' = 'Nobody'
            'S-1-1' = 'World Authority'
            'S-1-1-0' = 'Everyone'
            'S-1-2' = 'Local Authority'
            'S-1-2-0' = 'Local'
            'S-1-2-1' = 'Console Logon'
            'S-1-3' = 'Creator Authority'
            'S-1-3-0' = 'Creator Owner'
            'S-1-3-1' = 'Creator Group'
            'S-1-3-2' = 'Creator Owner Server'
            'S-1-3-3' = 'Creator Group Server'
            'S-1-3-4' = 'Owner Rights'
            'S-1-5-80-0' = 'All Services'
            'S-1-4' = 'Non-unique Authority'
            'S-1-5' = 'NT Authority'
            'S-1-5-1' = 'Dialup'
            'S-1-5-2' = 'Network'
            'S-1-5-3' = 'Batch'
            'S-1-5-4' = 'Interactive'
            'S-1-5-6' = 'Service'
            'S-1-5-7' = 'Anonymous'
            'S-1-5-8' = 'Proxy'
            'S-1-5-9' = 'Enterprise Domain Controllers'
            'S-1-5-10' = 'Principal Self'
            'S-1-5-11' = 'Authenticated Users'
            'S-1-5-12' = 'Restricted Code'
            'S-1-5-13' = 'Terminal Server Users'
            'S-1-5-14' = 'Remote Interactive Logon'
            'S-1-5-15' = 'This Organization'
            'S-1-5-17' = 'This Organization'
            'S-1-5-18' = 'Local System'
            'S-1-5-19' = 'NT Authority'
            'S-1-5-20' = 'NT Authority'
            'S-1-5-21-500' = 'Administrator'
            'S-1-5-21-501' = 'Guest'
            'S-1-5-21-502' = 'KRBTGT'
            'S-1-5-21-512' = 'Domain Admins'
            'S-1-5-21-513' = 'Domain Users'
            'S-1-5-21-514' = 'Domain Guests'
            'S-1-5-21-515' = 'Domain Computers'
            'S-1-5-21-516' = 'Domain Controllers'
            'S-1-5-21-517' = 'Cert Publishers'            
            'S-1-5-21-518' = 'Schema Admins'
            'S-1-5-21-519' = 'Enterprise Admins'
            'S-1-5-21-520' = 'Group Policy Creator Owners'
            'S-1-5-21-522' = 'Cloneable Domain Controllers'
            'S-1-5-21-526' = 'Key Admins'
            'S-1-5-21-527' = 'Enterprise Key Admins'
            'S-1-5-21-553' = 'RAS and IAS Servers'
            'S-1-5-21-571' = 'Allowed RODC Password Replication Group'
            'S-1-5-21-572' = 'Denied RODC Password Replication Group'
            'S-1-5-32-544' = 'Administrators'
            'S-1-5-32-545' = 'Users'
            'S-1-5-32-546' = 'Guests'
            'S-1-5-32-547' = 'Power Users'
            'S-1-5-32-548' = 'Account Operators'
            'S-1-5-32-549' = 'Server Operators'
            'S-1-5-32-550' = 'Print Operators'
            'S-1-5-32-551' = 'Backup Operators'
            'S-1-5-32-552' = 'Replicators'
            'S-1-5-64-10' = 'NTLM Authentication'
            'S-1-5-64-14' = 'SChannel Authentication'
            'S-1-5-64-21' = 'Digest Authority'
            'S-1-5-80' = 'NT Service'
            'S-1-5-83-0' = 'NT VIRTUAL MACHINE\Virtual Machines'
            'S-1-16-0' = 'Untrusted Mandatory Level'
            'S-1-16-4096' = 'Low Mandatory Level'
            'S-1-16-8192' = 'Medium Mandatory Level'
            'S-1-16-8448' = 'Medium Plus Mandatory Level'
            'S-1-16-12288' = 'High Mandatory Level'
            'S-1-16-16384' = 'System Mandatory Level'
            'S-1-16-20480' = 'Protected Process Mandatory Level'
            'S-1-16-28672' = 'Secure Process Mandatory Level'
            'S-1-5-32-554' = 'BUILTIN\Pre-Windows 2000 Compatible Access'
            'S-1-5-32-555' = 'BUILTIN\Remote Desktop Users'
            'S-1-5-32-556' = 'BUILTIN\Network Configuration Operators'
            'S-1-5-32-557' = 'BUILTIN\Incoming Forest Trust Builders'
            'S-1-5-32-558' = 'BUILTIN\Performance Monitor Users'
            'S-1-5-32-559' = 'BUILTIN\Performance Log Users'
            'S-1-5-32-560' = 'BUILTIN\Windows Authorization Access Group'
            'S-1-5-32-561' = 'BUILTIN\Terminal Server License Servers'
            'S-1-5-32-562' = 'BUILTIN\Distributed COM Users'
            'S-1-5-32-569' = 'BUILTIN\Cryptographic Operators'
            'S-1-5-32-573' = 'BUILTIN\Event Log Readers'
            'S-1-5-32-574' = 'BUILTIN\Certificate Service DCOM Access'
            'S-1-5-32-575' = 'BUILTIN\RDS Remote Access Servers'
            'S-1-5-32-576' = 'BUILTIN\RDS Endpoint Servers'
            'S-1-5-32-577' = 'BUILTIN\RDS Management Servers'
            'S-1-5-32-578' = 'BUILTIN\Hyper-V Administrators'
            'S-1-5-32-579' = 'BUILTIN\Access Control Assistance Operators'
            'S-1-5-32-580' = 'BUILTIN\Remote Management Users'
        }
    }

    Process {

        #loop through provided SIDs
        foreach($id in $sid){
            $fullsid = $id
            #Check for domain contextual SID's
            try {
                $IsDomain = $false
                if($id.Remove(8) -eq "S-1-5-21"){
                    $IsDomain = $true
                    $suffix = $id.Substring($id.Length - 4)
                    $id = $id.Remove(8) + $suffix
                }
            }
            catch {
                # String size issues
            }

            #Map name to well known sid.  If this fails, use .net to get the account
            if($name = $wellKnownSIDs[$id]){ }
            else{
                if($IsDomain){
                    $id = $fullsid
                }
                #Try to translate the SID to an account
                Try{
                    $objSID = New-Object System.Security.Principal.SecurityIdentifier($id)
                    $name = ( $objSID.Translate([System.Security.Principal.NTAccount]) ).Value
                }
                Catch{
                    $name = "Not a valid SID or could not be identified"
                    Write-Verbose "$id is not a valid SID or could not be identified"
                }
            }

            #Display the results
            New-Object -TypeName PSObject -Property @{
                SID = $fullsid
                Name = $name
            } | Select-Object SID, Name

        }
    }
}

function Send-Wol ($TargetMac = "", $TargetIp = "", $TargetPort = 9, $WorkingServers = @('veralab','sccm','tivoliem'), $PacketRepeat = 3) {

	$ret = @()

	if (-not $TargetMac -or -not $TargetIp -or $TargetMac -eq "" -or $TargetIp -eq "") {
		Write-Host "Send-Wol Usage:

		Example:   Send-Wol -TargetMac `"01:23:45:67:89:0a`" -TargetIp `"192.168.1.1`"

		The TargetMac is the MAC address of the listening interface on the targeted device.
		The TargetIp is the last known IP address of the listening interface on the targeted device.

		"
		return $ret
	}

	$jobnumber=0

	foreach ($server in $WorkingServers) {
		write-host "[.] Working with $server..."
		$open_sessions = get-pssession 
		$s = $open_sessions | where {$_.ComputerName -like "$server*"}
		if (-not $s -or $s.count -lt 1) {
			# create a new session. TODO: catch other errors here
			try {
				$sess = new-pssession -computername $server -ErrorAction SilentlyContinue -ErrorVariable looperr
				if (-not $sess -or $looperr) {
					throw $looperr
				}
			} catch {
				if ("." -in $server) {
					$server = ($server -split "\.")[0]
				}
				$server = "$server" + ".csub.edu"
				try {
					$sess = new-pssession -computername $server -ErrorAction SilentlyContinue -ErrorVariable looperr
					if (-not $sess -or $looperr) {
						throw $looperr
					}
				} catch {
					if ("." -in $server) {
						$server = ($server -split "\.")[0]
					}
					$server = "$server" + ".ad.csub.edu"
					try {
						$sess = new-pssession -computername $server -ErrorAction SilentlyContinue -ErrorVariable looperr
						if (-not $sess -or $looperr) {
							throw $looperr
						}
					} catch {
						write-host "[!] Error with $server : $looperr"
						continue
					}
				}
			}
		} elseif ($s.count -eq 1) {
			$sess = $s
		} else {
			# TODO: cleanup sessions. select a good one based on state, availability, and configurationname
			$sess = $s[0]
		}
		for ($i = 1; $i -lt $CmdCnt+1; $i++) {
			$j = Invoke-Command -Session $sess -AsJob -ScriptBlock {

				$mac = $Using:TargetMac
				$ip = $Using:TargetIp
				$port = $Using:TargetPort

				$MacByteArray = $mac -split "[:-]" | ForEach-Object { [Byte] "0x$_"}
				[Byte[]] $MagicPacket = (,0xFF * 6) + ($MacByteArray  * 16)
				$UdpClient = New-Object System.Net.Sockets.UdpClient
				$UdpClient.Connect($ip,$port)
				$UdpClient.Send($MagicPacket,$MagicPacket.Length)
				$UdpClient.Close()
			}
			$val = Receive-Job $j -Wait
			if ($val -and $val -gt 80 -and $val -lt 120) {
				write-verbose "[.]"
			} elseif ($val) {
				write-host "[-] Job returned an unexpected value: $val"
			} else {
				write-host "[!] Nothing was returned from job $i on $server . This probably represents an error."
			}
			$fin = @{
				JobNumber = ($jobnumber * $CmdCnt + $i);
				SourceHost = $server;
				SourceHostJobNumber = $i;
				TargetIp = $TargetIp;
				TargetMac = $TargetMac;
				ReturnValue = $val;
			}
			$ret += $fin
		}
		Remove-PsSession $sess
		$jobnumber=$jobnumber+1
	}
	return $ret
}


function Recursive-AdGroupMembership {

    param (
            [string]
            $GroupName="",

            [switch]
            $Unique,

            [string]
            $Properties=""
          )
    
    Remove-Variable ragmerr -Force -ErrorAction SilentlyContinue -WarningAction SilentlyContinue

    $members = @()
    if (-not $groupname -or $groupname -eq "") {
    	return $members
    }
    $members = Get-ADGroupMember $groupname -ErrorAction SilentlyContinue -ErrorVariable ragmerr -WarningAction SilentlyContinue
    if (-not $members -or $ragmerr) {
        Write-Verbose "I had trouble with the group named $groupname : $ragmerr"
        Remove-Variable ragmerr
        return $members
    }
    $groups = $members | where {$_.objectClass -eq 'group'}

    if ($Unique) {
        foreach ($group in $groups) {
            $newmembers = Recursive-AdGroupMembership -groupname $group -Properties $properties -Unique
            $members = @($newmembers) + $members
        }

        $members = $members | sort-object -unique
    } else {
        foreach ($group in $groups) {
            $newmembers = Recursive-AdGroupMembership -groupname $group -Properties $properties
            $members = @($newmembers) + $members
        }

        $members = $members | sort-object
    }

    $ret = @()
    
    $members = $members | where {$_.objectClass -ne 'group'}
    
    $ret = $members
    
    if ($Properties -ne "") {
        foreach ($member in $members) {
            $n = get-aduser $member -properties $Properties -ErrorAction SilentlyContinue -ErrorVariable ragmerr -WarningAction SilentlyContinue
            if (-not $n -or $ragmerr) {
                $ret = @($member) + $ret
            } else {
                $ret = @($n) + $ret
            }
        }
        if ($ragmerr) {
            Write-Error "I had trouble with the one of the additional properties requested: $ragmerr"
            Remove-Variable ragmerr
            return $ret
        }
    }

    return $ret
 }


function Connect-365 
{
    Import-Module MSOnline
    $O365Cred = Get-Credential
    $O365Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://ps.outlook.com/powershell -Credential $O365Cred -Authentication Basic -AllowRedirection
    Import-PSSession $O365Session
    Connect-MsolService -Credential $O365Cred
}


function Recursive-AdGroupMembership {

    param (
            [string]
            $GroupName="",

            [switch]
            $Unique,

            [string]
            $Properties=""
          )
    
    Remove-Variable ragmerr -Force -ErrorAction SilentlyContinue -WarningAction SilentlyContinue

    $members = @()
    if (-not $groupname -or $groupname -eq "") {
    	return $members
    }
    $members = Get-ADGroupMember $groupname -ErrorAction SilentlyContinue -ErrorVariable ragmerr -WarningAction SilentlyContinue
    if (-not $members -or $ragmerr) {
        Write-Error "I had trouble with the group named $groupname : $ragmerr"
        Remove-Variable ragmerr
        return $members
    }
    $groups = $members | where {$_.objectClass -eq 'group'}

    if ($Unique) {
        foreach ($group in $groups) {
            $newmembers = Recursive-AdGroupMembership -groupname $group -Properties $properties -Unique
            $members = @($newmembers) + $members
        }

        $members = $members | sort-object -unique
    } else {
        foreach ($group in $groups) {
            $newmembers = Recursive-AdGroupMembership -groupname $group -Properties $properties
            $members = @($newmembers) + $members
        }

        $members = $members | sort-object
    }

    $ret = @()
    
    $members = $members | where {$_.objectClass -ne 'group'}
    
    $ret = $members
    
    if ($Properties -ne "") {
        foreach ($member in $members) {
            $n = get-aduser $member -properties $Properties -ErrorAction SilentlyContinue -ErrorVariable ragmerr -WarningAction SilentlyContinue
            if (-not $n -or $ragmerr) {
                $ret = @($member) + $ret
            } else {
                $ret = @($n) + $ret
            }
        }
        if ($ragmerr) {
            Write-Error "I had trouble with the one of the additional properties requested: $ragmerr"
            Remove-Variable ragmerr
            return $ret
        }
    }

    return $ret
 }

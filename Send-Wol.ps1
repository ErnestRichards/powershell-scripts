
function Send-Wol ($TargetMac = "", $TargetIp = "", $TargetPort = 9, $WorkingServers = @('veralab','sccm','tivoliem'), $PacketRepeat = 3) {

	$ret = @()

	if (-not $TargetMac -or -not $TargetIp -or $TargetMac -eq "" -or $TargetIp -eq "") {
		Write-Host "Send-Wol Usage:

		Example:   Send-Wol -TargetMac `"01:23:45:67:89:0a`" -TargetIp `"192.168.1.1`"

		The TargetMac is the MAC address of the listening interface on the targeted device.
		The TargetIp is the last known IP address of the listening interface on the targeted device.

		"
		return $ret
	}

	$jobnumber=0

	foreach ($server in $WorkingServers) {
		write-host "[.] Working with $server..."
		$open_sessions = get-pssession 
		$s = $open_sessions | where {$_.ComputerName -like "$server*"}
		if (-not $s -or $s.count -lt 1) {
			# create a new session. TODO: catch other errors here
			try {
				$sess = new-pssession -computername $server -ErrorAction SilentlyContinue -ErrorVariable looperr
				if (-not $sess -or $looperr) {
					throw $looperr
				}
			} catch {
				if ("." -in $server) {
					$server = ($server -split "\.")[0]
				}
				$server = "$server" + ".csub.edu"
				try {
					$sess = new-pssession -computername $server -ErrorAction SilentlyContinue -ErrorVariable looperr
					if (-not $sess -or $looperr) {
						throw $looperr
					}
				} catch {
					if ("." -in $server) {
						$server = ($server -split "\.")[0]
					}
					$server = "$server" + ".ad.csub.edu"
					try {
						$sess = new-pssession -computername $server -ErrorAction SilentlyContinue -ErrorVariable looperr
						if (-not $sess -or $looperr) {
							throw $looperr
						}
					} catch {
						write-host "[!] Error with $server : $looperr"
						continue
					}
				}
			}
		} elseif ($s.count -eq 1) {
			$sess = $s
		} else {
			# TODO: cleanup sessions. select a good one based on state, availability, and configurationname
			$sess = $s[0]
		}
		for ($i = 1; $i -lt $CmdCnt+1; $i++) {
			$j = Invoke-Command -Session $sess -AsJob -ScriptBlock {

				$mac = $Using:TargetMac
				$ip = $Using:TargetIp
				$port = $Using:TargetPort

				$MacByteArray = $mac -split "[:-]" | ForEach-Object { [Byte] "0x$_"}
				[Byte[]] $MagicPacket = (,0xFF * 6) + ($MacByteArray  * 16)
				$UdpClient = New-Object System.Net.Sockets.UdpClient
				$UdpClient.Connect($ip,$port)
				$UdpClient.Send($MagicPacket,$MagicPacket.Length)
				$UdpClient.Close()
			}
			$val = Receive-Job $j -Wait
			if ($val -and $val -gt 80 -and $val -lt 120) {
				write-verbose "[.]"
			} elseif ($val) {
				write-host "[-] Job returned an unexpected value: $val"
			} else {
				write-host "[!] Nothing was returned from job $i on $server . This probably represents an error."
			}
			$fin = @{
				JobNumber = ($jobnumber * $CmdCnt + $i);
				SourceHost = $server;
				SourceHostJobNumber = $i;
				TargetIp = $TargetIp;
				TargetMac = $TargetMac;
				ReturnValue = $val;
			}
			$ret += $fin
		}
		Remove-PsSession $sess
		$jobnumber=$jobnumber+1
	}
	return $ret
}


$CurrentUser = New-Object Security.Principal.WindowsPrincipal $([Security.Principal.WindowsIdentity]::GetCurrent()) 
if (($CurrentUser.IsInRole([Security.Principal.WindowsBuiltinRole]::Administrator)) -eq $false) 
{ 
    $ArgumentList = "-noprofile -noexit -file `"{0}`" -Path `"$Path`" -MaxStage $MaxStage" 
    If ($ValidateOnly) { $ArgumentList = $ArgumentList + " -ValidateOnly" } 
    If ($SkipValidation) { $ArgumentList = $ArgumentList + " -SkipValidation $SkipValidation" } 
    If ($Mode) { $ArgumentList = $ArgumentList + " -Mode $Mode" } 
    Write-Host "elevating" 
    Start-Process powershell.exe -Verb RunAs -ArgumentList ($ArgumentList -f ($myinvocation.MyCommand.Definition)) -Wait 
    Exit 
} 

$interface = (Get-WmiObject win32_networkadapter -erroraction SilentlyContinue -errorvariable err | select name,index,netconnectionstatus | where {($_.netconnectionstatus -eq 2) -and -not (($_name -match 'virtual') -or ($_.name -match 'loop') -or ($_.name -match 'vmware'))})

if (!$interface) {
	write-host "An error occured while enumerating the primary network interface"
	write-host $err
	echo "An error occured while enumerating the primary network interface" >> c:\temp\error.err
	echo $err >> c:\temp\error.err
	exit (1)
}

$config = (Get-WmiObject win32_networkadapterconfiguration -filter "ipenabled = 'true'" | where {-not (($_.description -match 'virtual') -or ($_.description -match 'loop') -or ($_.description -match 'vmware') )})
$ret = $config.SetDNSServerSearchOrder(@("136.168.0.70","209.129.125.216","136.168.0.75","136.168.1.10"))
if ($ret.returnvalue -ne 0) {
	echo $ret.returnvalue >> c:\temp\error.err
	exit($ret.returnvalue)
}
	

exit (0)



foreach ($url in $urls) {
	$cidr = "null"
	$range = "null"
	$res = Invoke-WebRequest $url -ErrorAction SilentlyContinue -WarningAction SilentlyContinue
	if ($res) {
		$res = $res.content -match '((([0-9]{1,3}\.){3}[0-9]{1,3}) \- (([0-9]{1,3}\.){3}[0-9]{1,3}))'
		if ($res) {
			$range = $Matches[1]
			$res = $url -match '(([0-9]{1,3}\.){3}[0-9]{1,3}/[0-9]{1,2})'
			if ($res) {
				$cidr = $Matches[1]
				echo "$cidr : $range" >> Translations.txt
			}
		}
	}
}

function Setup-Certificate {
	param (
		[string]
		$CertPath="\\nas0\iso$\ernest\server deployment\cert.cer" # change this
	)

	$fingerprint = (\\nas0.csub.edu\iso$\ernest\server` deployment\openssl.exe x509 -in $CertPath -noout -fingerprint).split("=")[1].replace(":","")
	Import-Certificate -FilePath $CertPath -CertStoreLocation Cert:\LocalMachine\My -ErrorAction SilentlyContinue -ErrorVariable err
	if ($err) {
		Write-Host $err
		$err | Out-File -FilePath "C:\Users\\Desktop\setup-certificate.err" -Append
		exit(1)
	}
	$path = (Get-WmiObject -class "Win32_TSGeneralSetting" -Namespace root\cimv2\terminalservices -Filter "TerminalName='RDP-tcp'").__path
	Set-WmiInstance -Path $path -argument @{SSLCertificateSHA1Hash=$fingerprint}

}

$CurrentUser = New-Object Security.Principal.WindowsPrincipal $([Security.Principal.WindowsIdentity]::GetCurrent()) 
if (($CurrentUser.IsInRole([Security.Principal.WindowsBuiltinRole]::Administrator)) -eq $false) 
{ 
    $ArgumentList = "-noprofile -noexit -file `"{0}`" -Path `"$Path`" -MaxStage $MaxStage" 
    If ($ValidateOnly) { $ArgumentList = $ArgumentList + " -ValidateOnly" } 
    If ($SkipValidation) { $ArgumentList = $ArgumentList + " -SkipValidation $SkipValidation" } 
    If ($Mode) { $ArgumentList = $ArgumentList + " -Mode $Mode" } 
    Write-Host "elevating" 
    Start-Process powershell.exe -Verb RunAs -ArgumentList ($ArgumentList -f ($myinvocation.MyCommand.Definition)) -Wait 
    Exit 
}

Setup-Certificate

### Monitor-RiskySignins.psm1
### erichards1
### 20240516
###
### requires Entra ID P2 licensing
###
###

# TODO: catch keyboard interrupt
# TODO: event logging
# TODO: summarization review
# TODO: enrichment - ipinfo integration, ad/myid integration
# TODO: add a fix for initial trusted sites addition
# TODO: add installation attempt for first run. note that nuget may need to be installed, and PsGallery may not be trusted
# 	modules:
# 		Microsoft.Graph.Reports
# 		ActiveDirectory
# 
# TODO: storage file paths should not be temp, pick a location 
# TODO: set/validate save file/directory permissions
#
# there's lots of other todos in this file in relavent locations, just search for TODO
#

$Global:RiskySigninsSlackUri = $none
$Global:RPCreds
$Global:SaveDirectory

### Add all exclusions here
function Exclude-Events ($LogData, $EXCLUDED_ERRORCODES) {
	$ret = @()

	$LogData | %{
		$log = $_
		try {
			# add all exclusions here
			if ($log.status.errorcode -in $EXCLUDED_ERRORCODES) {
				throw "Exclusion 1: status.errorcode -in ($EXCLUDED_ERRORCODES)"
			}
			if ($log.status.errorcode -eq 50053 -and $log.RiskLevelDuringSignIn -ne 'high') {
				throw "Exclusion 2: status.errorcode -eq 50053 -and RiskLevelDuringSignIn -ne 'high'"
			}
			if ($log.AppDisplayName -match "Canvas") {
				throw "Exclusion 3: AppDisplayName matches 'Canvas' ($($log.AppDisplayName))"
			}

			$ret += $log
		} catch {
			Write-Verbose "Excluding $log for $_"
		}
	}

	return $ret 	
} # Exclude-Events

function Monitor-RiskySignins {
    [CmdletBinding()]
	param(
		$RiskLevels = @('high','medium'),
		$ExcludedErrorCodes = @(53003,50126),
		$DelayTime = 30,
		$RecordCount = 1000,
		$SaveFileDirectory = "$($env:temp)",
		[Switch]$EnrichLogs = $true,
		[Switch]$PostToSlack = $true,
		[Switch]$RemediateAccounts = $false,
		[Switch]$RemediateAccountsLogOnly = $true,
		[Switch]$ExcludeFalsePositives = $true,
		[Switch]$Quiet = $false,
		[Switch]$Silent = $false,
		[Switch]$SaveCliXml = $false,
		[Switch]$SaveAllLogs = $false,
		[Switch]$InstallGraph = $false,
		[Switch]$InstallRsat = $false
	)

	$sd = get-date
	write-verbose "Starting up"
	write-verbose "$sd"
	write-verbose "`n"
	write-verbose "Parameters:"
	write-verbose "RiskLevels: $RiskLevels"
	write-verbose "ExcludedErrorCodes: $ExcludedErrorCodes"
	write-verbose "DelayTime: $DelayTime"
	write-verbose "RecordCount: $RecordCount"
	write-verbose "SaveFileDirectory: $SaveFileDirectory"
	write-verbose "ExcludeFalsePositives: $ExcludeFalsePositives"
	write-verbose "EnrichLogs: $EnrichLogs"
	write-verbose "PostToSlack: $PostToSlack"
	write-verbose "RemediateAccounts: $RemediateAccounts"
	write-verbose "RemediateAccountsLogOnly: $RemediateAccountsLogOnly"
	write-verbose "Quiet: $Quiet"
	write-verbose "Silent: $Silent"
	write-verbose "SaveCliXml: $SaveCliXml"
	write-verbose "SaveAllLogs: $SaveAllLogs"
	write-verbose "`n"

	# only focus on certain risk levels
	$RISK_LEVELS = $RiskLevels

	# exclude irrelevant error coded auth events
		# 53003 - blocked by ca policy
		# 53053 - blocked by ip reputation or locked for repeated signin MUST INVESTIGATE THESE MANUALLY check authentication details, result detail to see if password was correct
		# 50126 - bad password
		# 50158 - external security challenge (ie MFA)

	$EXCLUDED_ERRORCODES = $ExcludedErrorCodes

	# seconds between api calls
	$DELAY_TIME = $DelayTime

	# how many records to pull/parse at a time
	$RECORD_COUNT = $RecordCount

	# variables TODO: export data, limit in-memory/on-disk size
	$riskyauths = @()
	# TODO: filename length check
	$Global:SaveDirectory = $SaveFileDirectory
	$filepath = $Global:SaveDirectory + "\riskySignins.csv"

	try {
		# import required Modules
		Import-Module Microsoft.Graph.Reports -ErrorAction Stop

		# connect. use required scopes.
		Connect-MgGraph -ErrorAction Stop -NoWelcome -Scopes AuditLog.Read.All
	} catch {
		write-verbose "Initially unable to load required modules. Attempting install."
		try {
			### http://stackoverflow.com/questions/7690994/powershell-running-a-command-as-administrator - Abatonime
			If (-NOT ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) {
				if ($InstallGraph -eq $true) {
					Write-Error "Already attempted escalation. Aborting."
					return $false
				}
				write-host "Elevating privilages..."
				$arguments = "& '" + $myinvocation.mycommand.definition + " -InstallGraph'"
				Start-Process "$psHome\powershell.exe" -wait -Verb runAs -ArgumentList $arguments
				Import-Module Microsoft.Graph.Reports -ErrorAction Stop
				Connect-MgGraph -ErrorAction Stop -NoWelcome -Scopes AuditLog.Read.All
			} else {
				Install-Module Microsoft.Graph.Reports -Force -Confirm:$false -Repository PSGallery -Scope CurrentUser -erroraction stop
				
				Import-Module Microsoft.Graph.Reports -ErrorAction Stop

				if ($InstallGraph -eq $true) {
					write-Verbose "Successfully installed module"
					return $true
				}
			}
		} catch {
			Write-Error "Unable to load modules or connect successfully to the MS Graph API: $_"
			return $riskyauths
		}
	}

	try {
		Import-Module ActiveDirectory -ErrorAction Stop
	} catch {
		try {

			### http://stackoverflow.com/questions/7690994/powershell-running-a-command-as-administrator - Abatonime
			If (-NOT ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) {
				if ($InstallRsat -eq $true) {
					Write-Error "Already attempted escalation. Aborting."
					return $false
				}
				write-host "Elevating privilages..."
				$arguments = "& '" + $myinvocation.mycommand.definition + " -InstallRsat'"
				Start-Process "$psHome\powershell.exe" -wait -Verb runAs -ArgumentList $arguments
				Import-Module ActiveDirectory -ErrorAction Stop
			} else {
				Get-WindowsCapability -Name RSAT* -Online | Add-WindowsCapability -Online
				Import-Module ActiveDirectory -ErrorAction Stop

				if ($InstallRsat -eq $true) {
					write-Verbose "Successfully installed module"
					return $true
				}
			}

		} catch {
			Write-Error "Unable to install RSAT: $_"
			return $riskyauths
		}
	}

	if ($EnrichLogs -eq $true) {
		$env:RiskySigninsAdEnrich = $true
	}

	if ($RemediateAccounts -eq $true) {
		# get's the password reset credential from a save file, or requests it from the user. be aware this is a blocking call. 
		$env:RiskySigninsRP = $true
		Set-RPCreds
	}

	if ($PostToSlack -eq $true) {
		# get's the slack uri from a save file, or requests it from the user. be aware this is a blocking call. 
		$env:RiskySigninsUseSlack = $true
		Set-SlackUri
		if ($RemediateAccounts -eq $true -and $env:RiskySigninsUseSlack -eq $true -and $env:RiskySigninsRP -eq $true) {
			Post-ToSlack "Auto-remediate enabled :pepe_box:"
		}
	}

	Write-Host "Running!"

	while ($true) {
		# get auth logs.
		# TODO: catch errors
		Write-Verbose "Getting $RECORD_COUNT logs"
		$auths = Get-MgAuditLogSignIn -Top $RECORD_COUNT
		Write-Verbose "Got $($auths.count) logs"

		if ($SaveAllLogs -eq $true) {
			$summarizedAllLogs = Summarize-LogData $auths
			# TODO: error handling
			$summarizedAllLogs | Add-Content -Path ($filepath + "-all.csv") -force
		}

		# Select new logs matching the risk and error code specifications.
		$newriskyauths = @()

		# TODO: handle long-running/large datasets
		$newriskyauths = $auths | ?{$_.risklevelduringsignin -in $RISK_LEVELS -and $_.id -notin $riskyauths.id}

		if ($ExcludeFalsePositives -eq $true) {
			$newriskyauths = Exclude-Events $newriskyauths $EXCLUDED_ERRORCODES
		}

		Write-Verbose "Got $($newriskyauths.count) new risky events"

		if ($newriskyauths.count -gt 0) {
			#TODO: truncate on high memory usage
			$riskyauths += $newriskyauths

			$summarizedLogs = Summarize-LogData $newriskyauths

			# process enrichment activites and update the Summarized logs var
			$enrichedLogs = @()
			try {
				$enrichedLogs = Get-AdEnrichment $summarizedLogs
				$summarizedLogs = $enrichedLogs
			} catch {
				write-error "Enrichment failed: $_"
			}
			
			if ($Silent -eq $false) {
				if ($Quiet -eq $false) {
					# print out pertinent details of new logs for analyst review
					Get-Date

					$summarizedLogs
				}
				# TODO: display metrics
			}

			$summarizedLogs | %{
				$log = $_
				write-verbose "processing $($log.UserPrincipalName)"

				if ($PostToSlack -eq $true) {
					# post individual logs as independant messages, enabling emoji responses
					# bulk post all new messages 
					#Post-ToSlack $summarizedLogs
					
					Post-ToSlack $log
				}

				if ($RemediateAccounts -eq $true) {
					if ($RemediateAccountsLogOnly -ne $false) {
						$rpsuccess = Reset-Password $log.UserPrincipalName -LogOnly
					} else {
						$rpsuccess = Reset-Password $log.UserPrincipalName
					}
					if ($rpsuccess -eq $true) {
						Write-Verbose "Remediated $($log.UserPrincipalName)"
						if ($PostToSlack -eq $true) {
							Post-ToSlack ":pepe_box: $($log.UserPrincipalName) remediated successfully"
						}
					} else {
						Write-Verbose "Failed to remediated $($log.UserPrincipalName) - Manual intervention Required."
						Post-ToSlack ":x: failed redeidation for $($log.UserPrincipalName) - manual intervention required :x:"
					}
				}
			}

			# save to disk TODO: syslog
			# TODO: error handling
			$summarizedLogs | Set-Content -Path $filepath -Force

			if ($SaveCliXml -eq $true) {
				# TODO: more sane file extension replacement process
				$riskyauths | Export-CliXml -Path ($filepath + ".xml") -Force 
			}
		}

		Write-Verbose "Sleeping $DELAY_TIME seconds"
		sleep $DELAY_TIME
	}

	$ed = get-date
	write-verbose "Done"
	write-verbose $ed
} # Monitor-RiskySignins

function Summarize-LogData ($LogData) {
	return $LogData | select UserId,@{n='timestamp (local)';e={(get-date $_.CreatedDateTime).tolocaltime()}},userprincipalname,appdisplayname,risklevelduringsignin,@{n="status-errorcode";e={$_.status.errorcode}},@{n="status-dets";e={$_.status.additionaldetails}},@{n="status-failurereason";e={$_.status.failurereason}},ipaddress
} # Summarize-LogData

function Post-ToSlack ($str, [switch]$NoRetry=$false) {
	if ($env:RiskySigninsUseSlack -eq $false) {
		Write-Verbose "Post-ToSlack called but previously determined not to do so. `$env:RiskySigninsUseSlack = $env:RiskySigninsUseSlack"
		return
	}

	try {
		if ($str.gettype().name -eq 'String') {
			$body = '{"text": "'
			$body += $str
			$body += '"}'
		} else {
			$body = Build-SlackBody $str
		}

	} catch {
		Write-Warning "Unable to build Slack Message Body, will not post to Slack: $_"
		return
	}

	try {
		$ret = Invoke-WebRequest -UseBasicParsing -Uri (Decode-SecureString $Global:RiskySigninsSlackUri) -Method Post -ContentType 'application/json' -Body $body -ErrorAction Stop
		write-verbose "Slack Post Return Code: $($ret.StatusCode)"
	} catch {
		Write-Warning "Unable to post to Slack: $_"
		if ($NoRetry -eq $true) {
			$env:RiskySigninsUseSlack = $false
			throw $_
		} else {
			Set-SlackUri -Fresh
			try {
				$ret = Invoke-WebRequest -UseBasicParsing -Uri (Decode-SecureString $Global:RiskySigninsSlackUri) -Method Post -ContentType 'application/json' -Body $body -ErrorAction Stop
				write-verbose "Slack Post Return Code: $($ret.StatusCode)"
			} catch {
				Write-Error "Still unable to post to Slack: $_"
				$env:RiskySigninsUseSlack = $false
			}
		}
	}
} # Post-ToSlack

function Build-SlackBody ($data) {
	$ret = '{"blocks": [{"type": "divider"}'

	$baseBlock = ',{"type": "section","fields": [{"type": "mrkdwn","text": ":RISKICON: RISKVALUE"},{"type": "mrkdwn","text": "TIMESTAMP"},{"type": "mrkdwn","text": "<https://portal.azure.com/#view/Microsoft_AAD_UsersAndTenants/UserProfileMenuBlade/~/SignIns/userId/USERIDVALUE/hidePreviewBanner~/true|UPNVALUE (AAD Sign-in Logs)>"},{"type": "mrkdwn","text": "ERRORCODEVALUE | STATUSFAILUREREASON"},{"type": "mrkdwn","text": "<https://ipinfo.io/IPVALUE|IPVALUE (IpInfo.io)>"},{"type": "mrkdwn","text": "APPDISPLAYNAME"},{"type": "mrkdwn","text": "PRIMARYAFFVALUE"}]}'
	$divider = ',{"type": "divider"}'

	$data | %{
		$item = $_;

		$block = $baseBlock

		# select icons/verbiage for risk indicators
		if ($item.RiskLevelDuringSignIn -eq 'high') {
			$riskicon = "red_circle"
			$riskvalue = "HIGH"
		} elseif ($item.RiskLevelDuringSignIn -eq 'medium') {
			$riskicon = "large_yellow_circle"
			$riskvalue = "Medium"
		} elseif ($item.RiskLevelDuringSignIn -eq 'low') {
			$riskicon = "large_green_circle"
			$riskvalue = "Low"
		} elseif ($item.RiskLevelDuringSignIn -eq 'none') {
			$riskicon = "white_circle"
			$riskvalue = "None"
		} else {
			$riskicon = "question"
			$riskvalue = $item.RiskLevelDuringSignIn
		}

		$block = $block.replace("RISKICON",$riskicon).replace("RISKVALUE",$riskvalue).replace("TIMESTAMP",$item.'timestamp (local)').replace("USERIDVALUE",$item.UserId).replace("UPNVALUE",$item.UserPrincipalName).replace("ERRORCODEVALUE",$item.'status-errorcode').replace("STATUSFAILUREREASON",$item.'status-failurereason').replace("IPVALUE",$item.IpAddress).replace("APPDISPLAYNAME",$item.AppDisplayName).replace("PRIMARYAFFVALUE",$(if ($item.IdAutoPersonPrimaryAffiliation -eq $none -or $item.IdAutoPersonPrimaryAffiliation -eq '') {"None"} else {"$($item.IdAutoPersonPrimaryAffiliation)"} ) )

		$ret += $block
		$ret += $divider
	}

	$ret += ']}'

	return $ret
} # Build-SlackBody

function Set-SlackUri ([Switch]$Fresh = $false, $UriFilePath = "$Global:SaveDirectory\RiskySigninsSlackUri.dat") {
	$str = ""
	$hostname = "$(hostname)"
	$username = "$($env:username)"
	$str += "$username"
	$str += "@"
	$str += "$hostname "
	$ip = (Get-NetIPAddress -Type Unicast -PrefixOrigin Dhcp,Manual).ipaddress -join "/"
	$str += "($ip) "
	$str += "now configured for Slack and monitoring for Risky Signins."

	$uri = ""
	if ($Fresh -eq $false) {
		try {
			$uri = Get-Content $UriFilePath -ErrorAction Stop | ConvertTo-SecureString
		} catch {
			$uri = ""
		}
	}

	if ($Fresh -eq $true -or $uri -eq "") {
		$uri = Read-Host -AsSecureString "Enter the Slack Webhook URI"
		Microsoft.PowerShell.Security\convertfrom-securestring $uri | Set-Content -Force -Path $UriFilePath
	}
	$Global:RiskySigninsSlackUri = $uri
	Write-Verbose "Captured Slack Webhook URI"
	try {
		Post-ToSlack $str -NoRetry
	} catch {
		Write-Warning "Failed to successfully set Slack URI: $_"
	}
	if ($env:RiskySigninsUseSlack -eq $false) {
		Write-Error "Failed to successfully set Slack URI"
		$Global:RiskySigninsSlackUri = $none
		Remove-Item $UriFilePath -Force
	}
} # Set-SlackUri

function Decode-SecureString ([System.Security.SecureString]$SecurePassword) {
	$UnsecurePassword = ""
	try {
		$BSTR = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($SecurePassword)
		$UnsecurePassword = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($BSTR)
		[Runtime.InteropServices.Marshal]::ZeroFreeBSTR($BSTR)
	} catch {
		write-error "Failed to decode Secure String: $_"
	}
	return $UnsecurePassword
} # Decode-SecureString

function Set-RPCreds ([Switch]$Interactive=$true, $CredsFilePath = "$Global:SaveDirectory\RiskySigninsRP.dat") {
	$Global:RPCreds = $none
	$env:RiskySigninsRP = $true

	try {
		$Global:RPCreds = Import-CliXml -Path $CredsFilePath -erroraction stop -warningaction stop
	} catch {
		$Global:RPCreds = $none
	}

	if ($Global:RPCreds-eq $none -and $Interactive -eq $true) {
		try {
			$Global:RPCreds = Get-Credential -Message "Enter API credentials for resetting passwords. These will be stored here: $CredsFilePath"
			$Global:RPCreds | Export-CliXml -Force -Path $CredsFilePath
		} catch {
			$Global:RPCreds = $none
		}
	}

	if ($Global:RPCreds-eq $none) {
		Write-Verbose "Unable to collect credentials. Unable to proceed with password resets."
		$env:RiskySigninsRP = $false
	}
} # Set-RPCreds

# valid keyFields are emlid, email, and netid
function Reset-Password {
	[CmdletBinding()]
	param (
		[Parameter(Mandatory=$true, ValueFromPipeline=$true)]
		$KeyValue, 
		[ValidateSet("netid","emplid","email")]
		$KeyField = "email", 
		[Switch]$LogOnly = $true, 
		$ResetUri = "https://myid.csub.edu/api/rest/restpoints/.main/resetPassword?keyField=KEYFIELDVALUE&keyValue=KEYVALUEVALUE&logOnly=LOGONLYVALUE",
		$ContentType = 'application/json',
		[ValidateSet("Post","Get")]
		$Method = 'Post'
	)
	$ret = $false

	write-verbose "$(if ($LogOnly -eq $True) {'(LOG ONLY) '} else {''})Requested to reset password for user with this $KeyField - $KeyValue"
	if ($env:RiskySigninsRP -ne $True) {
		Write-Verbose "Previously determined not to reset passwords based on `$env:RiskySigninsRP = $env:RiskySigninsRP"
		return $ret
	}

	try {
		$uri = $ResetUri
		$uri = $uri.replace("KEYFIELDVALUE", $KeyField).replace("KEYVALUEVALUE", $KeyValue).replace("LOGONLYVALUE", $LogOnly.ToString().ToLower())
		write-verbose "Request URI: $uri"
		try {

			$res = Invoke-WebRequest -UseBasicParsing -ContentType $ContentType -Method $Method -Verbose -Credential $Global:RPCreds -Uri $uri -ErrorAction Stop 
			write-verbose "Response: $res"
			write-verbose "Response Content: $($res.content)"
			if ($res.StatusCode -eq 200) {
				$obj = ConvertFrom-Json $res.content
				if ($obj.recordCount -ne 1) {
					Write-Error $obj
					throw "Error - recordCount not 1: ($($obj.recordCount))"
				}
				$response = $obj.records.0
				if ($response.result -ne 'Success') {
					Write-Error $obj
					throw "Error - result was not 'success': ($response.result))"
				} else {
					Write-Verbose "$(if ($LogOnly -eq $True) {'(LOG ONLY) '} else {''})Successfully reset password for $KeyValue"
					$ret = $true
				}
			}
		} catch {
			Write-Error "Unable to reset password for $KeyValue - $_"
			return $ret
		}

	} catch {
		Write-Error "Unable to set Request URI for Password Reset: $_"
		return $ret
	}

	return $ret
} # Reset-Password

function Get-AdEnrichment ($logs, $attributes = @('idAutoPersonPrimaryAffiliation','employeeNumber','passwordLastSet') ) {
	$ret = @()
	if ($env:RiskySigninsAdEnrich -eq $false) {
		write-verbose "previously determined to stop gathering enrichment"
		return $logs
	}

	try {
		$logs | %{
			$log = $_
			$netid = $log.UserPrincipalName.split("@",2)[0]
			$adu = get-aduser $netid -pr $attributes -erroraction stop
			$attributes | %{
				$log | add-member -notepropertyname $_ -notepropertyvalue ($adu.$_) -force
			}
			$ret += $log;
		}
	} catch {
		Write-Warning "Error on AD enrichment: $_"
		$env:RiskySigninsAdEnrich = $false
	}

	return $ret
} # Get-AdEnrichment

function Set-TrustedSites () { # UNTESTED
	### must add trusted sites to internet explorer (yes, that one):
	##### https://*.microsoftonline.com
	##### https://*.msftauth.net
	##### https://*.duosecurity.com
	##### https://login.live.com

	$sites = @("microsoftonline.com","msftauth.net","duosecurity.com","login.live.com")
	push-location
	$sites | %{
		$url = $_
		Set-Location "HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\Domains"

		New-Item $url
		Set-Location $url

		New-ItemProperty . -Name http -Value 2 -Type DWORD
	}
	pop-location
} # Set-TrustedSites

Export-ModuleMember -Function Monitor-RiskySignins 


### Reference chrisdee https://github.com/chrisdee/Scripts/blob/master/PowerShell/Working/certificates/GenerateCertificateSigningRequest(CSR).ps1
function Generate-Csr {
	param(
		[string]
		$CN=""
	)

	## PowerShell Script to generate a Certificate Signing Request (CSR) using the SHA256 (SHA-256) signature algorithm and a 4096 bit key size (RSA) via the Cert Request Utility (certreq) ##

	<#
	.SYNOPSIS
	This powershell script can be used to generate a Certificate Signing Request (CSR) using the SHA256 signature algorithm and a 4096 bit key size (RSA). Subject Alternative Names are supported.
	.DESCRIPTION
	Tested platforms:
	- Windows 7 with Powershell 2.0
	- Windows Server 2008R2 with PowerShell 2.0
	- Windows 8.1 with PowerShell 4.0
	- Windows 10 with PowerShell 5.0
	Created By:
	Reinout Segers
	Resource: https://pscsr256.codeplex.com
	Changelog
	v1.1
	- Added support for Windows Server 2008R2 and PowerShell 2.0
	v1.0
	- initial version
	#>



	#######################
	# Setting the variables
	#######################
	$UID = [guid]::NewGuid()
	$files = @{}
	$files['settings'] = "$($env:TEMP)\$($UID)-settings.inf";
	$files['csr'] = "C:\Users\$($env:username)\Desktop\$($UID)-csr.req"


	$request = @{}
	$request['SAN'] = @{}

	
	if ($CN -eq "") {
		Write-Host "Provide the Subject details required for the Certificate Signing Request" -ForegroundColor Yellow
		$CN = Read-Host "Common Name (CN)"
	}

	$request['CN'] = $CN
	$request['O'] = "California State University, Bakersfield"
	$request['OU'] = "Information Technology Services"
	$request['L'] = "Bakersfield"
	$request['S'] = "CA"
	$request['C'] = "US"

	###########################
	# Subject Alternative Names
	###########################
	$i = 0
	Do {
	$i++
	    $request['SAN'][$i] = read-host "Subject Alternative Name $i (e.g. alt.company.com / leave empty for none)"
	    if ($request['SAN'][$i] -eq "") {
	    
	    }
	    
	} until ($request['SAN'][$i] -eq "")

	# Remove the last in the array (which is empty)
	$request['SAN'].Remove($request['SAN'].Count)

	#########################
	# Create the settings.inf
	#########################
	$settingsInf = "
	[Version] 
	Signature=`"`$Windows NT`$ 
	[NewRequest] 
	KeyLength =  4096
	Exportable = TRUE 
	MachineKeySet = TRUE 
	SMIME = FALSE
	RequestType =  PKCS10 
	ProviderName = `"Microsoft RSA SChannel Cryptographic Provider`" 
	ProviderType =  12
	HashAlgorithm = sha256
	;Variables
	FriendlyName = $CN
	Subject = `"CN={{CN}}`"
	[Extensions]
	{{SAN}}
	;Certreq info
	;http://technet.microsoft.com/en-us/library/dn296456.aspx
	;CSR Decoder
	;https://certlogik.com/decoder/
	;https://ssltools.websecurity.symantec.com/checker/views/csrCheck.jsp
	"

	$request['SAN_string'] = & {
		if ($request['SAN'].Count -gt 0) {
			$san = "2.5.29.17 = `"{text}`"
	"
			Foreach ($sanItem In $request['SAN'].Values) {
				$san += "_continue_ = `"dns="+$sanItem+"&`"
	"
			}
			return $san
		}
	}

	$settingsInf = $settingsInf.Replace("{{CN}}",$request['CN']).Replace("{{SAN}}",$request['SAN_string'])

	# Save settings to file in temp
	$settingsInf > $files['settings']

	# Done, we can start with the CSR
	Clear-Host

	#################################
	# CSR TIME
	#################################

	# Display summary
	Write-Host "Certificate information
	Common name: $($request['CN'])
	Organisation: $($request['O'])
	Organisational unit: $($request['OU'])
	City: $($request['L'])
	State: $($request['S'])
	Country: $($request['C'])
	Subject alternative name(s): $($request['SAN'].Values -join ", ")
	Signature algorithm: SHA256
	Key algorithm: RSA
	Key size: 4096
	" -ForegroundColor Yellow

	certreq -new $files['settings'] $files['csr'] > $null

	# Output the CSR
	$CSR = Get-Content $files['csr']
	Write-Output $CSR
	$CSR | Out-File -FilePath "C:\Users\$($env:username)\Desktop\$($request['cn']).csr"
	Write-Host "
	"

	# Set the Clipboard (Optional)
	Write-Host "Copy CSR to clipboard? (y|n): " -ForegroundColor Yellow -NoNewline
	if ((Read-Host) -ieq "y") {
		$csr | clip
		Write-Host "Check your ctrl+v
	"
	}


	########################
	# Remove temporary files
	########################
	$files.Values | ForEach-Object {
	    Remove-Item $_ -ErrorAction SilentlyContinue
	}
}



$CurrentUser = New-Object Security.Principal.WindowsPrincipal $([Security.Principal.WindowsIdentity]::GetCurrent()) 
if (($CurrentUser.IsInRole([Security.Principal.WindowsBuiltinRole]::Administrator)) -eq $false) 
{ 
    $ArgumentList = "-noprofile -noexit -file `"{0}`" -Path `"$Path`" -MaxStage $MaxStage" 
    If ($ValidateOnly) { $ArgumentList = $ArgumentList + " -ValidateOnly" } 
    If ($SkipValidation) { $ArgumentList = $ArgumentList + " -SkipValidation $SkipValidation" } 
    If ($Mode) { $ArgumentList = $ArgumentList + " -Mode $Mode" } 
    Write-Host "elevating" 
    Start-Process powershell.exe -Verb RunAs -ArgumentList ($ArgumentList -f ($myinvocation.MyCommand.Definition)) -Wait 
    Exit 
}

Generate-Csr
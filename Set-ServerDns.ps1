
if ($false) {
	$CurrentUser = New-Object Security.Principal.WindowsPrincipal $([Security.Principal.WindowsIdentity]::GetCurrent()) 
	if (($CurrentUser.IsInRole([Security.Principal.WindowsBuiltinRole]::Administrator)) -eq $false) { 
	    $ArgumentList = "-noprofile -noexit -file `"{0}`" -Path `"$Path`" -MaxStage $MaxStage" 
	    If ($ValidateOnly) { $ArgumentList = $ArgumentList + " -ValidateOnly" } 
	    If ($SkipValidation) { $ArgumentList = $ArgumentList + " -SkipValidation $SkipValidation" } 
	    If ($Mode) { $ArgumentList = $ArgumentList + " -Mode $Mode" } 
	    Write-Host "elevating" 
	    Start-Process powershell.exe -Verb RunAs -ArgumentList ($ArgumentList -f ($myinvocation.MyCommand.Definition)) -Wait 
	    Exit 
	} 
}

$scriptBody={

	$log = ""
	$log += "Starting DNS script at $(Get-Date)"

	$config = (Get-WmiObject win32_networkadapterconfiguration -filter "ipenabled = 'true'" | where {-not (($_.description -match 'virtual') -or ($_.description -match 'loop') -or ($_.description -match 'vmware') )})

	if (!$config) {
		$log += "Error - Unable to get an interface: $err"
	} else {
		$log += "Got interface: $($config.Description) - $config"
		$log += "Interface IP: $($config.ipaddress)"
		$log += "Interface DNS: $($config.DNSServerSearchOrder)"

		$ret = $config.SetDNSServerSearchOrder(@("136.168.0.70","209.129.125.216","136.168.0.75","136.168.1.10"))
		if ($ret.returnvalue -ne 0) {
			$log += "Error: $($ret.returnvalue) - $ret"
		} else {
			$log += "Set DNS search order: $ret"
		}
	}

	$log += "Finished DNS script at $(Get-Date)"

	echo $log >> $env:temp\DNS-$(get-date -format "yyyyMMdd-HHmm").log

	return $log
}

$memberlist = (Get-ADComputer -SearchBase 'OU=Domain Servers,dc=ad,dc=csub,dc=edu' -Filter * -pr ipv4address | ?{$_.ipv4address -match "(136.168.|209.129.125.|198.189.181.)"} ).dnshostname | sort-object -unique

# start

if (1 -gt 0) {

	$jobs = @();
	$sessions = @();
	$errors = @{}

	if (0 -lt 1) {

		write-host "[.] Running on the servers..."

		$memberlist | %{ 
			$key = $_; try { $s = New-PSSession $key -ErrorAction Stop }  catch { $errors["$key"]="Unable to connect to $key : $_"; $s=$null; } ; 
			if ($s) { 
				try {

					if ('y' -eq 'y') {  
						$jobs += Invoke-Command -Session $s -AsJob -JobName $key -ScriptBlock $scriptBody
					} else {
						write-host "[.] Skipping $key"
					}  
				} catch { $errors["$key"]="Unable to complete on $key : $_"; }
				$sessions += $s; 
			} 
		}

		write-host "[.] Done with servers"
	}

	if ($jobs.count -gt 0) {
		Write-Host "[.] Waiting for jobs to finish..."
		while ($((get-job | ?{$_.id -in $jobs.id -and $_.state -eq 'Running'}).count) -gt 0) {
			sleep 1
		}

		if ($((get-job | ?{$_.id -in $jobs.id -and $_.state -ne 'Completed'}).count) -gt 0) {
			Write-Host "[!] There was an issue with one or more jobs. The jobs variable has all the job details and the sessions variable has the current sessions. Here are the incomplete jobs:"
			get-job | ?{$_.id -in $jobs.id -and $_.state -ne 'Completed'}
			$results = get-job | ?{$_.id -in $jobs.id -and $_.state -eq 'Completed'} | Receive-Job -AutoRemoveJob -Wait
		} else {
			Write-Host "[.] All jobs completed successfully."
			$results = get-job | ?{$_.id -in $jobs.id -and $_.state -eq 'Completed'} | Receive-Job -AutoRemoveJob -Wait
		}
	} else {
		Write-Host "[.] No jobs to await"
	}
	if ($sessions.count -gt 0) {
		Write-Host "[.] Clearing active sessions"
		$sessions | %{ $_ | remove-pssession }
	}
	write-host "Results: $results"
	write-host "Errors ($($errors.keys.count)): $($errors.values)"

	write-host "Writing log to $env:temp\DNS-$(get-date -format "yyyyMMdd-HHmm").log"
	echo $results >> $env:temp\DNS-$(get-date -format "yyyyMMdd-HHmm").log
	echo $errors.values >> $env:temp\DNS-$(get-date -format "yyyyMMdd-HHmm").log
}

# end

# Audit local group permissions
# 
# erichards1 - 20201208
# version 0.1
#
# optionally search for specific people - use netids, unsure if this function will ever attempt to return displaynames or emails
# default to serach the primary CSUB Server OU
# 
# Example: $res = Audit-LocalAccess -NetIds 'ewilliams'
# 		   $res.Membership
# 

function Audit-LocalAccess {
	[CmdletBinding()]
	param(
		[Parameter(ValueFromPipeline=$true,Position=0)]
		$NetIds = @(),
		$SearchBase = "OU=Domain Servers,DC=ad,DC=csub,DC=edu",
		$Credential = $null,
		$JobLimit = 25,
		[Switch] $JsonOutput = $false
	)

	Write-Verbose "[i] Audit-LocalAccess started"

	$targets = @()

	if ($NetIds -and $NetIds.Count -gt 0) {
		$ttype = $NetIds.GetType().Name
		if ($ttype -eq "String") {
			$targets += $NetIds.ToLower().Trim()
		} elseif ($ttype -eq "Object[]") {
			$NetIds | %{ $targets += $_.ToLower().Trim() }
		} else {
			throw "Unsupported parameter type: $ttype"
		}
	}

	if ($SearchBase -notlike "OU=*" -and $SearchBase -notlike "DC=*") {
		throw "Search Base appears to be malformed. Should begin with 'OU=' or 'DC='."
	}

	$jobs = @()
	$sessions = @()

	$sleeptime = 2
	$sleepiter = 0

	$avoidlist = @( # skip the AD servers since these return domain groups, rather than local groups
		"136.168.1.39", # AD2
		"136.168.1.10", # AD3
		"136.168.1.72", # AD4
		"209.129.125.203", # AD5
		"209.129.125.202", # AD6
		"10.181.1.8", # AD7
		"10.181.1.51" # AD8
	)

	$absstart = Get-Date
	$avgtime = 1.0

	Write-Verbose "[i] Starting scanning"

	if ($Credential -ne $null) {
		$computers = Get-AdComputer -Filter * -SearchBase "$SearchBase" -Properties IPv4Address -Credential $Credential
	} else {
		$computers = Get-AdComputer -Filter * -SearchBase "$SearchBase" -Properties IPv4Address
	}
	$i = 0
	$computercount = $computers.Count

	foreach ($computer in $computers) {
		Write-Progress -Activity "Initiating enumeration jobs ($($i + 1) / $($computercount))" -Status "Working... Average time per host: $([math]::round($avgtime,2)) seconds" -PercentComplete ($i * 100.0/ $computercount) -SecondsRemaining ($avgtime * ($computercount - $i))
		$starttime = get-date
		$ip = $computer.IPv4Address
		Write-Verbose "[i] Checking $ip ..."
		if ($ip -notin $avoidlist) {
			$sleepiter = 0
			while ($(get-job | ?{$_.Id -in $jobs.Id -and $_.State -eq 'Running'}).count -ge $JobLimit) { # TODO: expand state control
				if ($sleepiter -eq 0) {
					Write-Verbose "[i] Sleeping" -NoNewline
				} else {
					Write-Verbose "." -NoNewline
				}
				$sleepiter += 1
				sleep $sleeptime
			}
			if ($sleepiter -gt 0) {
				Write-Verbose " Resuming after $($sleepiter * $sleeptime) seconds"
			}

			try {
				$ComputerName = $computer.DNSHostName
			} catch {
				$ComputerName = $null
			}

			if ($ComputerName -and $ComputerName -ne '') {
				Write-Verbose "[i] Resolved $ip to $ComputerName"
				Write-Verbose "[i] Opening PsSession to $ComputerName ..."
				$sess = $null
				$err = $null
				try {
					if ($Credential -ne $null) {
						$sess = New-PsSession -Computer $ComputerName -Authentication Negotiate -ErrorAction SilentlyContinue -ErrorVariable err -Credential $Credential
					} else {
						$sess = New-PsSession -Computer $ComputerName -Authentication Negotiate -ErrorAction SilentlyContinue -ErrorVariable err
					}
				} catch {
					$sess = $null
					$err = $_
				}

				if ($sess) {
					$sessions += $sess
					$sb = {
						param($ComputerName,$ip,$targets)

						$ret = @()
					    $Computer = [ADSI]"WinNT://$ComputerName"
					                    
					    $Localgroups= (Get-WMIObject win32_group -filter "LocalAccount='True'" -ComputerName $ComputerName).Name
					    
					    foreach ($groupname in $Localgroups ) {
					        $group =[ADSI]"WinNT://$ComputerName/$groupname" 
					        $members = @($group.psbase.Invoke("Members"))
					        
					        foreach ($m in $members) {
				                $obj = (New-Object psobject -Property @{
				                    GroupName = $groupname
				                    ComputerName = $ComputerName
				                    IP = $ip
				                    Members = $m.GetType().InvokeMember("Name", 'GetProperty', $null, $m, $null)
				                })
				                if ($targets -and $targets.Count -gt 0) {
				                	$match = $False
				                	foreach ($target in $targets) {
				                		foreach ($member in $obj.Members) {
				                			if ($target -eq $member.ToLower().Trim()) { # TODO: shortcircuit this awful loop
				                				$match = $True
				                			}
				                		}
				                	}
				                	if ($match -eq $True) {
				                		$ret += $obj
				                	}
				                } else {
				                	$ret += $obj
				                }
				            }
					    }
					    return $ret
					}
					Write-Verbose "[i] Starting collection job"
					$job = Invoke-Command -Session $sess -AsJob -JobName "$ComputerName" -ScriptBlock $sb -ArgumentList $ComputerName,$ip,$targets
					$jobs += $job

				} else {
					Write-Verbose "[e] Unable to create a remote session on $Computer with error $err"
				}
			} else {
				Write-Verbose "[e] No DNS name for $ip"
			}
		} else {
			Write-Verbose "[w] Skipping $ip as it's in our avoidlist"
		}
		$avgtime = ($avgtime + (new-timespan -end (get-date) -start $starttime).TotalSeconds) / 2.0
		$i += 1
	}
	Write-Verbose "[i] Done launching jobs. Checking if all jobs are complete..."
	$sleepiter = 0
	while ($(get-job | ?{$_.Id -in $jobs.Id -and $_.State -eq 'Running'}).count -ge $JobLimit) { # TODO: expand state control
		if ($sleepiter -eq 0) {
			Write-Verbose "[i] Waiting for jobs to complete" -NoNewline
		} else {
			Write-Verbose "." -NoNewline
		}
		$sleepiter += 1
		sleep $sleeptime
	}
	if ($sleepiter -gt 0) {
		Write-Verbose " All jobs complete."
	} else {
		Write-Verbose "[i] All jobs complete."
	}
	$results = @()
	foreach ($job in $jobs) {
		$obj = (New-Object psobject -Property @{
			Name = "$($job.Name)"
			Membership = Get-Job $job.Id | Receive-Job -AutoRemoveJob -Wait
		})
		if (($obj.Membership) -and ($obj.Membership.Count -gt 0)) {
			$results += $obj
		}
	}

	Get-PSSession | ?{$_.Id -in $sessions.Id} | Remove-PSSession
	Write-Verbose "[i] Collected $($results.count) results from $($jobs.count) jobs after scanning $($computercount) hosts"
	if ($JsonOutput -eq $true) {
		Write-Verbose "[i] Converting output to straight JSON"
		$results = $results.membership | select ComputerName,GroupName,Members | sort -unique computername,groupname,members | convertto-json
	}
	Write-Verbose "[i] Total time elapsed: $((new-timespan -start $absstart -end (get-date)).tostring())"
	return $results 
}

Import-Module -Force Utilities -WarningAction SilentlyContinue # Add a try and get/install operation here

function SS-Auth {
	[CmdletBinding()]
	param(
		$Credentials = $null,
		$OTP = "",
		$WebServicesUrl = 'https://arcane.csub.edu/SecretServer/webservices/sswebservice.asmx',
		$ServerName = "",
		$DomainName = "CSUB-AD",
		$proxy=$null
	)

	$token = $null

	$domain = $DomainName

	if ($proxy -eq $null) {
		if ($ServerName -and $ServerName.Length -gt 2 -and !$WebServicesUrl) {
			$url = "https://$ServerName/SecretServer/webservices/sswebservice.asmx"
		} else {
			$url = $WebServicesUrl
		}
		try {
			Write-Verbose "Connecting to '$url'"
			$proxy = New-WebServiceProxy -uri $url -UseDefaultCredential -ErrorAction SilentlyContinue -ErrorVariable err
			if (!$proxy -or $err) {
				Write-Log "url=$url Unable connect to URL in SS-Auth: $err" -Source "SecretServer" -level "Error"
				throw $err
			}
		} catch {
			Write-Log "url=$url Unable connect to URL ($url) in SS-Auth: $_" -Source "SecretServer" -level "Error"
			throw "Bad URL? url: $url error: $_"
		}
		Write-Verbose "Connected"
	} else {
		Write-Verbose "Using existing proxy"
	}

	if (!$Credentials) {
		Write-Verbose "Collecting credentials interactively"
		$creds = Get-Credential -Message "Enter your Secret Server Credentials" -UserName $Env:username
		$mfa = Read-Host -Prompt "Enter your MFA OTP, if applicable"
	} else {
		Write-Verbose "Credentials were passed at the command line"
		$creds = $Credentials
		$mfa = $OTP
	}
	if ($creds.UserName -match '@') {
		($user,$domain) = ($creds.UserName -split '@',2)
		if (($domain -notmatch 'csub-ad') -and ($domain -notmatch 'ad-csub') -and ($domain -notmatch 'csub.edu')) {
			Write-Log "domain=$domain Unknown domain. You shouldn't need to include a domain name in your credentials, but I'll accept csub.edu or CSUB-AD (the latter is the 'Domain' according to Secret Server)." -Source "SecretServer" -level "Error"
			throw "Unknown domain ($domain). You shouldn't need to include a domain name in your credentials, but I'll accept csub.edu or CSUB-AD (the latter is the 'Domain' according to Secret Server)."
		}
	}

	if (!$mfa -or $mfa -eq "") {
		Write-Verbose "Authenticating as '$($creds.GetNetworkCredential().UserName)' in domain '$domain'"
		$result1 = $proxy.Authenticate($creds.GetNetworkCredential().UserName, $creds.GetNetworkCredential().Password, '', $domain)
	} else {
		Write-Verbose "Authenticating as '$($creds.GetNetworkCredential().UserName)' in domain '$domain' using an OTP"
		$result1 = $proxy.AuthenticateRADIUS($creds.GetNetworkCredential().UserName, $creds.GetNetworkCredential().Password, '', $domain, $mfa)
	}

	$i=0
	$maxattempts=3
	while ($result1.Errors[0] -match 'Invalid Duo pin code' -and $i -lt $maxattempts) {
		$i=$i+1
		$mfa = Read-Host -Prompt "Invalid Duo pin code (try $i/$maxattempts). Enter your MFA OTP again"
		$result1 = $proxy.AuthenticateRADIUS($creds.GetNetworkCredential().UserName, $creds.GetNetworkCredential().Password, '', $domain, $mfa)
	}

	if ($result1.Errors.length -gt 0) {
		Write-Log "url=$url username=$($creds.GetNetworkCredential().UserName) domain=$domain Unable to authenticate to Secret Server ($url) in SS-Auth: $($result1.Errors[0])" -Source "SecretServer" -level "Warning"
		throw "An error occurred during authentication: $($result1.Errors[0])"
	} else {
		Write-Log "url=$url username=$($creds.GetNetworkCredential().UserName) domain=$domain $($creds.GetNetworkCredential().UserName) successfully authenticated to Secret Server ($url) with domain $domain" -Source "SecretServer"
		$token = $result1.Token
	}

	return $token
}

function Get-SecretId {
	[CmdletBinding()]
	param(
		$SecretName = "My Secret",
		$Credentials = $null,
		$OTP = "",
		$WebServicesUrl = 'https://arcane.csub.edu/SecretServer/webservices/sswebservice.asmx',
		$ServerName = "",
		$DomainName = "CSUB-AD",
		$proxy = $null,
		$token = $null
	)

	$ret = @()

	if ($proxy -eq $null) {
		if ($ServerName -and $ServerName.Length -gt 2 -and !$WebServicesUrl) {
			$url = "https://$ServerName/SecretServer/webservices/sswebservice.asmx"
		} else {
			$url = $WebServicesUrl
		}
		try {
			Write-Verbose "Connecting to '$url'"
			$proxy = New-WebServiceProxy -uri $url -UseDefaultCredential -ErrorAction SilentlyContinue -ErrorVariable err
			if (!$proxy -or $err) {
				Write-Log "url=$url Unable to connect to Secret Server ($url) in Get-SecretId: $err" -Source "SecretServer" -level "Error"
				throw $err
			}
		} catch {
			Write-Log "url=$url Unable to connect to Secret Server ($url) in Get-SecretId: $_" -Source "SecretServer" -level "Error"
			throw "Bad URL? url: $url error: $_"
		}
		Write-Verbose "Connected"
	} else {
		Write-Verbose "Using existing proxy"
	}

	if ($token -eq $null) {
		try {
			$token = SS-Auth -Credentials $Credentials -OTP $OTP -DomainName $DomainName -proxy $proxy
		} catch {
			Write-Error "Unable to authenticate in Get-SecretId: $_"
			Write-Log "Unable to authenticate in Get-SecretId: $_" -Source "SecretServer" -level "Warning"
			$token = $null
			return $ret
		}
	}

	Write-Verbose "Getting secret Id for $SecretName"

	$Secrets = $proxy.SearchSecrets($token,$SecretName,$false,$false)

	if (!$Secrets -or $Secrets.SecretSummaries.count -lt 1) {
		Write-Verbose "No matching secrets"
		$ret = -1
	} elseif ($Secrets.SecretSummaries.count -eq 1) {
		Write-Verbose "One Match: $($Secrets.SecretSummaries.SecretId)"
		$ret = $Secrets.SecretSummaries.SecretId
	} else {
		Write-Warning "$($Secrets.SecretSummaries.count) Secrets returned, returning the closest match"
		Write-Log "$($Secrets.SecretSummaries.count) Secrets returned, returning the closest match" -level "Warning" -source "SecretServer"
		$min = 99999
		$minid = -1
		$Secrets.SecretSummaries | %{
			Write-Verbose "$_"
			Write-Verbose "$($_ | select *)"
			$d = Get-LevenshteinDistance $SecretName $_.SecretName
			if ($d -lt $min) {
				$min = $d
				$minid = $_.SecretId
			}
		}
		$ret = $minid
	}
	Write-Log "secretname=$SecretName secretid=$ret Search for $SecretName complete" -source "SecretServer"

	return $ret
}

function Get-FolderId {
	[CmdletBinding()]
	param(
		$FolderName = "My Folder",
		$Credentials = $null,
		$OTP = "",
		$WebServicesUrl = 'https://arcane.csub.edu/SecretServer/webservices/sswebservice.asmx',
		$ServerName = "",
		$DomainName = "CSUB-AD",
		$proxy = $null,
		$token = $null
	)

	$ret = $null

	if ($proxy -eq $null) {
		if ($ServerName -and $ServerName.Length -gt 2 -and !$WebServicesUrl) {
			$url = "https://$ServerName/SecretServer/webservices/sswebservice.asmx"
		} else {
			$url = $WebServicesUrl
		}
		try {
			Write-Verbose "Connecting to '$url'"
			$proxy = New-WebServiceProxy -uri $url -UseDefaultCredential -ErrorAction SilentlyContinue -ErrorVariable err
			if (!$proxy -or $err) {
				Write-Log "url=$url Unable to connect to Secret Server ($url) in Get-SecretId: $err" -Source "SecretServer" -level "Error"
				throw $err
			}
		} catch {
			Write-Log "url=$url Unable to connect to Secret Server ($url) in Get-SecretId: $_" -Source "SecretServer" -level "Error"
			throw "Bad URL? url: $url error: $_"
		}
		Write-Verbose "Connected"
	} else {
		Write-Verbose "Using existing proxy"
	}

	if ($token -eq $null) {
		try {
			$token = SS-Auth -Credentials $Credentials -OTP $OTP -DomainName $DomainName -proxy $proxy
		} catch {
			Write-Error "Unable to authenticate in Get-SecretId: $_"
			Write-Log "Unable to authenticate in Get-SecretId: $_" -Source "SecretServer" -level "Warning"
			$token = $null
			return $ret
		}
	}

	Write-Verbose "Getting Folder Id for $FolderName"

	$Folders = $proxy.SearchFolders($token,$FolderName)

	if (!$Folders -or $Folders.Folders.count -lt 1) {
		Write-Verbose "No matching folders"
		$ret = -1
	} elseif ($Folders.Folders.count -eq 1) {
		Write-Verbose "One Match: $($Folders.Folders.Id)"
		$ret = $Folders.Folders.Id
	} else {
		Write-Warning "$($Folders.Folders.count) folders returned, returning the closest match"
		Write-Log "$($Folders.Folders.count) folders returned, returning the closest match" -level "Warning" -source "SecretServer"
		$min = 99999
		$minid = -1
		$Folders.Folders | %{
			$d = Get-LevenshteinDistance $FolderName $_.Name
			if ($d -lt $min) {
				$min = $d
				$minid = $_.Id
			}
		}
		$ret = $minid
	}
	Write-Log "foldername=$FolderName folderid=$ret Search for $FolderName complete" -source "SecretServer"

	return $ret
}

function Get-Secret {
	[CmdletBinding()]
	param(
		$SecretName = "",
		$SecretId = "5246",
		$Credentials = $null,
		$OTP = "",
		[Switch] $AsCredential,
		$WebServicesUrl = 'https://arcane.csub.edu/SecretServer/webservices/sswebservice.asmx',
		$ServerName = "",
		$DomainName = "CSUB-AD",
		$proxy = $null,
		$token = $null
	)

	$ret = $null

	if ($proxy -eq $null) {
		if ($ServerName -and $ServerName.Length -gt 2 -and !$WebServicesUrl) {
			$url = "https://$ServerName/SecretServer/webservices/sswebservice.asmx"
		} else {
			$url = $WebServicesUrl
		}
		try {
			Write-Verbose "Connecting to '$url'"
			$proxy = New-WebServiceProxy -uri $url -UseDefaultCredential -ErrorAction SilentlyContinue -ErrorVariable err
			if (!$proxy -or $err) {
				Write-Log "url=$url Unable to connect to Secret Server ($url) in Get-Secret: $err" -Source "SecretServer" -level "Error"
				throw $err
			}
		} catch {
			Write-Log "url=$url Unable to connect to Secret Server ($url) in Get-Secret: $_" -Source "SecretServer" -level "Error"
			throw "Bad URL? url: $url error: $_"
		}
		Write-Verbose "Connected"
	} else {
		Write-Verbose "Using existing proxy"
	}

	if ($token -eq $null) {
		try {
			$token = SS-Auth -Credentials $Credentials -OTP $OTP -DomainName $DomainName -proxy $proxy
		} catch {
			Write-Error "Unable to authenticate in Get-Secret: $_"
			Write-Log "Unable to authenticate in Get-Secret: $_" -Source "SecretServer" -level "Warning"
			$token = $null
			return $ret
		}
	}

	if ((!$SecretId -or $SecretId -eq "") -and $SecretName -and $SecretName.length -gt 1) {
		Write-Log "secretname=$SecretName secretid=$SecretId username=$($Credentials.username) retrieving secret id from passed secret name" -source "SecretServer"
		$SecretId = Get-SecretId $SecretName -proxy $proxy -token $token
	}

	if (!$SecretId -or $SecretId -eq -1) {
		Write-Error "Not sure what secret to get. Sorry!"
		Write-Log "secretname=$SecretName secretid=$SecretId username=$($Credentials.username) Unable to get a secret id matching the passed secret name. this could be a permissions issue with the secret, or the secret may not exist" -level "Warning" -source "SecretServer"
		return $ret
	}

	Write-Verbose "Getting secret $SecretId"
	$result2 = $proxy.GetSecret($token, $SecretId, $false, $null)
	if ($result2.Errors.length -gt 0) {
		if ($result2.Errors[0] -match 'Bad token') {
			Write-Verbose "Bad token in Get-Secret. Trying to rectify..."
			try {
				$token = SS-Auth -Credentials $Credentials -OTP $OTP -DomainName $DomainName -proxy $proxy
			} catch {
				Write-Error "Unable to authenticate in Get-Secret: $_"
				Write-Log "Unable to authenticate in Get-Secret: $_" -Source "SecretServer" -level "Warning"
				$token = $null
				return $ret
			}
			$result2 = $proxy.GetSecret($token, $SecretId, $false, $null)
			if ($result2.Errors.length -gt 0) {
				Write-Log "secretid=$SecretId username=$($Credentials.GetNetworkCredential().UserName) Unable to retrieve secret number $SecretId in Get-Secret after recreating the access token: $($result2.Errors[0])" -Source "SecretServer" -level "Error"
				throw "An error occurred retreiving secret number $SecretId : $($result2.Errors[0])"
			}
		} else {
			Write-Log "secretid=$SecretId username=$($Credentials.GetNetworkCredential().UserName) Unable to retrieve secret number $SecretId in Get-Secret: $($result2.Errors[0])" -Source "SecretServer" -level "Error"
			throw "An error occurred retreiving secret number $SecretId : $($result2.Errors[0])"
		}
	} else {
		Write-Log "secretid=$SecretId username=$($Credentials.GetNetworkCredential().UserName) secretname=$($result2.Secret.Name) Got secret '$($result2.Secret.Name)'" -Source "SecretServer"
		Write-Verbose "Got secret '$($result2.Secret.Name)'"
		if ($AsCredential) {
			Write-Verbose "Constructing a PSCredential object"
			$user = $null
			$pass = $null
			Write-Verbose "Processing $($result2.Secret.Items.Count) fields"
			$result2.Secret.Items | %{ $item = $_; try { if ($item.IsPassword) { $pass = ConvertTo-SecureString $item.Value -AsPlainText -Force; } elseif (($item.FieldName -match 'username') -or ($item.FieldName -match 'login') -or ($item.FieldName -match 'email') -or ($item.FieldName -match 'account')) { $user = $item.Value; } elseif (($item.FieldName -match 'appid') -or ($item.FieldName -match 'app id') -or ($item.FieldName -match 'application id')) { $user = $item.Value; } } catch { Write-Verbose "Error handling a secret field in item $item : $_" } }
			if (!$user -or !$pass) {
				throw "Unable to extract a username and password from the secret. I had the following choices: $($result2.Secret.Items.FieldName -join ', ')"
			}
			Write-Verbose "Decided on username: $user"
			$ret = New-Object System.Management.Automation.PSCredential ($user,$pass)
			if (!$ret) {
				throw "Unable to create a credential object. not sure why."
			}
		} else {
			Write-Verbose "Constructing an object with as many attributes as I can"
			Write-Verbose "Processing $($result2.Secret.Items.Count) fields"
			$ret = New-Object -TypeName psobject

			$result2.Secret.Items | %{ $item = $_; try { if ($item.IsPassword) { $pass = ConvertTo-SecureString $item.Value -AsPlainText -Force ; $ret | Add-Member -MemberType NoteProperty -Name $item.FieldName -Value $pass -ErrorAction SilentlyContinue ; } else {  $ret | Add-Member -MemberType NoteProperty -Name $item.FieldName -Value $item.Value -ErrorAction SilentlyContinue -ErrorVariable err; if ($err) { throw "$err" } } if (($item.FieldName -match 'username') -or ($item.FieldName -match 'login') -or ($item.FieldName -match 'email') -or ($item.FieldName -match 'account')) { $user = $item.Value; } elseif (($item.FieldName -match 'appid') -or ($item.FieldName -match 'app id') -or ($item.FieldName -match 'application id')) { $user = $item.Value; } } catch { Write-Verbose "Error handling a secret field in item $item : $err" } }

			if (!$user -or !$pass) {
				throw "Unable to extract a username and password from the secret. I had the following choices: $($result2.Secret.Items.FieldName -join ', ')"
			}
			Write-Verbose "Decided on username: $user"
			$ret | Add-Member -MemberType NoteProperty -Name "Credentials" -Value (New-Object System.Management.Automation.PSCredential ($user,$pass))
		}
	}

	return $ret
}

function ConvertFrom-SecureString {
	param(
		[Parameter(Mandatory=$true,ValueFromPipeline=$true,Position=0)]
		[System.Security.SecureString]
		$SecurePassword
	)
	$BSTR = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($SecurePassword)
	$UnsecurePassword = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($BSTR)
	return $UnsecurePassword
}

function Get-SecretTemplate {
	[CmdletBinding()]
	param(
		$Credentials = $null,
		$OTP = "",
		$WebServicesUrl = 'https://arcane.csub.edu/SecretServer/webservices/sswebservice.asmx',
		$ServerName = "",
		$DomainName = "CSUB-AD",
		$proxy = $null,
		$token = $null,
		$TemplateName="Active Directory Account"
	)

	$ret = $null

	if ($TemplateName -eq "") {
		return $ret
	}

	if ($proxy -eq $null) {
		if ($ServerName -and $ServerName.Length -gt 2 -and !$WebServicesUrl) {
			$url = "https://$ServerName/SecretServer/webservices/sswebservice.asmx"
		} else {
			$url = $WebServicesUrl
		}
		try {
			Write-Verbose "Connecting to '$url'"
			$proxy = New-WebServiceProxy -uri $url -UseDefaultCredential -ErrorAction SilentlyContinue -ErrorVariable err
			if (!$proxy -or $err) {
				Write-Log "url=$url Unable to connect to Secret Server ($url) in Get-SecretTemplate: $err" -Source "SecretServer" -level "Error"
				throw $err
			}
		} catch {
			Write-Log "url=$url Unable to connect to Secret Server ($url) in Get-SecretTemplate: $_" -Source "SecretServer" -level "Error"
			throw "Bad URL? url: $url error: $_"
		}
		Write-Verbose "Connected"
	} else {
		Write-Verbose "Using existing proxy"
	}

	if ($token -eq $null) {
		try {
			$token = SS-Auth -Credentials $Credentials -OTP $OTP -DomainName $DomainName -proxy $proxy
		} catch {
			Write-Error "Unable to authenticate in Get-SecretTemplate: $_"
			Write-Log "Unable to authenticate in Get-SecretTemplate: $_" -Source "SecretServer" -level "Warning"
			$token = $null
			return $ret
		}
	}

	Write-Verbose "Getting secret template $TemplateName"
    $res = $proxy.GetSecretTemplates($token)
	if ($res.Errors.length -gt 0) {
		Write-Error "templatename=$TemplateName Unable to retrieve template in Get-SecretTemplate: $($res.Errors)"
		Write-Log "templatename=$TemplateName Unable to retrieve template in Get-SecretTemplate: $($res.Errors)" -Source "SecretServer" -level "Warning"
		$ret = $null
	} else {
    	Write-Verbose "Got $($res.SecretTemplates.count) templates"
    	$ret = $res.SecretTemplates | ?{$_.Name -eq "$TemplateName"}
	}
	Write-Verbose "Got Template: '$($Template.Name)' ($($Template.Id))"
    return $ret
}

function Generate-Secret {
	[CmdletBinding()]
	param(
		$Credentials = $null,
		$OTP = "",
		$WebServicesUrl = 'https://arcane.csub.edu/SecretServer/webservices/sswebservice.asmx',
		$ServerName = "",
		$DomainName = "CSUB-AD",
		$proxy = $null,
		$token = $null,
		$TemplateName="",
		$Template=$null,
		$SecretData=@{}
	)

	$ret = @{}

	if ($proxy -eq $null) {
		if ($ServerName -and $ServerName.Length -gt 2 -and !$WebServicesUrl) {
			$url = "https://$ServerName/SecretServer/webservices/sswebservice.asmx"
		} else {
			$url = $WebServicesUrl
		}
		try {
			Write-Verbose "Connecting to '$url'"
			$proxy = New-WebServiceProxy -uri $url -UseDefaultCredential -ErrorAction SilentlyContinue -ErrorVariable err
			if (!$proxy -or $err) {
				Write-Log "url=$url Unable to connect to Secret Server ($url) in Create-Secret: $err" -Source "SecretServer" -level "Error"
				throw $err
			}
		} catch {
			Write-Log "url=$url Unable to connect to Secret Server ($url) in Create-Secret: $_" -Source "SecretServer" -level "Error"
			throw "Bad URL? url: $url error: $_"
		}
		Write-Verbose "Connected"
	} else {
		Write-Verbose "Using existing proxy"
	}

	if ($token -eq $null) {
		try {
			$token = SS-Auth -Credentials $Credentials -OTP $OTP -DomainName $DomainName -proxy $proxy
		} catch {
			Write-Error "Unable to authenticate in Create-Secret: $_"
			Write-Log "Unable to authenticate in Create-Secret: $_" -Source "SecretServer" -level "Warning"
			$token = $null
			return $ret
		}
	}

	if ($Template -eq $null -and $TemplateName -ne "") {
		$Template = Get-SecretTemplate -proxy $proxy -token $token -TemplateName $TemplateName
	}

	if ($Template -eq $null) {
		Write-Error "Unable to get a template to work off of with template name $TemplateName"
		Write-Log "templatename=$("$TemplateName" -replace " ","_") Unable to get a template to work off of" -Source "SecretServer" -level "Warning"
		return $ret
	}

	Write-Verbose "Generating '$($Template.Name)' ($($Template.Id)) secret"
	$ret["templateId"] = $Template.Id

	$fields = New-Object string[] $Template.Fields.Count; 
	$values = New-Object object[] $Template.Fields.Count; 
	$passwords = @(); # passwords is a list of indexes that will require decryption
	$i = 0;

	if ($SecretData.Keys.count -lt 1) { # assume interactive mode

		Write-Host "Enter the secret data manually. Does not support files at this time."

		$Template.Fields | %{
			$field = $_;
			if ($field.IsPassword -eq $false) {
				$fields[$i] = "$($field.Id)"
				$values[$i] = Read-Host "$($field.DisplayName)"
				$i += 1;
			} else {
				$cred = (Get-Credential -UserName "$($field.DisplayName)" -Message "Enter the password that should be in the '$($field.DisplayName)' field, or blank to automatically generate one using Secret Server")
				if ($cred.GetNetworkCredential().password -eq "") {
					Write-Host "Filling $($field.DisplayName) with a password generated by Secret Server."
					$cred = (New-Object System.Management.Automation.PSCredential ($cred.username,(convertto-securestring -asplaintext -force "$($proxy.GeneratePassword($token, $field.Id).GeneratedPassword)")))
				}
				$fields[$i] = "$($field.Id)"
				$values[$i] = $cred.password
				$passwords += $i
				$i += 1;
			}
		}
	} else {
		$Template.Fields | %{
			$field = $_;
			if ($field.DisplayName -in $SecretData.Keys) {
				Write-Verbose "Filling $($field.DisplayName) from provided secret data."
				$fields[$i] = "$($field.Id)"
				if ($field.IsPassword -eq $true) {
					if (($SecretData["$($field.DisplayName)"]).GetType().name -eq "String") {
						$pass = ConvertTo-SecureString $SecretData["$($field.DisplayName)"] -AsPlainText -Force
					} elseif (($SecretData["$($field.DisplayName)"]).GetType().name -eq "SecureString") {
						$pass = $SecretData["$($field.DisplayName)"]
					} else {
						Write-Warning "An unexpected data type was passed as a secret. This will result in unexpected behavior."
						$pass = ConvertTo-SecureString "$($SecretData["$($field.DisplayName)"])" -AsPlainText -Force
					}
					$values[$i] = $pass
					$passwords += $i
				} else {
					$values[$i] = $SecretData["$($field.DisplayName)"]
				}
				$i += 1;
			} elseif ($field.IsPassword -eq $true) { # a plain-text password was not passed (good practice). generate an encrypted one automatically using secret server's policy for this type of secret. 
				Write-Verbose "Filling $($field.DisplayName) with a password generated by Secret Server."
				$fields[$i] = "$($field.Id)"
				$values[$i] = (New-Object System.Management.Automation.PSCredential ("$($field.DisplayName)",(convertto-securestring -asplaintext -force "$($proxy.GeneratePassword($token, $field.Id).GeneratedPassword)"))).password
				$passwords += $i
				Write-Verbose "Generated password length: $($values[$i].length)"
				$i += 1;
			} else {
				Write-Verbose "Filling $($field.DisplayName) with an empty string."
				$fields[$i] = "$($field.Id)"
				$values[$i] = ""
				$i += 1;
			}
		}
	}

	$ret["fields"] = $fields
	$ret["values"] = $values
	$ret["passwords"] = $passwords

	return $ret
}

function Create-Secret {
	[CmdletBinding()]
	param(
		$Credentials = $null,
		$OTP = "",
		$WebServicesUrl = 'https://arcane.csub.edu/SecretServer/webservices/sswebservice.asmx',
		$ServerName = "",
		$DomainName = "CSUB-AD",
		$proxy = $null,
		$token = $null,
		$SecretName="",
		$TemplateName="Active Directory Account",
		$SecretData=@{},
		$FolderId=-1,
		$FolderName=""
	)

	$ret = $null

	if ($proxy -eq $null) {
		if ($ServerName -and $ServerName.Length -gt 2 -and !$WebServicesUrl) {
			$url = "https://$ServerName/SecretServer/webservices/sswebservice.asmx"
		} else {
			$url = $WebServicesUrl
		}
		try {
			Write-Verbose "Connecting to '$url'"
			$proxy = New-WebServiceProxy -uri $url -UseDefaultCredential -ErrorAction SilentlyContinue -ErrorVariable err
			if (!$proxy -or $err) {
				Write-Log "url=$url Unable to connect to Secret Server ($url) in Create-Secret: $err" -Source "SecretServer" -level "Error"
				throw $err
			}
		} catch {
			Write-Log "url=$url Unable to connect to Secret Server ($url) in Create-Secret: $_" -Source "SecretServer" -level "Error"
			throw "Bad URL? url: $url error: $_"
		}
		Write-Verbose "Connected"
	} else {
		Write-Verbose "Using existing proxy"
	}

	if ($token -eq $null) {
		try {
			$token = SS-Auth -Credentials $Credentials -OTP $OTP -DomainName $DomainName -proxy $proxy
		} catch {
			Write-Error "Unable to authenticate in Create-Secret: $_"
			Write-Log "Unable to authenticate in Create-Secret: $_" -Source "SecretServer" -level "Warning"
			$token = $null
			return $ret
		}
	}

	Write-Verbose "Token: $token"

	if ($SecretName -eq "") {
		$SecretName = Read-Host "Enter a secret name"
	}

	if ($FolderName -ne "") {
		$FolderId=4088
	}

	$Secret = Generate-Secret -proxy $proxy -token $token -TemplateName $TemplateName -SecretData $SecretData
	if ($Secret.Keys.count -lt 2) {
		Write-Error "No Secret Data Generated. Aborting."
		Write-Log "No Secret Data Generated. Aborting." -source "SecretServer" -level "Error"
		return
	}

	$fields = $Secret["fields"]
	$values = $Secret["values"]
	$passwords = $Secret["passwords"]

	Write-Verbose "Got $($fields.count) fields, $($values.count) values, and $($passwords.count) encrypted passwords"

	$passwords | %{
		# for each value that is an encrypted password, decrypt it
		Write-Verbose "Decrypting index $_"
		$values[$_] = ConvertFrom-SecureString $values[$_]
	}

    $addResult = $proxy.AddSecret($token, $Secret["templateId"], $SecretName, $fields, $values, $FolderId)

    if ($addResult.Errors.Count -gt 0) {
		Write-Log "secretname=$("$SecretName" -replace " ","_") foldername=$("$FolderName" -replace " ","_") folderid=$FolderId templatename=$("$TemplateName" -replace " ","_") Names have had spaces replaced with underscords. Unable to create secret in Create-Secret: $($addResult.Errors)" -Source "SecretServer" -level "Warning"
		Write-Error "Unable to create secret in Create-Secret: $($addResult.Errors)"
    } else {
		Write-Log "secretname=$("$SecretName" -replace " ","_") foldername=$("$FolderName" -replace " ","_") folderid=$FolderId templatename=$("$TemplateName" -replace " ","_") secretid=$($addResult.Secret.Id) Names have had spaces replaced with underscords. Successfully created secret $SecretName" -Source "SecretServer"
		Write-Verbose "Successfully created secret $SecretName"
		$ret = $addResult.Secret.Id
    }
	return $ret
}

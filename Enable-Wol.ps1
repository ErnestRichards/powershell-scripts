
### 0. Check compatibility
$log_file = $env:temp + "\dellEnableWol.log"
echo "Log File: "
echo $log_file
start-sleep -s 5
echo "Script start: " >> $log_file
Get-Date >> $log_file
$s = gwmi win32_computersystem
if ($s.manufacturer -ne 'dell inc.' -or ($s.model -notlike '*precision workstation t3400*' -and $s.model -ne 'optiplex 7010')) {
	echo "Not a Precision T3400 or an Optiplex 7010" >> $log_file
	write-host This script is tested compatible with Dell Optiplex 7010 and Dell Precision WorkStation T3400.
	write-host Press Ctrl-C to cancel now, or else Ill just try anyway. You have 60 seconds.
	Start-Sleep -s 60 

}

### 1. Get Admin

### http://stackoverflow.com/questions/7690994/powershell-running-a-command-as-administrator - Abatonime
If (-NOT ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator"))
{   
echo "Elevating privilages..." >> $log_file
$arguments = "& '" + $myinvocation.mycommand.definition + "'"
Start-Process "$psHome\powershell.exe" -Verb runAs -ArgumentList $arguments

break
}

echo "Running as administrator" >> $log_file


### 2. Dell BIOS Configs

try {
$bios = gwmi -namespace root\dcim\sysman DCIM_BIOSService
$bios.SetBIOSAttributes($null,$null,"Wake-On-LAN","4") 
$bios.SetBIOSAttributes($null,$null,"Deep Sleep Control","2")
} catch {
	try {
	echo "Must install sma.exe to continue configuring BIOS" >> $log_file
copy-item -literalpath \\nas.csub.edu\Departments\IT\sma.exe -destination c:\ -force
C:\sma.exe /s 
Start-Sleep -s 180 
$bios = gwmi -namespace root\dcim\sysman DCIM_BIOSService
$bios.SetBIOSAttributes($null,$null,"Wake-On-LAN","4") 
$bios.SetBIOSAttributes($null,$null,"Deep Sleep Control","2")
	echo "BIOS Config complete" >> $log_file
} catch {
	echo "unable to configure BIOS" >> $log_file
	Write-Host "Dell BIOS Configurations Failed"
Start-Sleep -s 60 
}
rm c:\sma.exe -force
}

### 3. NIC Configs

$name = [System.Net.NetworkInformation.NetworkInterface]::GetAllNetworkInterfaces() | where {$_.description -like '*intel*'}
if ($name) { ### Intel Nic
	echo "Intel NIC" >> $log_file
	try { if ( -not (Test-Path "C:\Program Files\Intel\IntelNetCmdlets\IntelNetCmdlets.psd1")) {
		echo "Installing Intel Powershell commandlets" >> $log_file
copy-item -literalpath \\nas.csub.edu\Departments\IT\prowin64 -destination c:\ -recurse -force

			Start-Sleep -s 120 
C:\prowin64\APPS\PROSETDX\Winx64\DxSetup.exe /quiet /qn /norestart POWERSHELL=1
	Start-Sleep -s 300
	}
	import-module "C:\Program Files\Intel\IntelNetCmdlets\IntelNetCmdLets"
	set-intelnetadaptersetting -name $name.description -displayname "Wake on Magic Packet" -displayvalue "Enabled"
	set-intelnetadaptersetting -name $name.description -displayname "Wake on Pattern Match" -displayvalue "Enabled"
	set-intelnetadaptersetting -name $name.description -displayname "Wake on Magic Packet from power off state" -displayvalue "Enabled"
	echo "Successfully configured NIC" >> $log_file
	Write-Host "Success!"
Start-Sleep -s 60 
	} catch {
		echo "Intel NIC Configurations Failed" >> $log_file
		Write-Host "Intel NIC Configurations Failed"
Start-Sleep -s 60 
	}
rmdir c:\prowin64 -recurse -force
} else {
	$name = [System.Net.NetworkInformation.NetworkInterface]::GetAllNetworkInterfaces() | where {$_.description -like '*broadcom*'}

	if ($name) { ### Broadcom NIC
	echo "Broadcom NIC" >> $log_file
		try {
		if ( -not (Test-Path "C:\Program Files\Broadcom\BACS\BACSCli.exe")) {
			echo "Installing Broadcom CLI" >> $log_file
copy-item -literalpath \\nas.csub.edu\Departments\IT\bacs -destination C:\ -recurse -force

			Start-Sleep -s  120
C:\bacs\setup.exe /s /v"/qn NO_DRIVER=Y FRESHINSTALL=true"


			Start-Sleep -s 180 
		}
		$cmds = '(echo list ndis & echo cfg advanced "Wake Up Capabilities"="Both" & echo q) | "C:\program files\broadcom\bacs\bacscli.exe"'
		cmd /c $cmds

	echo "NIC Configured" >> $log_file
	Write-Host "Success!"

Start-Sleep -s 60 
		} catch {
	echo "Broadcom NIC configuration failed" >> $log_file
		Write-Host "Broadcom NIC Configurations Failed"
Start-Sleep -s 60 
		}
rmdir c:\bacs -recurse -force
		
	} else { ### Unknown NIC
		Write-Host "I am not familiar with this type of NIC. Manual intervention and script modification required!"
		echo "I am not familiar with this type of NIC. Manual intervention and script modification required!" >> $log_file
Start-Sleep -s 60 
	}

}
set-executionpolicy default
echo "Script complete. Restarting for good measure. Stop this restart from a command prompt with 'shutdown /a'" >> $log_file
echo "Script complete. Restarting for good measure. Stop this restart from a command prompt with 'shutdown /a'"
shutdown /r /t 300 /c "administrative restart to force bios changes to take effect. Stop this restart from a command prompt with 'shutdown /a'"

echo "Script end: " >> $log_file
Get-Date >> $log_file

echo " " >> $log_file
echo "=============================================" >> $log_file
echo " " >> $log_file

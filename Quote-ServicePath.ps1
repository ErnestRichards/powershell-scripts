### Quote-ServicePath.ps1
### erichards1
### 20221118


$LogFile = "$($Env:temp)\Quote-ServicePath.log"
$Timeout = 30 # seconds, max wait after an escalation to complete tasks and return results
$ActionLog = @()

if ($args.count -lt 1) { # ensure at least one service name was passed
	Write-Host "Pass one (or more) service names to try to quote their binary paths"
	return "Pass one (or more) service names to try to quote their binary paths"
}

Write-Output "Startup $(Get-Date)" >> $LogFile

### Thanks, Hashbrown https://serverfault.com/questions/11879/gaining-administrator-privileges-in-powershell
#at top of script
if (!
    #current role
    (New-Object Security.Principal.WindowsPrincipal(
        [Security.Principal.WindowsIdentity]::GetCurrent()
    #is admin?
    )).IsInRole(
        [Security.Principal.WindowsBuiltInRole]::Administrator
    )
) {
    #elevate script and exit current non-elevated runtime
    Write-Host 'escalating'
    Start-Process `
        -FilePath 'powershell' `
        -ArgumentList (
            #flatten to single array
            '-File', $MyInvocation.MyCommand.Source, $args `
            | %{ $_ }
        ) `
        -Verb RunAs
    
    ### try to track results and return them, even in the case where we need to escalate

	Write-Output "Escalated $(Get-Date)" >> $LogFile
	sleep 1
    $i = 0
	$lastlogdata = ""
    While ($i -lt $Timeout) {
    	$i += 1
    	$logdata = ""
    	try {
    		$logdata = (gc $LogFile) -join "`n"
    		$lastlogdata = $logdata.substring($logdata.lastindexof("Startup"))
    		if (($lastlogdata -split "`n")[-2] -match "^Shutdown") {
    			return "$lastlogdata"
    		}
    	} catch {}

    	sleep 1
    }
    return "Exceeded timeout ($Timeout). Last known log data: $lastlogdata"
}

$args | %{ # process multiple service names, if passed
	$serviceName = $_

	Write-Host "Working on service: $serviceName"
	$ActionLog += "Working on service: $serviceName"

	try {
		$results = cmd /c "sc qc `"$serviceName`""

		if ($results -match "Does not exist") { # idiot checks - does the service exist, no other error occurred
			Write-Host "The service does not exist"
			$ActionLog += "The service does not exist"
		} elseif ($results -match "FAILED") {
			Write-Host "Querying for the service failed:`n$results"
			$ActionLog += "Querying for the service failed:`n$results"
		} else {
			$servicePath = ($results.split("`n") | ?{$_ -match "BINARY_PATH"}).split(":",2)[1].trim() # this could fail if there's no binary path specified, or perhaps while processing the string
			if ($servicePath -match "^`".*`"$") { # if the path is already quoted, don't proceed
				Write-Host "Service Path already appears to be quoted: $servicePath"
				$ActionLog += "Service Path already appears to be quoted: $servicePath"
			} else {
				Write-Host "Service Path needs to be quoted: $servicePath"
				$ActionLog += "Service Path needs to be quoted: $servicePath"
				$newServicePath = "\`"$servicePath\`""
				$changeResults = cmd /c "sc config `"$serviceName`" binpath= `"$newServicePath`""
				if ($changeResults -match "SUCCESS") { # sanity check - command succeeded
					Write-Host "Successfully Changed Service"
					$ActionLog += "Successfully Changed Service"
				} else {
					Write-Host "Failed to Change Service"
					$ActionLog += "Failed to Change Service"
				}
				$newResults = cmd /c "sc qc `"$serviceName`""
				$checkServiceName = ($newResults.split("`n") | ?{$_ -match "BINARY_PATH"}).split(":",2)[1].trim() # again, this could fail, possibly, though probably not
				if ($newServiceName.replace("\`"","`"") -eq $checkServiceName) { # validate changed path matches desired path
					Write-Host "Quoted Service Name Confirmed: $checkServiceName"
					$ActionLog += "Quoted Service Name Confirmed: $checkServiceName"
				} else {
					Write-Host "Quoted Service Name Not Confirmed: $checkServiceName"
					$ActionLog += "Quoted Service Name Not Confirmed: $checkServiceName"
				}
			}
		}
	} catch {
		Write-Host "An unhandled exception occurred while handling $serviceName : $_"
		$ActionLog += "An unhandled exception occurred while handling $serviceName : $_"
	}
}

Write-Output "$($ActionLog -join "`n")" >> $LogFile
Write-Output "Shutdown $(Get-Date)`n" >> $LogFile

Read-Host "Press Enter to Continue"

return "$($ActionLog -join "`n")"

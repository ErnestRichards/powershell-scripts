function New-DuoAdmin {
	[CmdletBinding()]
	param (
        [Parameter(Mandatory=$true,ValueFromPipeline=$true,Position=0)]
		[string]$Netid,
		[string]$Phone="",
		[ValidateSet("Owner","Administrator","Application Manager","User Manager","Help Desk","Billing","Read-only")]
		[string]$Role="Help Desk",
		[Switch]$Restricted=$false,
		[String]$Suffix="-admin",
		[String]$Domain="csub.edu"
	)

	$admin = $null

	$existinguser = duoGetUser -username "$Netid" -erroraction stop

	$existingadminuser = duoGetUser -username "$Netid" -erroraction stop

	Write-Verbose "[.] Got existing user $($existinguser.username)"

	if (($existinguser.phones.count -lt 1 -or "$($existinguser.phones[0].number)".length -lt 10) -and $Phone.length -lt 10) {
		Write-Error "Existing user account has no phones. Pass a phone number manually or update the existing user account."
		Write-Log "netid=$Netid Existing user account has no phones. Pass a phone number manually or update the existing user account." -level "Error" -Source "Admin-Lifecycle"
		return $admin
	} else {
		if ($Phone) {
			$number = $Phone
		} else {
			$number = $existinguser.phones[0].number
		}
	}

	Write-Verbose "[.] Determined to use phone number $number"

	$name = ""

	if ($existinguser.RealName.length -gt 2) {
		$name += $existinguser.RealName
	} elseif ($existinguser.firstname.length -gt 1 -and $existinguser.lastname.length -gt 1) {
		$name += "$($existinguser.firstname) $($existinguser.lastname)"
	} else {
		$name += $Netid
	}

	$name += " Admin"

	$email = "$Netid"
	$email += "$Suffix"
	$email += "@"
	$email += "$Domain"

	Write-Verbose "[.] Determined to use name '$name'"
	Write-Verbose "[.] Determined to use email '$email'"

	try {
		if ($Restricted -eq $true) {
			$admin = duoCreateAdmin -email "$email" -name "$name" -role "$Role" -restricted_by_admin_units "true" -phone "$number" -erroraction stop
		} else {
			$admin = duoCreateAdmin -email "$email" -name "Test $name Admin" -role "$Role" -phone "$number" -erroraction stop
		}
	} catch {
		Write-Error "Unable to create the admin account for $name : $_"
		Write-Log "netid=$Netid name=$name email=$email role=$role phone=$number Unable to create the admin account for $name : $_" -level "Error" -Source "Admin-Lifecycle"
		$admin = $null
	}

	return $admin
}

# valid types are ita, itc, and is
function New-Admin {
	[CmdletBinding()]
	param (
        [Parameter(Mandatory=$true,Position=0)]
		[string]$Netid,
		[string]$Phone="",
		[string]$Type="ITA",
		$Credentials = $null, # should not be MFA enforced, specifically for Secret Server
		[string]$Notes = "Created with Admin-Lifecycle:New-Admin",
		[string]$Suffix = "-admin",
		[string]$Domain = "csub.edu"
	)

	$admin = $null

	Write-Verbose "Importing modules"

	try {
		import-module Utilities -Force -ErrorAction stop -WarningAction SilentlyContinue # Add a try and get/install operation here
		import-module ActiveDirectory -force -erroraction stop -WarningAction SilentlyContinue # Add a try and get/install operation here
		import-module Get-Secret -force -erroraction stop -WarningAction SilentlyContinue # Add a try and get/install operation here
		import-module duo -force -erroraction stop -WarningAction SilentlyContinue # Add a try and get/install operation here
	} catch {
		Write-Error "Unable to import a required module. Ensure that Utilities, ActiveDirectory, Get-Secret, and duo are installed in the Documents\WindowsPowerShell\Modules\<Module Name> directory of the account running this script. More details in the error meesage. Aborting. Error: $_"
		Write-Log "Unable to import a required module. Ensure that Utilities, ActiveDirectory, Get-Secret, and duo are installed in the Documents\WindowsPowerShell\Modules\<Module Name> directory of the account running this script. More details in the error meesage. Aborting. Error: $_" -level "Error" -Source "Admin-Lifecycle"
		return $admin
	}

	Write-Verbose "Checking for existing AD Account with netid $Netid"

	try {
		if ($Credentials -eq $null) {
			$existingaduser = Get-AdUser $Netid -Properties DisplayName -ErrorAction Stop
		} else {
			$existingaduser = Get-AdUser $Netid -Properties DisplayName -ErrorAction Stop -Credentials $Credentials -AuthType Negotiate
		}
	} catch {
		$existingaduser = $null
	}

	if ($existingaduser -eq $null) {
		Write-Error "Unable to locate an existing AD user with netid $Netid. I will not proceed."
		Write-Log "Unable to locate an existing AD user with netid $Netid. I will not proceed." -level "Error" -Source "Admin-Lifecycle"
		return $admin
	}

	Write-Verbose "Calculating name, membership, role, restricted, admin netid, and admin email..."

	$Name = "$($existingaduser.DisplayName)"
	if ($Name.Length -lt 3) {
		$Name = $Netid
	}

	if ($Type.ToLower() -eq 'ita') {
		$memberof = $('ITA-Admin','DuoUsers','2StepUsers','adhoc_o365','gpitss')
		$role = "Help Desk"
		$restricted = $True
	} elseif ($Type.ToLower() -eq 'itc') {
		$memberof = $('ITC-Admin','DuoUsers','2StepUsers','adhoc_o365','gpitss')
		$role = "Help Desk"
		$restricted = $True
	} elseif($Type.ToLower() -eq 'is') {
		$memberof = $('IS-Admin','DuoUsers','2StepUsers','adhoc_o365','gpnoc')
		$role = "Administrator"
		$restricted = $False
	} else {
		Write-Error "Invalid Admin Type: $Type. Should be ITA, ITC, or IS." 
		Write-Log "Invalid Admin Type: $Type. Should be ITA, ITC, or IS." -level "Error" -Source "Admin-Lifecycle" 
		return $admin
	}

	$adminnetid = "$Netid"
	$adminnetid += "$Suffix"
	$adminnetid += "@"
	$adminnetid += "$Domain"

	$adminname = "$Name"
	$adminname += "$Suffix"

	Write-Verbose "Creating account: $adminnetid"

	try {
		if ($Credentials -eq $null) {
			$admin = Create-AdAccount -netid $adminnetid -name $adminname -memberof $memberof
		} else {
			$admin = Create-AdAccount -netid $adminnetid -name $adminname -memberof $memberof -runascreds $Credentials
		}
	} catch {
		Write-Error "An unexpected error occurred calling Create-AdAccount. This could be caused by missing dependencies or network connectivity issues. I cannot proceed past this point. Here's the error: $_"
		Write-Log "An unexpected error occurred calling Create-AdAccount. This could be caused by missing dependencies or network connectivity issues. I cannot proceed past this point. Here's the error: $_" -level "Error" -Source "Admin-Lifecycle"
		return $null
	}

	if ($admin -eq $null) {
		Write-Error "Unable to create an AD user. I will not proceed."
		Write-Log "Unable to create an AD user. I will not proceed." -level "Error" -Source "Admin-Lifecycle"
		return $admin
	}

	Write-Verbose "Using produced attributes to generate secret data..."
	
	$sd = @{}
	$sd["username"] = "$($admin.UserPrincipalName)"
	$sd["domain"] = "$(($admin.UserPrincipalName -split '@')[1])"
	$sd["notes"] = "$Notes"
	$sd["password"] = $admin.Password # should be a secure string
	$sn = "$Name Admin AD Account"

	Write-Verbose "Adding the information to Secret Server..."
	
	try {
		if ($Credentials -eq $null) {
			$secretid = Create-Secret -Credentials $Credentials -SecretName $sn -SecretData $sd
		} else {
			$secretid = Create-Secret -Credentials $Credentials -SecretName $sn -SecretData $sd -Credentials $Credentials
		}
	} catch {
		Write-Error "An unexpected error occurred calling Create-Secret. This could be caused by missing dependencies or network connectivity issues. I will try to proceed. Here's the error: $_"
		Write-Log "An unexpected error occurred calling Create-AdAccount. This could be caused by missing dependencies or network connectivity issues. I will try to proceed. Here's the error: $_" -level "Error" -Source "Admin-Lifecycle"
	}

	if ($secretid -eq $null) {
		Write-Warning "Unable to create a Secret in Secret Server for $Netid. Check the logs for more information."
		Write-Log "netid=$Netid adminnetid=$adminnetid Unable to create a Secret in Secret Server for $Netid. Check the logs for more information." -level "Warning" -Source "Admin-Lifecycle"
	} else {
		$admin | Add-Member -NotePropertyName "SecretName" -NotePropertyValue $sn -Force -PassThru | Add-Member -NotePropertyName "SecretId" -NotePropertyValue $secretid -Force 
	}

	Write-Verbose "Creating a Duo Admin Account..."

	try {
		$duoadmin = New-DuoAdmin -Netid "$Netid" -Phone "$Phone" -Suffix "$Suffix" -Domain "$Domain" -Restricted:$restricted
	} catch {
		Write-Error "An unexpected error occurred calling Create-Secret. This could be caused by missing dependencies or network connectivity issues. I will try to proceed. Here's the error: $_"
		Write-Log "An unexpected error occurred calling Create-AdAccount. This could be caused by missing dependencies or network connectivity issues. I will try to proceed. Here's the error: $_" -level "Error" -Source "Admin-Lifecycle"
	}

	if ($duoadmin -eq $null) {
		Write-Warning "Unable to create a Duo Admin account for $Netid. Check the logs for more information."
		Write-Log "netid=$Netid Unable to create a Duo Admin account for $Netid. Check the logs for more information." -level "Warning" -Source "Admin-Lifecycle"
	} else {
		$admin | Add-Member -NotePropertyName "DuoUsername" -NotePropertyValue $duoadmin.username -Force -PassThru | Add-Member -NotePropertyName "DuoEmail" -NotePropertyValue $duoadmin.email -Force 
	}

	Write-Log "netid=$Netid adminnetid=$adminnetid Created an admin account in AD: $($admin -ne $null) Created a Secret in Secret Server: $($secretid -ne $null) Created a Duo Admin Account: $($duoadmin -ne $null)" -Source "Admin-Lifecycle"
	
	Write-Verbose "Created an admin account in AD: $($admin -ne $null)`nCreated a Secret in Secret Server: $($secretid -ne $null)`nCreated a Duo Admin Account: $($duoadmin -ne $null)"
	Write-Verbose "Done!"

	return $admin
}

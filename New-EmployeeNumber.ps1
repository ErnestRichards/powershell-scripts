function New-EmployeeNumber {
	[CmdletBinding()]
	param(
		[int]$Quantity = 1, # how many employeeNumbers should be generated
		$BaseEmployeeNumber = "000665861", # employeeNumber to start the search at. should be a 9-digit number starting with 0. 
		[int]$MaxErrors = 3, # maximum number of errors to hit before giving up.
		[int]$MaxIterations = 999, # maximum number of iterations to run before giving up.
		[int]$MaxEmployeeNumber = 99999999 # maximum possible employeeNumber
	)
	$iter = 0
	$errct = 0
	$returnList = @()

	# sanity check. We'll work off of numbers, then cast back to a string for the return. 
	if ($BaseEmployeeNumber -is [string] -or $BaseEmployeeNumber -is [int]) {
		$BaseEmployeeNumber = [int]($BaseEmployeeNumber)
	} else {
		Write-Error "BaseEmployeeNumber must be a number or a string, similar to a real employeenumber, ie '000665861' or '665862'."
		return $null
	}
	if (-not ($Quantity -is [int]) -or $Quantity -lt 1 -or $Quantity -gt 1000000) { # 1000000 is an arbitrary limit
		Write-Error "Quantity must be a reasonable integer."
		return $null
	}

	$CurrentEmployeeNumber = $BaseEmployeeNumber
	Write-Verbose "Searching for a valid candidate employeeNumber starting at '$CurrentEmployeeNumber'"
	Write-Debug "MaxErrors: $MaxErrors"
	Write-Debug "MaxIterations: $MaxIterations"
	Write-Debug "MaxEmployeeNumber: $MaxEmployeeNumber"

	# search all searchbase linearly 
	while ($returnList.Count -lt $Quantity) {
		$iter += 1
		if ($iter -ge $MaxIterations) {
			Write-Error "Reached maximum iterations. Try passing a different '-BaseEmployeeNumber'. Giving up."
			return $null
		}

		# otherwise unused but potentially valid ID numbers that PeopleSoft may issue begin with a '0' and are evenly divisble by 13 - bypass these candidates
		if (($CurrentEmployeeNumber % 13) -eq 0) {
			Write-Debug "Skipping otherwise potentially valid ID number ($CurrentEmployeeNumber)"
			$CurrentEmployeeNumber += 1
			continue
		}

		# abort if we start trying invalid numbers
		if ($CurrentEmployeeNumber -lt 1 -or $CurrentEmployeeNumber -gt $MaxEmployeeNumber) {
			Write-Error "Generated invalid candidate employeeNumber. Aborting."
			return $null
		}

		# check for existing users with this employeeNumber
		try {
			$candidate = "{0:d9}" -f $CurrentEmployeeNumber
			Write-Debug "Iteration $iter - Testing candidate $CurrentEmployeeNumber ($candidate)"
			$users = (get-aduser -filter {employeenumber -eq $candidate} -pr employeenumber -ErrorAction Stop).employeenumber
			if ($users.count -gt 1) {
				Write-Verbose "Warning - More than one user with the same employeeNumber ($($users.count)): $CurrentEmployeeNumber"
			}
			if ($users.count -gt 0) {
				Write-Debug "Candidate ($candidate) is taken. Iterating."
				$CurrentEmployeeNumber += 1
			} elseif ($candidate -in $returnList) {
				Write-Debug "Candidate ($candidate) is already proposed. Iterating and incrementing error count."
				$CurrentEmployeeNumber += 1
				$errct += 1
				if ($errct -ge $MaxErrors) {
					Write-Error "Maximum tolerable errors reached. Check that you have network connectivity, AD access, and permissions to search. Additionaly, try checking Debug and Verbose information since this threshold was reached while adding Candidates to the returnList."
					return $null
				}
			} else {
				Write-Verbose "Found a valid candidate employeeNumber: $CurrentEmployeeNumber"
				$returnList += ("{0:d9}" -f $CurrentEmployeeNumber)
				$CurrentEmployeeNumber += 1
			}
		} catch {
			Write-Error "Error when searching for users by employeenumber ($CurrentEmployeeNumber): $_"
			$errct += 1
			if ($errct -ge $MaxErrors) {
				Write-Error "Maximum tolerable errors reached. Check that you have network connectivity, AD access, and permissions to search."
				return $null
			}
		}
	}

	Write-Debug "final iter: $iter`nfinal errct: $errct`nfinal returnlist size: $($returnList.count)"
	Write-Verbose "Done!"
	return $returnList
}

# Ernest Richards
# TODO:
# add TLS 1.3 support
# add debug/verbose logging options
# add a saftey/simulation argument to disable any real changes
# add more cmd line args/processing/parsing
# optimize user storage, stored user processing
# add more secure/long-term/legit data storage
# account for faculty vs staff employment differences: 
#      if previously faculty and no longer a duo reqd user (fac or staff), store for one year before removing. 
#      if staff and never faculty, remove as currently processed. 
# add some parallel processing for group membership gathering from AD while other non-AD processing is occuring in process-messages (i think)
# normalize commenting style
# add a Get-SessionToken function
# add parallel processing of messages?

[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12 # until 1.3 comes around
Add-Type -AssemblyName System.Web # req for the api call to graylog

function Write-Log ($message, $level = "Information", $eventid = 1337 ) {
    Write-EventLog -LogName Application -Source "Auto-GroupChange" -EntryType $level -EventId $eventid -Message $message
}

# searchtype is relative or absolute.
# groups is a list of groups to base the query on. in this case, I'm concerned about additions or removals from 'staff' or 'faculty'
# relative requires a timeframe (seconds ago, default 25 hours) and absolute requires a from/to as yyyy-MM-ddTHH:mm:ss.SSSZ (e.g. 2014-01-23T15:34:49.000Z) or yyyy-MM-dd HH:mm:ss
# fields is a list of good fields. not sure what'll happen if you pass bad values.
# MemberName = the user that was modified, TargetUserName = the name of the group the user was added to, SubjectUserName = the user that did the modification, EventID = ...
# streamid is the ... stream ID. AD is default. User creds only have rights to ad stream.

function Construct-Url($timeframe=25*60*60, $query="(EventID:4728 OR EventID:4729)", $groups=@('staff','faculty'), $fields="source,EventID,MemberName,TargetUserName,SubjectUserName,timestamp", $servername = "graylog.csub.edu", $port=443,$proto="https", $from="",$to="", $searchtype = "relative",$streamid="5d48c344ca325b304f768385") {
	$server = "$proto"
	$server += "://"
	$server += $servername
	$server += ":$port"

	$url = $server
	$url += "/api/search/universal/"
	$url += $searchtype # clunky/funky
	
	if ($groups -and $groups.Count -gt 0) {
		if ($query.length -gt 1) {
			$query += " AND "
		}
		$query += "("
		foreach ($group in $groups) {
			$query += "TargetUserName:$group OR "
		}
		$query = $query.Substring(0,$query.length-4)
		$query += ")"
	}

	$uri = "query=$query&range=$timeframe&fields=$fields&decorate=true" # not altogether sure what decorate does. research for another day.

	if ($streamid) {
		$uri += "&filter=streams:"
		$uri += $streamid
	}

	$url += "?"
	$url += $uri

	return $url
}

# Token is issued by graylog server. TODO: add a Get-SessionToken function
# tokentype is token or session

function Construct-Headers ($token = "8amvg6o6ibklr1d1sujmbu17sds5miripfkk5oiae5f5rm4vjsi", $tokentype = "token") {
	
	$pair = "$token" # static. issued by graylog.
	$pair += ":"
	$pair += "$tokentype" # session for session tokens
	$encodedCreds = [System.Convert]::ToBase64String([System.Text.Encoding]::ASCII.GetBytes($pair)) # encoded for basic auth

	$headers = @{ "Authorization"="Basic $encodedCreds";"X-Requested-By"="cli";"Content-Type"="application/json";"Accept"="application/json" } # per graylog docs
	
	return $headers
}

function Get-Response ($timeframe=25*60*60) {
	$url = Construct-Url -timeframe $timeframe
	$headers = Construct-Headers
	
	# TODO: add debug/verbose logging
	#Write-Log -message "URL: $url"
	#Write-Log -message "Headers: $headers" # this is useless

	$resp=""

	try {
		$resp = Invoke-RestMethod -Uri $url -Headers $headers
	} catch {
		$resp = ""
		$st = $_.ScriptStackTrace
		Write-Log -message "Invoke-RestMethod failed: $_
$st" -level "Error" -eventid 1338
	}

	return $resp
}

# Removes any duplicate messages. Also returns only the latest action against a particular account
# this is specific to the task at hand
# write your own
# groups are those groups that require mfa, and that if a user is a new member of either, mfa must be applied.
# use verify to verify current group membership in ad. just leave it, unless you want duplicate messaging and some funky cases. 

function Process-Messages($messages, $groups=@('staff','faculty'), $verify=$true) {
	$map = @{}
	$group_ct=@{} # count of groups seen per user
	foreach ($message in $messages) {
		if (-not ($message.message.MemberName -in $map.keys)) { # haven't seen this user before
			$map[$message.message.MemberName] = $message
			$group_ct[$message.message.MemberName] = 1
			continue
		} 
		
		# user already has operations pending against their account. determine the result of the set of operations
		# TODO: simplify this 
		
		if ($message.message.TargetUserName -eq ($map[$message.message.MemberName]).message.TargetUserName -and $message.message.EventID -eq ($map[$message.message.MemberName]).message.EventID) { # same user, target group, and action == same event. potential default case.
			if ($message.message.timestamp -gt ($map[$message.message.MemberName]).message.timestamp) { # take the newest one
				$map[$message.message.MemberName] = $message
			}
		} elseif ($message.message.EventID -eq ($map[$message.message.MemberName]).message.EventID) { # same user and action, different target group ~= same event. potential default case.
			if ($message.message.timestamp -gt ($map[$message.message.MemberName]).message.timestamp) { # take the newest one
				$map[$message.message.MemberName] = $message
			}
			$group_ct[$message.message.MemberName] += 1
		} elseif ($message.message.TargetUserName -eq ($map[$message.message.MemberName]).message.TargetUserName) { # same user and group, different action. potential default case.
			if ($message.message.timestamp -gt ($map[$message.message.MemberName]).message.timestamp) { # take the newest one
				$map[$message.message.MemberName] = $message
			}
		} elseif ($message.message.EventID -eq 4728) { # an addition to either group should result in an add action to the duo groups. Potential default case. different action and different group
			if ($message.message.timestamp -gt ($map[$message.message.MemberName]).message.timestamp) { # take the newest one
				$map[$message.message.MemberName] = $message
			}
			$group_ct[$message.message.MemberName] += 1
		}
		
		# different group, different action, action is remove. use the previous one.
		continue
	}
	
	# TODO: here, start the get-adgroupmember loop asynchronosly before the above processing loop, then wait for the results before proceeding to the next section
	if ($verify) {
		foreach ($group in $groups) {
			$ulist += (Get-AdGroupMember $group | Select-Object @{Label = "name";Expression = {($_.DistinguishedName).ToLower()}} ).name
		}
		# unique list of users in all groups specified
		$ulist = $ulist | Sort-Object -Unique
		# TODO: above, the async part
		foreach ($user in $group_ct.keys) {
			if ($user.ToLower() -in $ulist) { # still/already in a group that requires duo. dont remove or readd
				$map[$user].message.EventID = -1 # sentinel value to escape action in handle-message(s)
			}
		}
	}
	
	return @($map.Values)
}

# assumes $members is either a list object or a string of a username or a single user object 
function Take-Action($action, $group, $members) {
	$action = $action.ToLower()
	if ($members -is [array]) {
		foreach ($member in $members) {
			try {
				if ($action -eq 'add') {
					Add-AdGroupMember $group $member -Confirm:$false
				} elseif ($action -eq 'remove') {
					Remove-AdGroupMember $group $member -Confirm:$false
				} else {
					Write-Log -message "An unknown action was passed: $action" -level "Error" -eventid 1357
					return $false
				}
			} catch {
				$st = $_.ScriptStackTrace
				Write-Log -message "Unable to Take-Action: $action
Error: $_
Group: $group
User(s): $member
Stack Trace Begin:
$st
" -level "Error" -eventid 1351
				return $false
			}
		}
	} else {
		try {
			if ($action -eq 'add') {
				Add-AdGroupMember $group $members -Confirm:$false
			} elseif ($action -eq 'remove') {
				Remove-AdGroupMember $group $members -Confirm:$false
			} else {
				Write-Log -message "An unknown action was passed: $action" -level "Error" -eventid 1357
				return $false
			}
		} catch {
			$st = $_.ScriptStackTrace
			Write-Log -message "Unable to Take-Action: $action
Error: $_
Group: $group
User(s): $members
Stack Trace Begin:
$st
" -level "Error" -eventid 1351
			return $false
		}
	}
	
	return $true
}

# message is a message object returned from the query
# this handler assumes a lot. Build your own.
# this is designed to handle a group of messages all at once to reduce communication to the AD environment

function Handle-Messages ($messages, $groups, $notify=$true, $date = "") {
	if ((!($messages)) -or (!($groups))) {
		Write-Log -message "Unable to Handle-Messages if no messages and no groups are passed to the function" -level "Error" -eventid 1350
		return -1
	}
	$err=0
	$add=@()
	$rem=@()
	foreach ($message in $messages) {
		try {
			$name = $message.message.MemberName
			if ($message.message.EventID -eq 4728) { # added to group
				$add += $name
			} elseif ($message.message.EventID -eq 4729) { # removed from group
				$rem += $name
			} elseif ($message.message.EventID -ne -1) { # seen, but no action to take if EventID -eq -1. otherwise, error. TODO: on verbose/debug logging, add an else case here to log the skip.
				Write-Log -message "An unexpected eventID was found in the message handler: $message" -level "Warning" -eventid 1343
				$err+=1
			}
		} catch {
			$st = $_.ScriptStackTrace
			Write-Log -message "Unable to process the following message with this error: $_
$message
$st" -level "Error" -eventid 1344
			$err+=1
		}
	}
	try {
		if ($add.Count -gt 0) {
			if ($date  -eq "") {
				$date = Get-Date -Format "yyyyMMdd"
			}
			$members = ""
			$members = $add -join ','
			$ct = $add.Count
			foreach ($group in $groups) {
				if (Take-Action -action 'add' -group $group -members $add) {
					Write-Log -message "Successfully added $ct members to $group
$add
"
					# only when we successfully process users will we store them for later addition to other groups after a delay
					if (!(Store-Users -Users $add -date $date)) {
						Write-Log -message "Unable to store the following users to file with Store-Users!
$members" -level "Error" -eventid 1368
					} else {
						Write-Log -message "Successfully stored the following users with date $date
$add"
					}
				} else {
					Write-Log -message "Failed to add $ct members to $group" -level "Error" -eventid 1352
				}
			}
		}
	} catch {
		$st = $_.ScriptStackTrace
		Write-Log -message "Unable to add member(s) to ad group(s) with this error: $_
Users:
$add

Groups:
$groups

Stack Trace:
$st" -level "Error" -eventid 1347
		$err=-2
	}
	try {
		if ($rem.Count -gt 0) {
			$members = ""
			$members = $rem -join ','
			$ct = $rem.Count
			foreach ($group in $groups) {
				if (Take-Action -action 'remove' -group $group -members $rem) {
					Write-Log -message "Successfully removed $ct members from $group
$rem
"
				} else {
					Write-Log -message "Failed to remove $ct members from $group" -level "Error" -eventid 1353
				}
			}
		}
	} catch {
		$st = $_.ScriptStackTrace
		Write-Log -message "Unable to remove member(s) from ad group(s) with this error: $_
Users:
$rem

Groups:
$groups

Stack Trace:
$st" -level "Error" -eventid 1348
		$err=-3
	}
	
	$message_ct=0
	$ulist=@()
	
	if ($err -eq 0 -and $notify) {
		foreach ($message in $messages) {
			if ($message.message.EventID -eq -1) { # TODO: add debug/verbose logging here
				continue
			}
			try {
				$user = Get-AdUser $message.message.membername -Properties userprincipalname,givenname,surname
				$email = ($user | Select-Object @{Label = "email";Expression = {($_.UserPrincipalName).ToLower()}}).email
				$f = ($user | Select-Object @{Label = "f";Expression = {($_.GivenName)}}).f
				$l = ($user | Select-Object @{Label = "l";Expression = {($_.Surname)}}).l
			} catch {
				$st = $_.ScriptStackTrace
				$name = $message.message.membername
				Write-Log -message "Unable to get-aduser with this error: $_
	$name
	$st" -level "Error" -eventid 1361
			}
			
			try {
				Send-Email -emailAddress $email -first_name $f -last_name $l
				$ulist += $email
				$message_ct += 1
			} catch {
				$st = $_.ScriptStackTrace
				Write-Log -message "Unable to email user with this error: $_
	$f $l ($email)
	$st" -level "Error" -eventid 1360
			}
		}
		Write-Log -message "Sent $message_ct emails to the following users:
$ulist"
	}
	
	return $err
}

# message is a message object returned from the query
# this handler assumes a lot. Build your own.
# this is designed to handle individual messages in parallel
# to prevent many calls to get-date when processing in parallel, please just pass a "yyyyMMdd" formated date in $date

function Handle-Message($message, $groups, $notify=$true, $date = "") {
	if ((!($message)) -or (!($groups))) {
		Write-Log -message "Unable to Handle-Message if no message and no groups are passed to the function" -level "Error" -eventid 1349
		return $true
	}
	$err=$false
	try {
		if ($date  -eq "") {
			$date = Get-Date -Format "yyyyMMdd"
		}
		$name = $message.message.MemberName
		if ($message.message.EventID -eq 4728) { # added to group
			foreach ($group in $groups) {
				try {
					if (Take-Action -action 'add' -group $group -members $name) {
						Write-Log -message "Successfully added $name to $group"
						# only when we successfully process a user will we store them for later addition to other groups after a delay
						if (!(Store-User -User $name -date $date)) {
							Write-Log -message "Unable to store $name to file with Store-User!" -level "Error" -eventid 1367
						} else {
							Write-Log -message "Successfully stored $name with date $date"
						}
					} else {
						Write-Log -message "Failed to add $name to $group" -level "Error" -eventid 1354
					}
				} catch {
					$st = $_.ScriptStackTrace
					Write-Log -message "Unable to add $name to $group : $_
$st" -level "Error" -eventid 1345
					$err=$true
				}
			}
		} elseif ($message.message.EventID -eq 4729) { # removed from group
			foreach ($group in $groups) {
				try {
					if (Take-Action -action 'remove' -group $group -members $name) {
						Write-Log -message "Successfully removed $name from $group"
					} else {
						Write-Log -message "Failed to remove $name from $group" -level "Error" -eventid 1355
					}
				} catch {
					$st = $_.ScriptStackTrace
					Write-Log -message "Unable to remove $name from $group : $_
$st" -level "Error" -eventid 1346
					$err=$true
				}
			}
		} elseif ($message.message.EventID -ne -1) { # seen, but no action to take if EventID -eq -1. otherwise, error. TODO: on verbose/debug logging, add an else case here to log the skip.
			Write-Log -message "An unexpected eventID was found in the message handler: $message" -level "Warning" -eventid 1343
			$err=$true
		}
	} catch {
		$st = $_.ScriptStackTrace
		Write-Log -message "Unable to process the following message with this error: $_
$message
$st" -level "Error" -eventid 1344
		$err=$true
	}
	
	if (!($err) -and $notify) {
		if ($message.message.EventID -eq -1) { # TODO: logging
			continue
		}
		try {
			$user = Get-AdUser $message.message.membername -Properties userprincipalname,givenname,surname
			$email = ($user | Select-Object @{Label = "email";Expression = {($_.UserPrincipalName).ToLower()}}).email
			$f = ($user | Select-Object @{Label = "f";Expression = {($_.GivenName).ToLower()}}).f
			$l = ($user | Select-Object @{Label = "l";Expression = {($_.Surname).ToLower()}}).l
		} catch {
			$st = $_.ScriptStackTrace
			$name = $message.message.membername
			Write-Log -message "Unable to get-aduser with this error: $_
$name
$st" -level "Error" -eventid 1359
		}
		
		try {
			#Send-Email -emailAddress $email -first_name $f -last_name $l
			Write-Log -message "Emailed $email"
		} catch {
			$st = $_.ScriptStackTrace
			Write-Log -message "Unable to email user with this error: $_
$f $l ($email)
$st" -level "Error" -eventid 1358
		}
	}

	return $err
}

# dump users to file for action later
# assumes that $Users is a list object
# by default this will append to the file, which is not the behavior desired by Process-StoredUsers
function Store-Users ($Users,$date="",$FilePath="C:\Scripts\Auto-GroupChange\UsersToMove.tdv",$delimiter="`t") {
	$data = ""
	if ($date -eq "") {
		$date = Get-Date -Format "yyyyMMdd"
	}
	try {
		if (!(test-path $FilePath -pathtype leaf)) {
			$data = "user"
			$data += $delimiter
			$data += "date"
			$data > $FilePath
		}
		
		foreach ($user in $Users) {
			$data = $user
			$data += $delimiter
			$data += $date
			$data >> $FilePath
		}
	} catch {
		$st = $_.ScriptStackTrace
		Write-Log -message "Unable to store users to file ($FilePath) with this error: $_
$Users
$st" -level "Error" -eventid 1356
		return $false
	}
	
	return $true
}

# assumes user is a string
function Store-User ($User,$date="",$FilePath="C:\Scripts\Auto-GroupChange\UsersToMove.tdv",$delimiter="`t") {
	$data = ""
	if ($date -eq "") {
		$date = Get-Date -Format "yyyyMMdd"
	}
	try {
		if (!(test-path $FilePath -pathtype leaf)) {
			$data = "user"
			$data += $delimiter
			$data += "date"
			$data > $FilePath
		}
		
		$data = $User
		$data += $delimiter
		$data += $date
		
		$data >> $FilePath
		
	} catch {
		$st = $_.ScriptStackTrace
		Write-Log -message "Unable to store user to file ($FilePath) with this error: $_
$User
$st" -level "Error" -eventid 1366
		return $false
	}
	
	return $true
}


# Delay is the number of days to wait until moving users into 2StepUsers
function Process-StoredUsers ($FilePath="C:\Scripts\Auto-GroupChange\UsersToMove.tdv", $Groups=@('2StepUsers'), $Delay=5, $delimiter="`t") {
	$data = ""
	$targets = @()
	$wait_list = @{}
	$success = $true
	
	if (!(test-path $FilePath -pathtype leaf)) {
		Write-Log -message "No users exist in $FilePath"
		return $success
	}
	
	# get the users
	$data = Import-CSV -Delimiter $delimiter -Path $FilePath
	
	$target = (Get-Date).AddDays(-1*$Delay) # days in the past
	
	try {
		foreach ($line in $data) { # parse each user/date pair. if date is beyond specified time limit, add to group. else, replace in file. could optimize if data is always in date order. 
			$date = $line.date
			if ([datetime]::ParseExact("$date", "yyyyMMdd", $null) -lt $target) { # beyond timeline, act
				$targets += $line.user
			} else { # time to kill. wait. could optimize if data is always in date order. 
				if ($wait_list.ContainsKey($line.date)) { # append to existing list
					$wait_list[$line.date] += $line.user
				} else { # init empty list
					$wait_list[$line.date] = @()
					$wait_list[$line.date] += $line.user
				}
			}
		}
	} catch { # the file contains bad data, or was inaccessible
		$dest = $FilePath
		$dest += "-"
		$dest += (Get-Date -Format "yyyyMMdd")
		$dest += ".bad"
		
		Write-Log -message "The file $FilePath was inaccessible or contained bad data. Moving the file to $dest for your review (soon, please)" -level "Error" -eventid 1369
		try {
			Move-Item -Path $FilePath -Destination $dest -Force -Confirm:$false
		} catch {
			Write-Log -message "Unable to move the file! This may be a permissions issue with the service account" -level "Error" -eventid 1370
		} finally {
			$success = $false
		}
	}
	
	# TODO: add error handling to retry users that fail later. mayber readd them to the storage file.
	if ($targets.Count -gt 0) { # act
		foreach ($group in $Groups) {
			$ret = Take-Action -action 'add' -group $group -members $targets
			if ($ret) {
				Write-Log -message "Successfully took action against the following users for with the following group: $group
$targets"
			} else {
				Write-Log -message "Failed taking action against the following users for with the following group: $group
$targets" -level "Error" -eventid 1364
				$success = $false
			}
			
		}
	}
	
	# if no exception has occured by this point, go ahead and overwrite the file
	# until we add the error handling/graceful retry in the previous section, this could leave errored users out of the desired groups
	$data = "user"
	$data += $delimiter
	$data += "date"
	$data > $FilePath
	
	if ($wait_list.Count -gt 0) { # wait
		foreach ($key in $wait_list.keys) {
			$u = $wait_list[$key]
			# TODO: Distinct this list
			$ret = Store-Users -Users $u -date $key
			if ($ret) {
				Write-Log -message "Re-Stored the following users for with the storage date $key
$u"
			} else {
				Write-Log -message "Failed to Re-Store the following users for with the storage date $key
$u" -level "Error" -eventid 1363
				$success = $false
			}
		}
	}
	
	return $success
}

function Get-Template ($path = "C:\Scripts\Auto-GroupChange\2Step Initial Onboarding Communication.htm") {
	return (gc $path -encoding UTF8)
}

function Send-Email($emailAddress, $first_name, $last_name, $template_path = "C:\Scripts\Auto-GroupChange\2Step Initial Onboarding Communication.htm", $ForceTitleCase=$false) {
	$template = Get-Template -path $template_path
	
	# this is generally a bad idea, but if you want it, go ahead
	if ($ForceTitleCase) {
		$first_name = (Get-Culture).TextInfo.ToTitleCase($first_name)
		$last_name = (Get-Culture).TextInfo.ToTitleCase($last_name)
	}
	
    #Form Subject
    $subject = "[2Step] Welcome aboard " + $first_name + " " + $last_name + "!"

    #Create html body
    $body = $ExecutionContext.InvokeCommand.ExpandString($template)
    $emailSmtpServer = "smtp.office365.com"
    $emailSmtpServerPort = "587"
 
    $emailFrom = "ORG-2Step@csub.edu"
	
	$Pass = Get-Content 'C:\Scripts\Auto-GroupChange\login.pass' | ConvertTo-SecureString -AsPlainText -Force
	$Login = "agcnotify@csub.edu" # service account

    $emailMessage = New-Object System.Net.Mail.MailMessage( $emailFrom , $emailAddress )
	
    $emailMessage.Subject = $subject
	
    $emailMessage.IsBodyHtml = $true
    $emailMessage.Body = $body
        
    $SMTPClient = New-Object System.Net.Mail.SmtpClient( $emailSmtpServer , $emailSmtpServerPort )
    $SMTPClient.EnableSsl = $true
    $SMTPClient.Credentials = New-Object System.Net.NetworkCredential( $Login , $Pass ); 
    $SMTPClient.Send( $emailMessage )
}

# InitialTargetGroups is comma separated list of groups to add/remove to/from
# SubjectGroups is comma separated list of groups to base actions on
# Timeframe is a time in seconds in the past to check
# Parallel processes each message individually (so that in the future this could be parallelized)

function Auto-GroupChange ($Timeframe=25*60*60, $InitialTargetGroups="DuoUsers",$FinalTargetGroups="2StepUsers",$SubjectGroups="staff,faculty",$Parallel=$false,$Delay=5, $Notify=$true) { # TODO: add more cmd line args, arg processing
	$start = Get-Date
	Write-Log -message "Auto-GroupChange started: $start"
	
	$final_target_group_list = $FinalTargetGroups.split(',')
	
	# TODO: add notification option to process-storedusers
	$ret = Process-StoredUsers -Groups $final_target_group_list -Delay $Delay
	
	if ($ret) {
		Write-Log -message ("Successfully processed stored users. Elapsed time: {0:HH:mm:ss}" -f [datetime]((Get-Date).Ticks - $start.Ticks))
	} else {
		Write-Log -message ("There was an error processing stored users. Please look into it. Elapsed time: {0:HH:mm:ss}" -f [datetime]((Get-Date).Ticks - $start.Ticks)) -level "Error" -eventid 1365
	}
	
	$initial_target_group_list = $InitialTargetGroups.split(',')
	$subject_group_list = $SubjectGroups.split(',')
	$resp = Get-Response -timeframe $Timeframe # get the last day's worth of changes to the staff and faculty group
	if (!($resp) -or $resp -eq "") {;
		$tot_ct = -1
		$dd_ct = 0
		$err_ct = 1
	} else {
		$err_ct=0
		$messages=@()
		try {
			if ($resp.total_results -gt 0) { 
				try {
					$messages = Process-Messages -messages $resp.messages -verify $true -groups $subject_group_list  # remove duplicates, consolidate multiple changes for same user to latest change
					if ($Parallel) { # Parallel could be equivalently referred to as individual message processing
						foreach ($message in $messages) { # TODO: add parallel processing
							$err = Handle-Message $message $initial_target_group_list -notify $Notify # should not throw any errors, returns a bool if there was an error
							if ($err) {
								$err_ct += 1
							}
						}
					} else { # en-mass processing. only one call each for add/remove to ad
						$err_ct = Handle-Messages $messages $initial_target_group_list -notify $Notify
					}
				} catch {
					$resp = ""
					$st = $_.ScriptStackTrace
					Write-Log -message "Checking members of the messages in the response object probably failed: $_
	$st" -level "Error" -eventid 1342
				}
			} else {
				Write-Log -message "No changes to the provided groups found in the timeperiod specified ($Timeframe seconds)." -eventid 1340
			}
		} catch {
			$resp = ""
			$st = $_.ScriptStackTrace
			Write-Log -message "Checking members of the response object probably failed: $_
	$st" -level "Error" -eventid 1341
		}

		$tot_ct = $resp.messages.Count
		$dd_ct = $messages.Count
	}
	
	Write-Log -message ("Processed $tot_ct messages for $dd_ct changes with $err_ct error(s). Check the Application log for source=Auto-GroupChange for more details. Elapsed time: {0:HH:mm:ss}" -f [datetime]((Get-Date).Ticks - $start.Ticks))
}

try {
	Auto-GroupChange -Timeframe 90000 # last 25 hours
} catch {
	$st = $_.ScriptStackTrace
	Write-Log -message "An unhandled error occured in Auto-GroupChange. Please review, refactor, resign.
$_
$st
" -level "Error" -eventid 1362
}

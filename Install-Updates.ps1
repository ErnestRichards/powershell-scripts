#
# Install-Updates.ps1
#

function Install-Updates {
	param (
		[string]
		$PatchPath="C:\Users\erichards1\Documents\Patches\", # change this

		[string]
		$MachineListPath="C:\Users\erichards1\Documents\Patches\", # change this

		[string]
		$MachineListFileName="MachineList.txt", # add this file

		[string]
		$PatchList="", # optional. list of patches/reg updates

		[switch]
		$AutoReboot,

		[switch]
		$Recursive # optional. search for patches recursively 
	)
	$RootHotfixPath = $PatchPath
 
	if ($PatchList -ne "") {
		$Hotfixes = $PatchList.Split(',')
	} else {
		if ($Recursive) {
			$Hotfixes = gci $PatchPath *.* -rec | where { ! $_.PSIsContainer } | select Name
		} else {
			$Hotfixes = gci $PatchPath *.* | where { ! $_.PSIsContainer } | select Name
		}
	}

	$RootMachineListPath = $MachineListPath
	$path = $RootMachineListPath
	$path += $MachineListFileName
	$Servers = Get-Content $path

	foreach ($Server in $Servers) {
		Write-Host "Processing $Server..."

		$needsReboot = $False
		$remotePath = "\\$Server\c$\Temp\Patches\"
    
		if ( ! (Test-Connection $Server -Count 1 -Quiet)) {
			Write-Warning "$Server is not accessible"
			continue
		}

		if (!(Test-Path $remotePath)) {
			New-Item -ItemType Directory -Force -Path $remotePath | Out-Null
		}
    
		foreach ($Hotfix in $Hotfixes) {
			if ($PatchList -eq "") {
				$Hotfix = $Hotfix.name
			}
			if ($Hotfix -eq $MachineListFileName) {
				continue
			}
			Write-Host "`thotfix: $Hotfix"
			$HotfixPath = "$RootHotfixPath$Hotfix"

			Copy-Item $Hotfixpath $remotePath
			# Run command as SYSTEM via PsExec -nobanner (-s switch)
			if ($Hotfix -match "\.msu$") {
				& C:\Windows\PsExec -nobanner -s \\$Server wusa C:\Temp\Patches\$Hotfix /quiet /norestart
				write-host "& C:\Windows\PsExec -nobanner -s \\$Server wusa C:\Temp\Patches\$Hotfix /quiet /norestart"
				if ($LastExitCode -eq 3010) {
					$needsReboot = $true
				}
			} elseif ($Hotfix -match "\.ps1$") {
				& C:\Windows\PsExec -nobanner -s \\$Server Powershell.exe -NoLogo -ExecutionPolicy Unrestricted -WindowStyle Hidden -NonInteractive -File C:\Temp\Patches\$Hotfix
			} elseif ($Hotfix -match "\.reg$") {
				& C:\Windows\PsExec -nobanner -s \\$Server reg import C:\Temp\Patches\$Hotfix
				write-host "& C:\Windows\PsExec -nobanner -s \\$Server reg import C:\Temp\Patches\$Hotfix"
			} else {
				write-host "Sorry, I'm not sure how to handle $Hotfix type files"
			}
		}

		# Delete local copy of update packages
		Remove-Item $remotePath -Force -Recurse

		if($needsReboot -or $AutoReboot) {
			Write-Host "Restarting $Server..."
			Restart-Computer -ComputerName $Server -Force
		}
	}
}
Import-Module -Force Get-Secret -WarningAction SilentlyContinue
Import-Module -Force Wvd-AutoScale -WarningAction SilentlyContinue
Import-Module -Force Pan-Ban -WarningAction SilentlyContinue
Import-Module -Force Utilities -WarningAction SilentlyContinue

[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12 # until 1.3 comes around

$global:WordList = $null
$global:BadWordList = @()
$Env:PanBaseUrl = "https://panorama.csub.edu/api/?"

$DefaultSubscriptionId = "42b1bbd2-3677-431e-866d-a0ffa554937e"
$COSubscriptionId = "93fde401-3261-409c-b14b-3a9af6e993ed"
$DefaultDeploymentUrl = "https://rdbroker.wvd.microsoft.com"

#$UserCredentials = Get-Credential -Message "Enter your Domain User Credentials" -UserName $Env:username
#$OTP = Read-Host "Enter an OTP to retrieve your Domain Admin Creds"
#$DomainCredentials = Get-Secret 4309 -Credentials $UserCredentials -OTP $OTP
#$OTP = $null

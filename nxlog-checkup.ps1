### nxlog-checkup.ps1
### erichards1
### 5/6/2021 - last update 7/2/2021
### 
### checks all domain joined servers under the Domain Controllers and Domain Servers OUs for nxlog installation and conforming configuration file
### 
### takes no arguments as of yet. parameters are hard-coded.
### parameters are:
### 	$fix 			- this parameter indicates if we should attempt to fix any errors we find.
### 	$targetlist 	- don't search for all servers in domain servers and domain controllers. specific servers should be searched. 
### 	$triplecheck	- if attempting to fix, program will double-check before making any changes. this parameter indicates that we want to approve every single server that gets a correction. 
### 
### All appropriate configuration files must be in the local directory
### must be updated for new users/paths
### new config files must be manually added
### mapping of config files to server types/names is manual, requiring copying and modifying of chunks of code
### 


# TODO: command line arguments
# TODO: write-progress
# TODO: write-log
# TODO: simplify output

### Parameters

### leave targetlist empty to automatically enumerate and handle targets
$targetlist = @('nas4');
$targetconffile = 'bitvise-nxlog.conf';

### fix servers that either dont have nxlog installed or whose nxlog.conf file does not match the expected value
$fix = $true;
### double check that, after having seen the list of servers we identify as non-conforming, we still want to fix the things
$doublecheck = $true
### triple check on every system that we want to proceed with resolution
$triplecheck = $true

$installerlocation = "c:\users\erichards1\downloads\nxlog.msi"

### Start

$date = get-date -Format "yyyyMMdd"

Write-Host "[.] Starting NxLog checkup..." ; 
Write-Host "[.] Fix configs: $fix"
Write-Host "[.] Triple Check before changes: $triplecheck"
Write-Host "[.] Using manual targetlist: $($targetlist.count -gt 0)"

$errors = @{} ; $files = @{} ; $hashes = @{} ;
$haspowershelllogging = @() ; $nopowershelllogging = @() ;

$dcconfsum = ""
$memberconfsum = ""
$duoconfsum = ""
$adfsconfsum = ""
$bitviseconfsum = ""

$targetconfsum = ""


$dclist = @()
$memberlist = @()

$nonconformant = 0

function md5hash($path)
{
    $ret = ""
	if (!$path -or $path -eq "") {
		return $ret
	}
    $fullPath = Resolve-Path $path -ErrorAction SilentlyContinue -WarningAction SilentlyContinue
	if (!$fullPath -or $fullPath -eq "") {
		return $ret
	}
    $md5 = new-object -TypeName System.Security.Cryptography.MD5CryptoServiceProvider
    $file = $null
    try {
    	$file = [System.IO.File]::Open($fullPath,[System.IO.Filemode]::Open, [System.IO.FileAccess]::Read)
	} catch {
		$file = $null
	}
    if (!$file) {
    	return $ret
    }
    try {
        $ret = [System.BitConverter]::ToString($md5.ComputeHash($file))
    } catch {
    	write-host "[!] Unable to hash $path"
    	$ret = ""
    } finally {
        $file.Dispose()
    }
    return $ret
}

if ($targetlist.count -lt 1) {
	try {

		Write-Host "[.] Attempting to leverage the following hard-coded config files in the current working directory ($((pwd).path)):`n`t- dc-nxlog.conf`n`t- member-nxlog.conf`n`t- duo-nxlog.conf`n`t- adfs-nxlog.conf`n`t- bitvise-nxlog.conf`n`t- nas-nxlog.conf"
		$dcconfsum = md5hash .\dc-nxlog.conf
		$memberconfsum = md5hash .\member-nxlog.conf
		$duoconfsum = md5hash .\duo-nxlog.conf
		$adfsconfsum = md5hash .\adfs-nxlog.conf
		$bitviseconfsum = md5hash .\bitvise-nxlog.conf
		$nasconfsum = md5hash .\nas-nxlog.conf
		Write-host "[!] Using MD5 sum to compare files."
	} catch {
		Write-host "[!] Unable to MD5 sum the target config files. Cannot proceed."
		$dcconfsum = ""
		$memberconfsum = ""
		$duoconfsum = ""
		$adfsconfsum = ""
		$bitviseconfsum = ""
		$nasconfsum = ""
		return
	}
} else {
	try {
		Write-Host "[.] Attempting to leverage the following hard-coded config files in the current working directory ($((pwd).path)):`n`t- $targetconffile"
		$targetconfsum = md5hash ".\$targetconffile"
	} catch {
		Write-host "[!] Unable to MD5 sum the target config file. Cannot proceed."
		$targetconfsum = ""
		return
	}
}


if ($targetlist.count -lt 1) {

	# start gathering data

	write-host "[.] Gathering info from the Domain Controllers..."

	$dclist = (Get-ADComputer -SearchBase 'OU=Domain Controllers,dc=ad,dc=csub,dc=edu' -Filter *).dnshostname | sort-object -unique

	$dclist | %{ $server = $_; try { $s = New-PSSession $server -ErrorAction Stop }  catch { $errors["$server"]="Unable to connect to $server : $_"; $s=$null; } ; if ($s) { try { copy-item -FromSession $s -Path 'C:\Program Files (x86)\nxlog\conf\nxlog.conf' -Destination ".\$server-$date-nxlog.conf" -Force -ErrorAction Stop ; $files["$server"] = gc ".\$server-$date-nxlog.conf" ; try { $hashes["$server"] = md5hash .\$server-$date-nxlog.conf } catch {} ; } catch { $errors["$server"]="NxLog not installed on $server : $_"; } ; $s | remove-pssession ;} } ; 

	write-host "[.] Done with Domain Controllers"
	write-host "[.] Gathering info from the Domain Servers..."

	$memberlist = (Get-ADComputer -SearchBase 'OU=Domain Servers,dc=ad,dc=csub,dc=edu' -Filter *).dnshostname | sort-object -unique

	$memberlist | %{ $server = $_; try { $s = New-PSSession $server -ErrorAction Stop }  catch { $errors["$server"]="Unable to connect to $server : $_"; $s=$null; } ; if ($s) { try { copy-item -FromSession $s -Path 'C:\Program Files (x86)\nxlog\conf\nxlog.conf' -Destination ".\$server-$date-nxlog.conf" -Force -ErrorAction Stop ; $files["$server"] = gc ".\$server-$date-nxlog.conf" ; try { $hashes["$server"] = md5hash .\$server-$date-nxlog.conf } catch {} ; } catch { $errors["$server"]="NxLog not installed on $server : $_"; } ; $s | remove-pssession ;} } ; 

	write-host "[.] Done with Domain Servers`n"
} else {
	
	write-host "[.] Gathering info from the Target List..."

	$targetlist | %{ $server = $_; try { $s = New-PSSession $server -ErrorAction Stop }  catch { $errors["$server"]="Unable to connect to $server : $_"; $s=$null; } ; if ($s) { try { copy-item -FromSession $s -Path 'C:\Program Files (x86)\nxlog\conf\nxlog.conf' -Destination ".\$server-$date-nxlog.conf" -Force -ErrorAction Stop ; $files["$server"] = gc ".\$server-$date-nxlog.conf" ; try { $hashes["$server"] = md5hash .\$server-$date-nxlog.conf } catch {} ; } catch { $errors["$server"]="NxLog not installed on $server : $_"; } ; $s | remove-pssession ;} } ; 
}

# evaluate gathered data

write-host "[.] $($files.keys.count + $errors.keys.count) systems evaluated"
write-host "[.] $($hashes.count) file hashes calculated"
Write-host "[.] I'm unable to check the configs for $($errors.keys.count) systems"; 
Write-Host "[.] $(($errors.values | ?{ $_ -match "Unable to connect" }).Count) systems were not contactable; they may be offline or may not support remote management"
Write-Host "[.] $(($errors.values | ?{ $_ -match "not installed" }).Count) systems were contactable, but do not appear to have nxlog installed"

Write-Host "[.] Evaluating configs..."; 

### an old check for powershell logging specifically. no longer used. 
#foreach ($key in $files.keys) { if ($files["$key"] -match "Microsoft-Windows-PowerShell/Operational") { $haspowershelllogging += $key; } else { $nopowershelllogging += $key; } ; } ; write-host "`n[.] Should Have Powershell Logging: $($haspowershelllogging.count)" ; write-host "[.] No Powershell Logging: $($nopowershelllogging.count)" ; 

if ($targetlist.count -lt 1) {
	write-host "[.] $(($dclist | ?{ $errors["$_"] -match 'not installed' }).count) DCs do not have Nxlog installed"
	$dclist | ?{ $errors["$_"] -match 'not installed' }
	$nonconformant += ($dclist | ?{ $errors["$_"] -match 'not installed' }).count
	write-host "[.] $(($dclist | ?{ $errors["$_"] -match 'unable to contact' }).count) DCs could not be contacted"
	$dclist | ?{ $errors["$_"] -match 'unable to contact' }
	write-host "[.] $(($dclist | ?{ $_ -notin $errors.keys -and $hashes["$_"] -ne $dcconfsum }).count) DCs do not match the DC config file"
	$dclist | ?{ $_ -notin $errors.keys -and $hashes["$_"] -ne $dcconfsum }
	$nonconformant += ($dclist | ?{ $_ -notin $errors.keys -and $hashes["$_"] -ne $dcconfsum }).count

	write-host "[.] $(($memberlist | ?{$_ -notmatch 'duoproxy' -and $_ -notmatch 'adfs' -and $_ -notmatch 'sftp' -and $_ -notmatch 'nas'} | ?{ $errors["$_"] -match 'not installed' }).count) member servers do not have Nxlog installed"
	$memberlist | ?{$_ -notmatch 'duoproxy' -and $_ -notmatch 'adfs' -and $_ -notmatch 'sftp' -and $_ -notmatch 'nas'} | ?{ $errors["$_"] -match 'not installed' }
	$nonconformant += ($memberlist | ?{$_ -notmatch 'duoproxy' -and $_ -notmatch 'adfs' -and $_ -notmatch 'sftp' -and $_ -notmatch 'nas'} | ?{ $errors["$_"] -match 'not installed' }).count
	write-host "[.] $(($memberlist | ?{$_ -notmatch 'duoproxy' -and $_ -notmatch 'adfs' -and $_ -notmatch 'sftp' -and $_ -notmatch 'nas'} | ?{ $errors["$_"] -match 'unable to contact' }).count) member servers could not be contacted"
	$memberlist | ?{$_ -notmatch 'duoproxy' -and $_ -notmatch 'adfs' -and $_ -notmatch 'sftp' -and $_ -notmatch 'nas'} | ?{ $errors["$_"] -match 'unable to contact' }
	write-host "[.] $(($memberlist | ?{$_ -notmatch 'duoproxy' -and $_ -notmatch 'adfs' -and $_ -notmatch 'sftp' -and $_ -notmatch 'nas'} | ?{ $_ -notin $errors.keys -and $hashes["$_"] -ne $memberconfsum }).count) member servers do not match the Member config file"
	$memberlist | ?{$_ -notmatch 'duoproxy' -and $_ -notmatch 'adfs' -and $_ -notmatch 'sftp' -and $_ -notmatch 'nas'} | ?{ $_ -notin $errors.keys -and $hashes["$_"] -ne $memberconfsum }
	$nonconformant += ($memberlist | ?{$_ -notmatch 'duoproxy' -and $_ -notmatch 'adfs' -and $_ -notmatch 'sftp' -and $_ -notmatch 'nas'} | ?{ $_ -notin $errors.keys -and $hashes["$_"] -ne $memberconfsum }).count

	write-host "[.] $(($memberlist | ?{$_ -match 'duoproxy'} | ?{ $errors["$_"] -match 'not installed' }).count) Duo Proxies do not have Nxlog installed"
	$memberlist | ?{$_ -match 'duoproxy'} | ?{ $errors["$_"] -match 'not installed' }
	$nonconformant += ($memberlist | ?{$_ -match 'duoproxy'} | ?{ $errors["$_"] -match 'not installed' }).count
	write-host "[.] $(($memberlist | ?{$_ -match 'duoproxy'} | ?{ $errors["$_"] -match 'unable to contact' }).count) Duo Proxies could not be contacted"
	$memberlist | ?{$_ -match 'duoproxy'} | ?{ $errors["$_"] -match 'unable to contact' }
	write-host "[.] $(($memberlist | ?{$_ -match 'duoproxy'} | ?{ $_ -notin $errors.keys -and $hashes["$_"] -ne $duoconfsum }).count) Duo Proxies do not match the Duo Proxy config file"
	$memberlist | ?{$_ -match 'duoproxy'} | ?{ $_ -notin $errors.keys -and $hashes["$_"] -ne $duoconfsum }
	$nonconformant += ($memberlist | ?{$_ -match 'duoproxy'} | ?{ $_ -notin $errors.keys -and $hashes["$_"] -ne $duoconfsum }).count

	write-host "[.] $(($memberlist | ?{$_ -match 'adfs'} | ?{ $errors["$_"] -match 'not installed' }).count) ADFS Servers do not have Nxlog installed"
	$memberlist | ?{$_ -match 'adfs'} | ?{ $errors["$_"] -match 'not installed' }
	$nonconformant += ($memberlist | ?{$_ -match 'adfs'} | ?{ $errors["$_"] -match 'not installed' }).count
	write-host "[.] $(($memberlist | ?{$_ -match 'adfs'} | ?{ $errors["$_"] -match 'unable to contact' }).count) ADFS Servers could not be contacted"
	$memberlist | ?{$_ -match 'adfs'} | ?{ $errors["$_"] -match 'unable to contact' }
	write-host "[.] $(($memberlist | ?{$_ -match 'adfs'} | ?{ $_ -notin $errors.keys -and $hashes["$_"] -ne $adfsconfsum }).count) ADFS Servers do not match the ADFS Server config file"
	$memberlist | ?{$_ -match 'adfs'} | ?{ $_ -notin $errors.keys -and $hashes["$_"] -ne $adfsconfsum }
	$nonconformant += ($memberlist | ?{$_ -match 'adfs'} | ?{ $_ -notin $errors.keys -and $hashes["$_"] -ne $adfsconfsum }).count

	write-host "[.] $(($memberlist | ?{$_ -match 'sftp' -or $_ -match 'nas4'} | ?{ $errors["$_"] -match 'not installed' }).count) BitVise Servers do not have Nxlog installed"
	$memberlist | ?{$_ -match 'sftp' -or $_ -match 'nas4'} | ?{ $errors["$_"] -match 'not installed' }
	$nonconformant += ($memberlist | ?{$_ -match 'sftp' -or $_ -match 'nas4'} | ?{ $errors["$_"] -match 'not installed' }).count
	write-host "[.] $(($memberlist | ?{$_ -match 'sftp' -or $_ -match 'nas4'} | ?{ $errors["$_"] -match 'unable to contact' }).count) BitVise Servers could not be contacted"
	$memberlist | ?{$_ -match 'sftp' -or $_ -match 'nas4'} | ?{ $errors["$_"] -match 'unable to contact' }
	write-host "[.] $(($memberlist | ?{$_ -match 'sftp' -or $_ -match 'nas4'} | ?{ $_ -notin $errors.keys -and $hashes["$_"] -ne $bitviseconfsum }).count) BitVise Servers do not match the BitVise Server config file"
	$memberlist | ?{$_ -match 'sftp' -or $_ -match 'nas4'} | ?{ $_ -notin $errors.keys -and $hashes["$_"] -ne $bitviseconfsum }
	$nonconformant += ($memberlist | ?{$_ -match 'sftp' -or $_ -match 'nas4'} | ?{ $_ -notin $errors.keys -and $hashes["$_"] -ne $bitviseconfsum }).count

	write-host "[.] $(($memberlist | ?{$_ -notmatch 'nas4' -and $_ -match 'nas'} | ?{ $errors["$_"] -match 'not installed' }).count) NAS Servers do not have Nxlog installed"
	$memberlist | ?{$_ -notmatch 'nas4' -and $_ -match 'nas'} | ?{ $errors["$_"] -match 'not installed' }
	$nonconformant += ($memberlist | ?{$_ -notmatch 'nas4' -and $_ -match 'nas'} | ?{ $errors["$_"] -match 'not installed' }).count
	write-host "[.] $(($memberlist | ?{$_ -notmatch 'nas4' -and $_ -match 'nas'} | ?{ $errors["$_"] -match 'unable to contact' }).count) NAS Servers could not be contacted"
	$memberlist | ?{$_ -notmatch 'nas4' -and $_ -match 'nas'} | ?{ $errors["$_"] -match 'unable to contact' }
	write-host "[.] $(($memberlist | ?{$_ -notmatch 'nas4' -and $_ -match 'nas'} | ?{ $_ -notin $errors.keys -and $hashes["$_"] -ne $nasconfsum }).count) NAS Servers do not match the NAS Server config file"
	$memberlist | ?{$_ -notmatch 'nas4' -and $_ -match 'nas'} | ?{ $_ -notin $errors.keys -and $hashes["$_"] -ne $nasconfsum }
	$nonconformant += ($memberlist | ?{$_ -notmatch 'nas4' -and $_ -match 'nas'} | ?{ $_ -notin $errors.keys -and $hashes["$_"] -ne $nasconfsum }).count
} else {
	write-host "[.] $(($targetlist | ?{ $errors["$_"] -match 'not installed' }).count) targeted member servers do not have Nxlog installed"
	$targetlist | ?{ $errors["$_"] -match 'not installed' }
	$nonconformant += ($targetlist | ?{ $errors["$_"] -match 'not installed' }).count

	write-host "[.] $(($targetlist | ?{ $_ -notin $errors.keys -and $hashes["$_"] -ne $targetconfsum }).count) targeted member servers do not match the Member config file"
	$targetlist | ?{ $_ -notin $errors.keys -and $hashes["$_"] -ne $targetconfsum }
	$nonconformant += ($targetlist | ?{ $_ -notin $errors.keys -and $hashes["$_"] -ne $targetconfsum }).count
}

write-host "[.] Checkup Done!" ;
write-host "[.] Identified $nonconformant contactable non-conforming systems" ;

if ($nonconformant -gt 0) {
	if ($fix -eq $true) { # are you sure?
		if ($doublecheck -eq $True) {
			$fix = $false
			$in = read-Host "[?] Fix the non-conforming servers? (Y/n)"
			try {
				$fix = $in -eq $null -or $in -eq "" -or $in.tolower()[0] -eq 'y';
			} catch {
				$fix = $false
			}

			write-host "[.] Determined to fix? $fix"
		} else {
			$fix = $false
		}
	}

	if ($fix -eq $true) {
		
		# fix the files
		$jobs = @();
		$sessions = @();

		if ($targetlist.count -lt 1) {

			write-host "[.] Squashing configs on the Domain Controllers..."

			$configfile = (gc .\dc-nxlog.conf).split("`n");

			# install and stomp config
			$dclist | ?{$errors["$_"] -match "not installed"} | %{ $key = $_; try { $s = New-PSSession $key -ErrorAction Stop }  catch { $errors["$key"]="Unable to connect to $key : $_"; $s=$null; } ; if ($s) { try {if ($triplecheck -eq $true) {$in = Read-Host "[?] Install and Stomp on $key. Proceed? N/y";} else {$in = "y";} if ($in.tolower()[0] -eq 'y') {  copy-item -force -tosession $s -Path $installerlocation -destination c:\temp\ ;  ; $jobs += Invoke-Command -Session $s -AsJob -JobName $key -ScriptBlock { param($data) sleep 10 ; Unblock-File C:\temp\nxlog.msi; cmd /c "msiexec /i C:\temp\nxlog.msi /qn" ; sleep 60 ; Move-Item -Path 'C:\Program Files (x86)\nxlog\conf\nxlog.conf' -Destination 'C:\Program Files (x86)\nxlog\conf\nxlog.conf.bak' -Force ; [System.IO.File]::WriteAllLines('C:\Program Files (x86)\nxlog\conf\nxlog.conf', "$data") ; Get-Service nxlog | Set-Service -StartupType Automatic -PassThru | Restart-Service; } -ArgumentList "$($configfile -join "`r`n")" ;  } else {write-host "[.] Skipping $key"}  } catch { $errors["$key"]="Unable to replace the config file on $key : $_"; } $sessions += $s; } }

			# stomp config
			$dclist | ?{$_ -notin $errors.keys } | ?{ $hashes["$_"] -ne $dcconfsum } | %{ $key = $_; try { $s = New-PSSession $key -ErrorAction Stop }  catch { $errors["$key"]="Unable to connect to $key : $_"; $s=$null; } ; if ($s) { try { if ($triplecheck -eq $true) { $in = Read-Host "[?] Stomping on $key. Proceed? N/y";} else {$in = "y";} if ($in.tolower()[0] -eq 'y') { $jobs += Invoke-Command -Session $s -AsJob -JobName $key -ScriptBlock { param($data) Move-Item -Path 'C:\Program Files (x86)\nxlog\conf\nxlog.conf' -Destination 'C:\Program Files (x86)\nxlog\conf\nxlog.conf.bak' -Force ; [System.IO.File]::WriteAllLines('C:\Program Files (x86)\nxlog\conf\nxlog.conf', "$data") ; Get-Service nxlog | Set-Service -StartupType Automatic -PassThru | Restart-Service; } -ArgumentList "$($configfile -join "`r`n")" ; } else {write-host "[.] Skipping $key"} } catch { $errors["$key"]="Unable to replace the config file on $key : $_"; } $sessions += $s; } } ;


			write-host "[.] Done with Domain Controllers"
			write-host "[.] Squashing configs on Domain Servers..."

			$configfile = (gc .\member-nxlog.conf).split("`n");

			# install and stomp config
			$memberlist | ?{$_ -notmatch 'duoproxy' -and $_ -notmatch 'adfs' -and $_ -notmatch 'sftp' -and $_ -notmatch 'nas4'} | ?{$errors["$_"] -match "not installed"} | %{ $key = $_; try { $s = New-PSSession $key -ErrorAction Stop }  catch { $errors["$key"]="Unable to connect to $key : $_"; $s=$null; } ; if ($s) { try { if ($triplecheck -eq $true) { $in = Read-Host "[?] Install and Stomp on $key. Proceed? N/y";} else {$in = "y";} if ($in.tolower()[0] -eq 'y') { copy-item -force -tosession $s -Path $installerlocation -destination c:\temp\ ;  ; $jobs += Invoke-Command -Session $s -AsJob -JobName $key -ScriptBlock { param($data) sleep 10 ; Unblock-File C:\temp\nxlog.msi; cmd /c "msiexec /i C:\temp\nxlog.msi /qn" ; sleep 60 ; Move-Item -Path 'C:\Program Files (x86)\nxlog\conf\nxlog.conf' -Destination 'C:\Program Files (x86)\nxlog\conf\nxlog.conf.bak' -Force ; [System.IO.File]::WriteAllLines('C:\Program Files (x86)\nxlog\conf\nxlog.conf', "$data") ; Get-Service nxlog | Set-Service -StartupType Automatic -PassThru | Restart-Service; } -ArgumentList "$($configfile -join "`r`n")" ;  } else {write-host "[.] Skipping $key"} } catch { $errors["$key"]="Unable to replace the config file on $key : $_"; } $sessions += $s; } }

			# stomp config
			$memberlist | ?{$_ -notmatch 'duoproxy' -and $_ -notmatch 'adfs' -and $_ -notmatch 'sftp' -and $_ -notmatch 'nas4'} | ?{$_ -notin $errors.keys} | ?{ $hashes["$_"] -ne $memberconfsum } | %{ $key = $_; try { $s = New-PSSession $key -ErrorAction Stop }  catch { $errors["$key"]="Unable to connect to $key : $_"; $s=$null; } ; if ($s) { try { if ($triplecheck -eq $true) { $in = Read-Host "[?] Stomping on $key. Proceed? N/y";} else {$in = "y";} if ($in.tolower()[0] -eq 'y') { $jobs += Invoke-Command -Session $s -AsJob -JobName $key -ScriptBlock { param($data) Move-Item -Path 'C:\Program Files (x86)\nxlog\conf\nxlog.conf' -Destination 'C:\Program Files (x86)\nxlog\conf\nxlog.conf.bak' -Force ; [System.IO.File]::WriteAllLines('C:\Program Files (x86)\nxlog\conf\nxlog.conf', "$data") ; Get-Service nxlog | Set-Service -StartupType Automatic -PassThru | Restart-Service; } -ArgumentList "$($configfile -join "`r`n")" ;  } else {write-host "[.] Skipping $key"}  } catch { $errors["$key"]="Unable to replace the config file on $key : $_"; } $sessions += $s; } } ;

			write-host "[.] Done with Domain Members"
			write-host "[.] Squashing configs on Duo Proxies..."

			$configfile = (gc .\duo-nxlog.conf).split("`n");

			# install and stomp config
			$memberlist | ?{$_ -match 'duoproxy'} | ?{$errors["$_"] -match "not installed"} | %{ $key = $_; try { $s = New-PSSession $key -ErrorAction Stop }  catch { $errors["$key"]="Unable to connect to $key : $_"; $s=$null; } ; if ($s) { try {  if ($triplecheck -eq $true) { $in = Read-Host "[?] Install and Stomp on $key. Proceed? N/y";} else {$in = "y";} if ($in.tolower()[0] -eq 'y') { copy-item -force -tosession $s -Path $installerlocation -destination c:\temp\ ;  ; $jobs += Invoke-Command -Session $s -AsJob -JobName $key -ScriptBlock { param($data) sleep 10 ; Unblock-File C:\temp\nxlog.msi; cmd /c "msiexec /i C:\temp\nxlog.msi /qn" ; sleep 60 ; Move-Item -Path 'C:\Program Files (x86)\nxlog\conf\nxlog.conf' -Destination 'C:\Program Files (x86)\nxlog\conf\nxlog.conf.bak' -Force ; [System.IO.File]::WriteAllLines('C:\Program Files (x86)\nxlog\conf\nxlog.conf', "$data") ; Get-Service nxlog | Set-Service -StartupType Automatic -PassThru | Restart-Service; } -ArgumentList "$($configfile -join "`r`n")" ;  } else {write-host "[.] Skipping $key"}  } catch { $errors["$key"]="Unable to replace the config file on $key : $_"; } $sessions += $s; } }

			# stomp config
			$memberlist | ?{$_ -match 'duoproxy'} | ?{$_ -notin $errors.keys} | ?{ $hashes["$_"] -ne $duoconfsum } | %{ $key = $_; try { $s = New-PSSession $key -ErrorAction Stop }  catch { $errors["$key"]="Unable to connect to $key : $_"; $s=$null; } ; if ($s) { try { if ($triplecheck -eq $true) { $in = Read-Host "[?] Stomping on $key. Proceed? N/y"; } else {$in = "y";}if ($in.tolower()[0] -eq 'y') { $jobs += Invoke-Command -Session $s -AsJob -JobName $key -ScriptBlock { param($data) Move-Item -Path 'C:\Program Files (x86)\nxlog\conf\nxlog.conf' -Destination 'C:\Program Files (x86)\nxlog\conf\nxlog.conf.bak' -Force ; [System.IO.File]::WriteAllLines('C:\Program Files (x86)\nxlog\conf\nxlog.conf', "$data") ; Get-Service nxlog | Set-Service -StartupType Automatic -PassThru | Restart-Service; } -ArgumentList "$($configfile -join "`r`n")" ;  } else {write-host "[.] Skipping $key"}  } catch { $errors["$key"]="Unable to replace the config file on $key : $_"; } $sessions += $s; } } ;

			write-host "[.] Done with Duo Proxies"
			write-host "[.] Squashing configs on ADFS Servers..."

			$configfile = (gc .\adfs-nxlog.conf).split("`n");

			# install and stomp config
			$memberlist | ?{$_ -match 'adfs'} | ?{$errors["$_"] -match "not installed"} | %{ $key = $_; try { $s = New-PSSession $key -ErrorAction Stop }  catch { $errors["$key"]="Unable to connect to $key : $_"; $s=$null; } ; if ($s) { try { if ($triplecheck -eq $true) { $in = Read-Host "[?] Install and Stomp on $key. Proceed? N/y"; } else {$in = "y";} if ($in.tolower()[0] -eq 'y') { copy-item -force -tosession $s -Path $installerlocation -destination c:\temp\ ;  ; $jobs += Invoke-Command -Session $s -AsJob -JobName $key -ScriptBlock { param($data) sleep 10 ; Unblock-File C:\temp\nxlog.msi; cmd /c "msiexec /i C:\temp\nxlog.msi /qn" ; sleep 60 ; Move-Item -Path 'C:\Program Files (x86)\nxlog\conf\nxlog.conf' -Destination 'C:\Program Files (x86)\nxlog\conf\nxlog.conf.bak' -Force ; [System.IO.File]::WriteAllLines('C:\Program Files (x86)\nxlog\conf\nxlog.conf', "$data") ; Get-Service nxlog | Set-Service -StartupType Automatic -PassThru | Restart-Service; } -ArgumentList "$($configfile -join "`r`n")" ;  } else {write-host "[.] Skipping $key"}  } catch { $errors["$key"]="Unable to replace the config file on $key : $_"; } $sessions += $s; } }

			# stomp config
			$memberlist | ?{$_ -match 'adfs'} | ?{$_ -notin $errors.keys} | ?{ $hashes["$_"] -ne $adfsconfsum } | %{ $key = $_; try { $s = New-PSSession $key -ErrorAction Stop }  catch { $errors["$key"]="Unable to connect to $key : $_"; $s=$null; } ; if ($s) { try { if ($triplecheck -eq $true) { $in = Read-Host "[?] Stomping on $key. Proceed? N/y"; } else {$in = "y";} if ($in.tolower()[0] -eq 'y') { $jobs += Invoke-Command -Session $s -AsJob -JobName $key -ScriptBlock { param($data) Move-Item -Path 'C:\Program Files (x86)\nxlog\conf\nxlog.conf' -Destination 'C:\Program Files (x86)\nxlog\conf\nxlog.conf.bak' -Force ; [System.IO.File]::WriteAllLines('C:\Program Files (x86)\nxlog\conf\nxlog.conf', "$data") ; Get-Service nxlog | Set-Service -StartupType Automatic -PassThru | Restart-Service; } -ArgumentList "$($configfile -join "`r`n")" ;  } else {write-host "[.] Skipping $key"}  } catch { $errors["$key"]="Unable to replace the config file on $key : $_"; } $sessions += $s; } } ;

			write-host "[.] Done with ADFS Servers"
			write-host "[.] Squashing configs on BitVise Servers..."

			$configfile = (gc .\bitvise-nxlog.conf).split("`n");

			# install and stomp config
			$memberlist | ?{$_ -match 'sftp' -or $_ -match 'nas4'} | ?{$errors["$_"] -match "not installed"} | %{ $key = $_; try { $s = New-PSSession $key -ErrorAction Stop }  catch { $errors["$key"]="Unable to connect to $key : $_"; $s=$null; } ; if ($s) { try { if ($triplecheck -eq $true) { $in = Read-Host "[?] Install and Stomp on $key. Proceed? N/y"; } else {$in = "y";} if ($in.tolower()[0] -eq 'y') { copy-item -force -tosession $s -Path $installerlocation -destination c:\temp\ ;  ; $jobs += Invoke-Command -Session $s -AsJob -JobName $key -ScriptBlock { param($data) sleep 10 ; Unblock-File C:\temp\nxlog.msi; cmd /c "msiexec /i C:\temp\nxlog.msi /qn" ; sleep 60 ; Move-Item -Path 'C:\Program Files (x86)\nxlog\conf\nxlog.conf' -Destination 'C:\Program Files (x86)\nxlog\conf\nxlog.conf.bak' -Force ; [System.IO.File]::WriteAllLines('C:\Program Files (x86)\nxlog\conf\nxlog.conf', "$data") ; Get-Service nxlog | Set-Service -StartupType Automatic -PassThru | Restart-Service; } -ArgumentList "$($configfile -join "`r`n")" ;  } else {write-host "[.] Skipping $key"}  } catch { $errors["$key"]="Unable to replace the config file on $key : $_"; } $sessions += $s; } }

			# stomp config
			$memberlist | ?{$_ -match 'sftp' -or $_ -match 'nas4'} | ?{$_ -notin $errors.keys} | ?{ $hashes["$_"] -ne $bitviseconfsum } | %{ $key = $_; try { $s = New-PSSession $key -ErrorAction Stop }  catch { $errors["$key"]="Unable to connect to $key : $_"; $s=$null; } ; if ($s) { try { if ($triplecheck -eq $true) { $in = Read-Host "[?] Stomping on $key. Proceed? N/y"; } else {$in = "y";} if ($in.tolower()[0] -eq 'y') { $jobs += Invoke-Command -Session $s -AsJob -JobName $key -ScriptBlock { param($data) Move-Item -Path 'C:\Program Files (x86)\nxlog\conf\nxlog.conf' -Destination 'C:\Program Files (x86)\nxlog\conf\nxlog.conf.bak' -Force ; [System.IO.File]::WriteAllLines('C:\Program Files (x86)\nxlog\conf\nxlog.conf', "$data") ; Get-Service nxlog | Set-Service -StartupType Automatic -PassThru | Restart-Service; } -ArgumentList "$($configfile -join "`r`n")" ;  } else {write-host "[.] Skipping $key"}  } catch { $errors["$key"]="Unable to replace the config file on $key : $_"; } $sessions += $s; } } ;

			write-host "[.] Done with BitVise Servers"
			write-host "[.] Squashing configs on NAS Servers..."

			$configfile = (gc .\nas-nxlog.conf).split("`n");

			# install and stomp config
			$memberlist | ?{$_ -notmatch 'nas4' -and $_ -match 'nas'} | ?{$errors["$_"] -match "not installed"} | %{ $key = $_; try { $s = New-PSSession $key -ErrorAction Stop }  catch { $errors["$key"]="Unable to connect to $key : $_"; $s=$null; } ; if ($s) { try { if ($triplecheck -eq $true) { $in = Read-Host "[?] Install and Stomp on $key. Proceed? N/y"; } else {$in = "y";} if ($in.tolower()[0] -eq 'y') { copy-item -force -tosession $s -Path $installerlocation -destination c:\temp\ ;  ; $jobs += Invoke-Command -Session $s -AsJob -JobName $key -ScriptBlock { param($data) sleep 10 ; Unblock-File C:\temp\nxlog.msi; cmd /c "msiexec /i C:\temp\nxlog.msi /qn" ; sleep 60 ; Move-Item -Path 'C:\Program Files (x86)\nxlog\conf\nxlog.conf' -Destination 'C:\Program Files (x86)\nxlog\conf\nxlog.conf.bak' -Force ; [System.IO.File]::WriteAllLines('C:\Program Files (x86)\nxlog\conf\nxlog.conf', "$data") ; Get-Service nxlog | Set-Service -StartupType Automatic -PassThru | Restart-Service; } -ArgumentList "$($configfile -join "`r`n")" ;  } else {write-host "[.] Skipping $key"}  } catch { $errors["$key"]="Unable to replace the config file on $key : $_"; } $sessions += $s; } }

			# stomp config
			$memberlist | ?{$_ -notmatch 'nas4' -and $_ -match 'nas'} | ?{$_ -notin $errors.keys} | ?{ $hashes["$_"] -ne $nasconfsum } | %{ $key = $_; try { $s = New-PSSession $key -ErrorAction Stop }  catch { $errors["$key"]="Unable to connect to $key : $_"; $s=$null; } ; if ($s) { try { if ($triplecheck -eq $true) { $in = Read-Host "[?] Stomping on $key. Proceed? N/y"; } else {$in = "y";} if ($in.tolower()[0] -eq 'y') { $jobs += Invoke-Command -Session $s -AsJob -JobName $key -ScriptBlock { param($data) Move-Item -Path 'C:\Program Files (x86)\nxlog\conf\nxlog.conf' -Destination 'C:\Program Files (x86)\nxlog\conf\nxlog.conf.bak' -Force ; [System.IO.File]::WriteAllLines('C:\Program Files (x86)\nxlog\conf\nxlog.conf', "$data") ; Get-Service nxlog | Set-Service -StartupType Automatic -PassThru | Restart-Service; } -ArgumentList "$($configfile -join "`r`n")" ;  } else {write-host "[.] Skipping $key"}  } catch { $errors["$key"]="Unable to replace the config file on $key : $_"; } $sessions += $s; } } ;

			write-host "[.] Done with NAS Servers"

		} else {

			write-host "[.] Squashing configs on Target List..."

			$configfile = (gc .\$targetconffile).split("`n");

			# install and stomp config
			$targetlist | ?{$errors["$_"] -match "not installed"} | %{ $key = $_; try { $s = New-PSSession $key -ErrorAction Stop }  catch { $errors["$key"]="Unable to connect to $key : $_"; $s=$null; } ; if ($s) { try {  if ($triplecheck -eq $true) { $in = Read-Host "[?] Install and Stomp on $key. Proceed? N/y"; } else {$in = "y";} if ($in.tolower()[0] -eq 'y') { copy-item -force -tosession $s -Path $installerlocation -destination c:\temp\ ;  ; $jobs += Invoke-Command -Session $s -AsJob -JobName $key -ScriptBlock { param($data) sleep 10 ; Unblock-File C:\temp\nxlog.msi; cmd /c "msiexec /i C:\temp\nxlog.msi /qn" ; sleep 60 ; Move-Item -Path 'C:\Program Files (x86)\nxlog\conf\nxlog.conf' -Destination 'C:\Program Files (x86)\nxlog\conf\nxlog.conf.bak' -Force ; [System.IO.File]::WriteAllLines('C:\Program Files (x86)\nxlog\conf\nxlog.conf', "$data") ; Get-Service nxlog | Set-Service -StartupType Automatic -PassThru | Restart-Service; } -ArgumentList "$($configfile -join "`r`n")" ;  } else {write-host "[.] Skipping $key"}  } catch { $errors["$key"]="Unable to replace the config file on $key : $_"; } $sessions += $s; } }

			# stomp config
			$targetlist | ?{$_ -notin $errors.keys} | %{ $key = $_; try { $s = New-PSSession $key -ErrorAction Stop }  catch { $errors["$key"]="Unable to connect to $key : $_"; $s=$null; } ; if ($s) { try { if ($triplecheck -eq $true) { $in = Read-Host "[?] Stomping on $key. Proceed? N/y"; } else {$in = "y";} if ($in.tolower()[0] -eq 'y') { $jobs += Invoke-Command -Session $s -AsJob -JobName $key -ScriptBlock { param($data) Move-Item -Path 'C:\Program Files (x86)\nxlog\conf\nxlog.conf' -Destination 'C:\Program Files (x86)\nxlog\conf\nxlog.conf.bak' -Force ; [System.IO.File]::WriteAllLines('C:\Program Files (x86)\nxlog\conf\nxlog.conf', "$data") ; Get-Service nxlog | Set-Service -StartupType Automatic -PassThru | Restart-Service; } -ArgumentList "$($configfile -join "`r`n")" ;  } else {write-host "[.] Skipping $key"}  } catch { $errors["$key"]="Unable to replace the config file on $key : $_"; } $sessions += $s; } } ;

		}

		if ($jobs.count -gt 0) {
			Write-Host "[.] Waiting for stomp jobs to finish..."
			while ($((get-job | ?{$_.id -in $jobs.id -and $_.state -eq 'Running'}).count) -gt 0) {
				sleep 1
			}

			if ($((get-job | ?{$_.id -in $jobs.id -and $_.state -ne 'Completed'}).count) -gt 0) {
				Write-Host "[!] There was an issue with one or more jobs. The jobs variable has all the job details and the sessions variable has the current sessions. Here are the incomplete jobs:"
				get-job | ?{$_.id -in $jobs.id -and $_.state -ne 'Completed'}
				get-job | ?{$_.id -in $jobs.id -and $_.state -eq 'Completed'} | Receive-Job -AutoRemoveJob -Wait
			} else {
				Write-Host "[.] All jobs completed successfully. Checking that nxlog is running on these systems..."
				foreach ($s in $sessions) {
					Write-Host "[.] Checking $($s.ComputerName)"
					$jobs += Invoke-Command -Session $s -AsJob -JobName "$($s.ComputerName)" -ScriptBlock {
						$result = "";
						try { $service = Get-Service nxlog -ErrorAction stop } catch { write-host "[!] Unable to get the nxlog service - $_"; $result += "[!] Unable to get the nxlog service - $_`n"; $service = $null; }
						if ($service) {
							if ($service.status -eq 'Running') {
								Write-Host "[.] Service is Running";
								$result += "[.] Service is Running`n";
							} else {
								Write-Host "[!] Service status: $($service.status). Attempting a stop-start."
								$result += "[!] Service status: $($service.status). Attempting a stop-start.`n"
								try { $service | stop-service -Force -ErrorAction Stop ; sleep 3; } catch { write-host "[!] Unable to stop the service - $_" ; $result += "[!] Unable to stop the service - $_`n" ; }
								try { $service | start-service -ErrorAction stop ; sleep 3 ; $service = Get-Service nxlog ; if ($service.status -eq 'Running') { Write-Host "[.] Successfully restarted the nxlog service." ; $result += "[.] Successfully restarted the nxlog service.`n"; } else { $result += "Unable to start the service, but I'm not sure why. Here's the status: $($service.status)`n" } } catch { $result += "[!] Unable to start the service - $_`n"}
							}
						} else {
							$result += "[!] Unable to get the nxlog service`n";
						}
						Write-Host "$result"
						return $result;
					} ;
					$s | remove-pssession
				}
				write-host "[.] Waiting for Checkup jobs to finish"
				while ($((get-job | ?{$_.id -in $jobs.id -and $_.state -eq 'Running'}).count) -gt 0) {
					sleep 1
				}

				if ($((get-job | ?{$_.id -in $jobs.id -and $_.state -ne 'Completed'}).count) -gt 0) {
					Write-Host "[!] There was an issue with one or more jobs. The jobs varialbe has all the job details and the sessions variable has the current sessions. Here are the incomplete jobs:"
					get-job | ?{$_.id -in $jobs.id -and $_.state -ne 'Completed'}
				}
				get-job | ?{$_.id -in $jobs.id -and $_.state -eq 'Completed'} | Receive-Job -AutoRemoveJob -Wait

			}
		} else {
			Write-Host "[.] No jobs to await"
			if ($sessions.count -gt 0) {
				Write-Host "[.] Clearing active sessions"
				$sessions | %{ $_ | remove-pssession }
			}
		}
	}
}
Write-Host "[.] Done!"

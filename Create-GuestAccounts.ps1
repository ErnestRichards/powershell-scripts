function Create-GuestAccounts ($basename = "FormsAccount", $count = 30, $wordfilepath = "C:\users\erichards1\desktop\words.txt", $ou = "OU=Guest,OU=ITSS,OU=IT,DC=ad,DC=csub,DC=edu", $days = 2) {
	$accts = @()
	$words = (import-csv $wordfilepath).2
	$maxlen = $basename.length + $count.tostring().length
	if ($maxlen -ge 20) {
		$basename = $basename[0..$($maxlen-21)] -Join ""
		Write-Verbose "Truncating Base Name to $basename"
	}
	for ($i = 0; $i -lt $count; $i++) {
		$acct = @{"user"=$null;"pass"=""}
		$tname = ""
		$tpass = ""
		$tname = $basename + $i.ToString()
		while ($tpass.length -lt 10) {
			$tpass += (Get-Culture).TextInfo.ToTitleCase($words[$(Get-Random -Minimum 1 -Maximum 400000)])
		}
		$tpass += (Get-Random -Minimum 1 -Maximum 99).ToString()
		try {
			$user = New-ADUser -Name $tname -Type iNetOrgPerson -Path $ou -AccountExpirationDate (Get-Date).AddDays($days) -AccountPassword (convertto-securestring $tpass -AsPlainText -Force) -CannotChangePassword $True -Enabled $True -SamAccountName $tname.ToLower() -UserPrincipalName ($tname.ToLower() +"@csub.edu") -DisplayName $tname -GivenName $tname -PassThru
			$acct["user"] = $user
			$acct["pass"] = $tpass
			$accts += $acct
		} catch {
			Write-Error "[!] - Failed once. Trying a new password..."
			$tpass = ""
			while ($tpass.length -lt 10) {
				$tpass += (Get-Culture).TextInfo.ToTitleCase($words[$(Get-Random -Minimum 1 -Maximum 400000)])
			}
			$tpass += (Get-Random -Minimum 1 -Maximum 99).ToString()
			try {
				$user = New-ADUser -Name $tname -Type iNetOrgPerson -Path $ou -AccountExpirationDate (Get-Date).AddDays($days) -AccountPassword (convertto-securestring $tpass -AsPlainText -Force) -CannotChangePassword $True -Enabled $True -SamAccountName $tname.ToLower() -UserPrincipalName ($tname.ToLower() +"@csub.edu") -DisplayName $tname -GivenName $tname -PassThru
				$acct["user"] = $user
				$acct["pass"] = $tpass
				$accts += $acct
			} catch {
				Write-Error "[!] - Failed again. Aborting."
				return 1
			}
		}
	}
	Write-Host "Created $count Accounts!"
	return $accts
}

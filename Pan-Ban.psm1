<#
 .Synopsis
  Bans IPs in Palo Alto Panoramas.

 .Description
  Used to ban an IP address in the network firewalls at CSUB.
  Assumes that you have a rule to ban IPs that references an
  address group called 'banned'. 

 .Example
   # Ban an IP Address
   Pan-Ban 1.2.3.4

 .Example
   # Display rediculous logging
   Pan-Ban 1.2.3.4 -InternalDebug
   
 .Todo
   Add checks for already banned addresses
   Test different commit methods - currently seeing double pushes
   Change the return values to be more useful
   Accept a file of IPs
   Add banned URLs
   check for commit queues/limits
   add a report option (pull some evidence, pull the abuse contact for the IP, (pgp sign?) and send email)
#>

[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12 # until 1.3 comes around
$Env:PanBaseUrl = "https://panorama.csub.edu/api/?"

function Pan-SetBaseUrl {
	[CmdletBinding()]
	param (
		[string]
		$url="https://panorama.csub.edu/api/?",

		[switch]
		$InternalDebug
	)

	if ($InternalDebug) {
		Write-Host "[.] Setting `$Env:PanBaseUrl to `"$url`""
	}

	$Env:PanBaseUrl = $url
}

function Pan-GetCreds {
	[CmdletBinding()]

	param (
		[string]
		$burl=$Env:PanBaseUrl,

		[switch]
		$InternalDebug
	)

	if (!$burl -or $burl.Length -lt 4) {
		Pan-SetBaseUrl -InternalDebug:$InternalDebug
		$burl = $Env:PanBaseUrl
		if (!$burl -or $burl.Length -lt 4) {
			throw "missing_PanBaseUrl"
		}
	}

	$url = $burl
	$url += "type=keygen&user="
	$UserName = Read-Host -Prompt "Enter your Panorama Username"
	$url += $UserName
	$url += "&password="
	$password = Read-Host -Prompt "Enter your password" -AsSecureString
	$Credentials = New-Object System.Management.Automation.PSCredential -ArgumentList $UserName, $password
	if ($InternalDebug) {
		Write-Host "[.] Requesting $url to validate credentials"
	}
	$url += $Credentials.GetNetworkCredential().password
	
	$reg = "response.status.+[ ""']success[ ""']"

	$resp = Invoke-WebRequest -UseBasicParsing $url

	$url = ""

	if ($resp.content -notmatch $reg) {
		if ($InternalDebug) {
			Write-Host "[.] Credentials were bad"
		}
		$Credentials=$null
	} else {
		$Env:PanUsername=$Credentials.UserName
		if ($InternalDebug) {
			Write-Host "[.] Credentials were good"
		}
	}
	return $Credentials
}

function Pan-GetKey {
	[CmdletBinding()]

	param (
		[string]
		$burl=$Env:PanBaseUrl,

		[System.Management.Automation.PSCredential]
		$creds,

		[switch]
		$InternalDebug,

		[switch]
		$Quiet=$true
	)

	if (!$burl -or $burl.Length -lt 4) {
		Pan-SetBaseUrl -InternalDebug:$InternalDebug
		$burl = $Env:PanBaseUrl
		if (!$burl -or $burl.Length -lt 4) {
			throw "missing_PanBaseUrl"
		}
	}

	if (!$creds) {
		$creds = Pan-GetCreds -InternalDebug:$InternalDebug
	}
	if (!$creds) {
		throw "Unable to get Credentials"
		return
	} else {
		$url = $burl
		$url += "type=keygen&user="
		$url += $creds.UserName
		$url += "&password="
		if ($InternalDebug) {
			Write-Host "[.] Requesting $url"
		}
		$url += $creds.GetNetworkCredential().password
	
		$reg = "response.status.+[ ""']success[ ""']"

		$resp = Invoke-WebRequest -UseBasicParsing $url

		if ($InternalDebug) {
			Write-Host "Response: $resp"
		}

		if ($resp.content -notmatch $reg) {
			$tkey = ""
		} else {
			$tkey = $resp.content.tostring().replace("<response status = 'success'><result><key>","")
			$tkey = $tkey.replace("</key></result></response>","")
		}
		$Env:PanKey=$tkey
		if ($Quiet) {
			return
		}
		return $tkey
	}
}

function Pan-AddressExists { # only searches by exact name
	[CmdletBinding()]
	param (
		[string]
		$Name="",

		[string]
		$burl=$Env:PanBaseUrl,

		[string]
		$key=$Env:PanKey,

		[switch]
		$InternalDebug
	)

	if (!$burl -or $burl.Length -lt 4) {
		Pan-SetBaseUrl -InternalDebug:$InternalDebug
		$burl = $Env:PanBaseUrl
		if (!$burl -or $burl.Length -lt 4) {
			throw "missing_PanBaseUrl"
		}
	}

	if (!$key -or ($key -eq "")) {
		$key = Pan-GetKey -Quiet:$false -InternalDebug:$InternalDebug
	}
	if (!$key -or ($key -eq "")) {
		return $false
	}
	
	$xpath_sharedAddress="/config/shared/address/entry[@name='$Name']"

	$url = $burl
	$url += "type=config&action=get&key="
	$url += $key
	$url += "&xpath="
	$url += $xpath_sharedAddress
	if ($InternalDebug) {
		Write-Host "[.] Requesting $url"
	}
	$res = Invoke-WebRequest -UseBasicParsing $url
	
	$reg = "response.status.+[ ""']success[ ""']"

	$res = Invoke-WebRequest -UseBasicParsing $url

	if ($InternalDebug) {
		Write-Host "[.] Response: $res"
	}

	if ($res.content -notmatch $reg) {
		# problem
		if ($InternalDebug) {
			write-error "Failed at 92"
			write-error $res
		}
		return $false
	}

	$reg = "<result/>"

	if ($res.content.tostring() -match $reg) {
		if ($InternalDebug) {
			Write-Host "[i] Address does not exist"
		}
		return $false
	}

	if ($InternalDebug) {
		Write-Host "[i] Address exists"
	}
	
	return $true
}

function Pan-AddAddress {
	[CmdletBinding()]
	param (
		[string]
		$IpAddress="",

		[string]
		$Name="",

		[string]
		$burl=$Env:PanBaseUrl,

		[string]
		$key=$Env:PanKey,

		[switch]
		$InternalDebug,

		[switch]
		$SkipExistenceCheck # useful when newly created address objects havent been committed yet
	)

	if (!$burl -or $burl.Length -lt 4) {
		Pan-SetBaseUrl -InternalDebug:$InternalDebug
		$burl = $Env:PanBaseUrl
		if (!$burl -or $burl.Length -lt 4) {
			throw "missing_PanBaseUrl"
		}
	}

	if ($SkipExistenceCheck) {
		if ($InternalDebug) {
			Write-Host "[.] Skipping check to see if $Name exists, assuming it does"
		}
		return $true
	}

	if (!$key -or ($key -eq "")) {
		$key = Pan-GetKey -Quiet:$false -InternalDebug:$InternalDebug
	}
	if (!$key -or ($key -eq "")) {
		return $false
	}

	if (!$Name -or $Name -eq "") {
		if ($IpAddress -match "/\d{1,2}$") {
			$Name = $IpAddress -replace '/', '-'
		} else {
			$Name = $IpAddress + "-32"
		}
	}
	if ($InternalDebug) {
		Write-Host "[.] Attempting to add $IpAddress as $Name"
	}

	if (Pan-AddressExists -Name $Name -InternalDebug:$InternalDebug) {
		Write-Host "[i] $Name already exists"
		return $true
	}
	
	$xpath_sharedAddress="/config/shared/address/entry[@name='$Name']"
	$elementValue_sharedAddress="<ip-netmask>$IpAddress</ip-netmask><description>$Name</description><tag><member>ScriptCreated</member></tag>"

	$url = $burl
	$url += "type=config&action=set&key="
	$url += $key
	$url += "&xpath="
	$url += $xpath_sharedAddress
	$url += "&element="
	$url += $elementValue_sharedAddress
	if ($InternalDebug) {
		Write-Host "[.] Requesting $url"
	}
	$res = Invoke-WebRequest -UseBasicParsing $url
	
	$reg = "response.status.+[ ""']success[ ""']"

	$res = Invoke-WebRequest -UseBasicParsing $url
	if ($InternalDebug) {
		Write-Host "[.] Response: $res"
	}

	if ($res.content -notmatch $reg) {
		# problem
		if ($InternalDebug) {
			write-error "Failed at 90"
			write-error $res
		}
		return $false
	}
	
	return $true
}

function Pan-AddAddresses {
	[CmdletBinding()]
	param (
		[string[]]
		$IpAddresses,

		[string[]]
		$Names,

		[string]
		$burl=$Env:PanBaseUrl,

		[string]
		$key=$Env:PanKey,

		[switch]
		$InternalDebug,

		[switch]
		$SkipExistenceCheck # useful when newly created address objects havent been committed yet
	)

	if (!$burl -or $burl.Length -lt 4) {
		Pan-SetBaseUrl -InternalDebug:$InternalDebug
		$burl = $Env:PanBaseUrl
		if (!$burl -or $burl.Length -lt 4) {
			throw "missing_PanBaseUrl"
		}
	}

	for ($i=0; $i -lt $IpAddresses.Count; $i++) {
		$ip = $IpAddresses[$i]
		$name = ""
		try {
			$name = $Names[$i]
		} catch {
			if ($ip -match "/\d{1,2}$") {
				$name = $ip -replace '/', '-'
			} else {
				$name = $ip + "-32"
			}
		}
		if (Pan-AddAddress -IpAddress $ip -Name $name -InternalDebug:$InternalDebug -SkipExistenceCheck:$SkipExistenceCheck) {
			if ($InternalDebug) {
				Write-Host "[.] Success: $name ($ip)"
			}
		} else {
			if ($InternalDebug) {
				Write-Host "[.] Failure: $name ($ip)"
			}
		}
	}
}

function Pan-AddAddressGroupMember {
	[CmdletBinding()]
	param (
		[string]
		$AddressGroupName="",

		[string]
		$IpAddress="",

		[string]
		$AddressName="",

		[string]
		$burl=$Env:PanBaseUrl,

		[string]
		$key=$Env:PanKey,

		[switch]
		$InternalDebug,

		[switch]
		$SkipExistenceCheck # useful when newly created address objects havent been committed yet
	)

	if (!$burl -or $burl.Length -lt 4) {
		Pan-SetBaseUrl -InternalDebug:$InternalDebug
		$burl = $Env:PanBaseUrl
		if (!$burl -or $burl.Length -lt 4) {
			throw "missing_PanBaseUrl"
		}
	}

	if ((!$AddressName -or $AddressName -eq "") -and ($IpAddress -and $IpAddress.Length -gt 6)) { # no address name, but have an ip
		if ($IpAddress -match "/\d{1,2}$") {
			$AddressName = $IpAddress -replace '/', '-'
		} else {
			$AddressName = $IpAddress + "-32"
		}
	}
	if (!$AddressName -or $AddressName -eq "") { # no ip address or name was passed
		if ($InternalDebug) {
			Write-Host "[.] Insufficient information in Pan-AddAddressGroupMember"
		}
		return $false
	}

	if (!(Pan-AddAddress -Name $AddressName -IpAddress $IpAddress -InternalDebug:$InternalDebug -SkipExistenceCheck:$SkipExistenceCheck)) {
		if ($InternalDebug) {
			Write-Host "[.] Couldn't create the address in Pan-AddAddressGroupMember"
		}
		return $false
	}
	
	$xpath="/config/shared/address-group/entry[@name=%27$AddressGroupName%27]"
	$elementValue="<static><member>$AddressName</member></static>"

	$url = $burl
	$url += "type=config&action=set&key="
	if (!$key -or $key -eq "") {
		$key = $Env:PanKey
	}
	if (!$key -or $key -eq "") {
		$key = Pan-GetKey -Quiet:$false
	}
	if (!$key -or $key -eq "") {
		Write-Host "[!] Couldn't get the API key in AddAddressGroupMember"
		return $false
	}
	$url += $key
	$url += "&xpath="
	$url += $xpath
	$url += "&element="
	$url += $elementValue
	if ($InternalDebug) {
		Write-Host "[.] Requesting $url"
	}
	
	$reg = "response.status.+[ ""']success[ ""']"

	$res = Invoke-WebRequest -UseBasicParsing $url

	if ($InternalDebug) {
		Write-Host "[.] Response: $res"
	}

	if ($res.content -notmatch $reg) {
		# problem
		if ($InternalDebug) {
			write-host "Failed at 548"
			Write-Host "URL: $url"
			write-host $res
		}
		return $false
	}
	return $true
}

function Pan-AddAddressGroupMembers {
	[CmdletBinding()]
	param (
		[string]
		$AddressGroupName="",

		[string[]]
		$IpAddresses,

		[string[]]
		$AddressNames,

		[string]
		$burl=$Env:PanBaseUrl,

		[string]
		$key=$Env:PanKey,

		[switch]
		$InternalDebug,

		[switch]
		$SkipExistenceCheck # useful when newly created address objects havent been committed yet
	)

	if (!$burl -or $burl.Length -lt 4) {
		Pan-SetBaseUrl -InternalDebug:$InternalDebug
		$burl = $Env:PanBaseUrl
		if (!$burl -or $burl.Length -lt 4) {
			throw "missing_PanBaseUrl"
		}
	}

	if (!$AddressNames -or $AddressNames.Count -lt 1) {
		$AddressNames = @()
		for ($i=0; $i -lt $IpAddresses.Count; $i++) {
			$ip = $IpAddresses[$i]
			$name = ""
			try {
				$name = $AddressNames[$i]
			} catch {
				if ($ip -match "/\d{1,2}$") {
					$name = $ip -replace '/', '-'
				} else {
					$name = $ip + "-32"
				}
			}
		}
	}

	if (!$AddressNames -or $AddressNames.Count -ne $IpAddresses.Count) {
		Write-Error "[!] Nothing to work with in AddAddressGroupMembers, or bad data somewhere"
		return $false
	}

	for ($i=0;$i -lt $AddressNames.Count; $i++) {
		$name = $AddressNames[$i]
		$ip = $IpAddresses[$i]
		if (Pan-AddAddressGroupMember -AddressGroupName $AddressGroupName -AddressName $name -IpAddress $ip -InternalDebug:$InternalDebug -SkipExistenceCheck:$SkipExistenceCheck) {
			if ($InternalDebug) {
				Write-Host "[.] Success: $name ($ip)"
			}
		} else {
			if ($InternalDebug) {
				Write-Host "[.] Failure: $name ($ip)"
			}
		}
	}
}

function Pan-GetDeviceGroups {
	[CmdletBinding()]
	param (
		[string]
		$burl=$Env:PanBaseUrl,

		[string]
		$key=$Env:PanKey,

		[switch]
		$InternalDebug
	)

	if (!$burl -or $burl.Length -lt 4) {
		Pan-SetBaseUrl -InternalDebug:$InternalDebug
		$burl = $Env:PanBaseUrl
		if (!$burl -or $burl.Length -lt 4) {
			throw "missing_PanBaseUrl"
		}
	}

	if (!$key -or ($key -eq "")) {
		$key = Pan-GetKey -Quiet:$false
	}
	if (!$key -or ($key -eq "")) {
		return $false
	}

	$url = $burl
	$xpath = "/config/readonly/devices/entry[@name='localhost.localdomain']/device-group"
	$url += "type=config&action=get&key="
	$url += $key
	$url += "&xpath="
	$url += $xpath
	if ($InternalDebug) {
		Write-Host "[.] Requesting $url"
	}
	$res = Invoke-WebRequest -UseBasicParsing $url
	
	$reg = "response.status.+[ ""']success[ ""']"

	$res = Invoke-WebRequest -UseBasicParsing $url
	if ($InternalDebug) {
		Write-Host "[.] Response: $res"
	}

	if ($res.content -notmatch $reg) {
		# problem
		if ($InternalDebug) {
			write-error "Failed at 450"
			write-error $res
		}
		return $false
	}

	$groups = $res.content.tostring() -replace "(?ms)(<response.*<device-group>\s*|</device-group>.*)", "" # multiline, include linebreaks in wildcard matches
	$groups = $groups -replace "(?ms)(<address-group>\s*(<entry [^>]+>\s*<id>\d+</id>\s*</entry>)*\s*</address-group>)", "" # sub address groups need to be removed
	$groups = $groups -replace "(?ms)(<entry name=`"|`">|^\s+|\s+$)", "" -split "`n"
	$groups = $groups |where {$_[0] -ne '<'}
	$groups = $groups -join ' ' # we're assuming there'll never be a space in a device-group name

	if ($groups.Length -gt 0) {
		$Env:PanDeviceGroups=$groups
		return $true
	}
	return $false
}

function Pan-Commit {
	[CmdletBinding()]
	param (
		[string]
		$Name="Pan-Ban",

		[string]
		$burl=$Env:PanBaseUrl,

		[string]
		$key=$Env:PanKey,

		[switch]
		$InternalDebug
	)

	if (!$burl -or $burl.Length -lt 4) {
		Pan-SetBaseUrl -InternalDebug:$InternalDebug
		$burl = $Env:PanBaseUrl
		if (!$burl -or $burl.Length -lt 4) {
			throw "missing_PanBaseUrl"
		}
	}

	if (!$key -or ($key -eq "")) {
		$key = Pan-GetKey -Quiet:$false
	}
	if (!$key -or ($key -eq "")) {
		return $false
	}

	$commit = "type=commit&key="
	$commit += $key
	$commit += "&cmd=<commit>"
	if ($Name -and $Name.Length -gt 0) {
		$UrlName = ""
		try {
			$UrlName = [System.Net.WebUtility]::UrlEncode($Name)
		} catch {
			try {
				Add-Type -AssemblyName System.Web
				$UrlName = [System.Web.HttpUtility]::UrlEncode($Name)
			} catch {
				$UrlName = ""
			}
		} finally {
			$commit += "<description>"
			$commit += "$UrlName"
			$commit += "</description>"
		}
	}
	$commit += "</commit>"
	
	$url = $burl
	$url += $commit
	if ($InternalDebug) {
		Write-Host "[.] Requesting $url"
	}
	$res = Invoke-WebRequest -UseBasicParsing $url
	
	$reg = "response.status.+[ ""']success[ ""']"

	$res = Invoke-WebRequest -UseBasicParsing $url
	if ($InternalDebug) {
		Write-Host "[.] Response: $res"
	}

	if ($res.content -notmatch $reg) {
		# problem
		if ($InternalDebug) {
			write-host "Failed at 831"
			write-host $res
		}
		return $false
	}

	return $true
}

function Pan-CommitAll {
	[CmdletBinding()]
	param (
		[string]
		$Name="Pan-Ban",

		[string]
		$burl=$Env:PanBaseUrl,

		[string]
		$key=$Env:PanKey,

		[switch]
		$InternalDebug
	)

	if (!$burl -or $burl.Length -lt 4) {
		Pan-SetBaseUrl -InternalDebug:$InternalDebug
		$burl = $Env:PanBaseUrl
		if (!$burl -or $burl.Length -lt 4) {
			throw "missing_PanBaseUrl"
		}
	}

	if (!$key -or ($key -eq "")) {
		$key = Pan-GetKey -Quiet:$false
	}
	if (!$key -or ($key -eq "")) {
		return $false
	}

	$commit = "type=commit&key="
	$commit += $key
	$commit += "&cmd=<commit-all><shared-policy>"
	if ($Name -and $Name.Length -gt 0) {
		$UrlName = ""
		try {
			$UrlName = [System.Net.WebUtility]::UrlEncode($Name)
		} catch {
			try {
				Add-Type -AssemblyName System.Web
				$UrlName = [System.Web.HttpUtility]::UrlEncode($Name)
			} catch {
				$UrlName = ""
			}
		} finally {
			$commit += "<description>"
			$commit += "$UrlName"
			$commit += "</description>"
		}
	}
	$commit += "</shared-policy></commit-all>"
	
	$url = $burl
	$url += $commit
	if ($InternalDebug) {
		Write-Host "[.] Requesting $url"
	}
	$res = Invoke-WebRequest -UseBasicParsing $url
	
	$reg = "response.status.+[ ""']success[ ""']"

	$res = Invoke-WebRequest -UseBasicParsing $url
	if ($InternalDebug) {
		Write-Host "[.] Response: $res"
	}

	if ($res.content -notmatch $reg) {
		# problem
		if ($InternalDebug) {
			write-host "Failed at 831"
			write-host $res
		}
		return $false
	}

	return $true
}

function Pan-CommitPush {
	[CmdletBinding()]
	param (
		[string]
		$Name="Pan-Ban",

		[string]
		$burl=$Env:PanBaseUrl,

		[string]
		$key=$Env:PanKey,

		[string]
		$devicegroups=$env:PanDeviceGroups,

		[switch]
		$InternalDebug
	)

	if (!$burl -or $burl.Length -lt 4) {
		Pan-SetBaseUrl -InternalDebug:$InternalDebug
		$burl = $Env:PanBaseUrl
		if (!$burl -or $burl.Length -lt 4) {
			throw "missing_PanBaseUrl"
		}
	}

	if (!$key -or ($key -eq "")) {
		$key = Pan-GetKey -Quiet:$false
	}
	if (!$key -or ($key -eq "")) {
		return $false
	}

	if (!$devicegroups -or $devicegroups.Length -lt 5) {
		if (!(Pan-GetDeviceGroups -InternalDebug:$InternalDebug)) {
			throw "no_device_groups"
		}
		$devicegroups=$env:PanDeviceGroups
	}
	if (!$devicegroups -or $devicegroups.Length -lt 5) {
		throw "no_device_groups"
	}
		
	$reg = "response.status.+[ ""']success[ ""']"

	foreach ($dgname in $devicegroups -split ' ') {

		$commit = "type=commit&action=all&key="
		$commit += $key
		$commit += "&cmd=<commit-all><shared-policy><device-group>"
		$commit += "<entry name='$dgname'/>"
		$commit += "</device-group>"
		if ($Name -and $Name.Length -gt 0) {
			$UrlName = ""
			try {
				$UrlName = [System.Net.WebUtility]::UrlEncode($Name)
			} catch {
				try {
					Add-Type -AssemblyName System.Web
					$UrlName = [System.Web.HttpUtility]::UrlEncode($Name)
				} catch {
					$UrlName = ""
				}
			} finally {
				$commit += "<description>"
				$commit += "$UrlName"
				$commit += "</description>"
			}
		}
		$commit += "</shared-policy></commit-all>"
		
		$url = $burl
		$url += $commit

		if ($InternalDebug) {
			Write-Host "[.] Requesting $url"
		}
		$res = Invoke-WebRequest -UseBasicParsing $url

		$res = Invoke-WebRequest -UseBasicParsing $url
		if ($InternalDebug) {
			Write-Host "[.] Response: $res"
		}

		if ($res.content -notmatch $reg) {
			# problem
			if ($InternalDebug) {
				write-host "Failed at 866 on dg $dgname"
				write-host $res
			}
		}
	}

	return $true
}

function Pan-GetAllJobs {
	[CmdletBinding()]
	param (
		[string]
		$burl=$Env:PanBaseUrl,

		[string]
		$key=$Env:PanKey,

		[string]
		$devicegroups=$env:PanDeviceGroups,

		[switch]
		$InternalDebug
	)

	if (!$burl -or $burl.Length -lt 4) {
		Pan-SetBaseUrl -InternalDebug:$InternalDebug
		$burl = $Env:PanBaseUrl
		if (!$burl -or $burl.Length -lt 4) {
			throw "missing_PanBaseUrl"
		}
	}

	if (!$key -or ($key -eq "")) {
		$key = Pan-GetKey -Quiet:$false
	}
	if (!$key -or ($key -eq "")) {
		return ""
	}

	$commit = "type=op&key="
	$commit += $key
	$commit += "&cmd=<show><jobs><all>"
	$commit += "</all>"
	$commit += "</jobs></show>"
	
	$url = $burl
	$url += $commit

	if ($InternalDebug) {
		Write-Host "[.] Requesting $url"
	}
	$res = Invoke-WebRequest -UseBasicParsing $url
	
	$reg = "response.status.+[ ""']success[ ""']"

	$res = Invoke-WebRequest -UseBasicParsing $url
	if ($InternalDebug) {
		Write-Host "[.] Response: $res"
	}

	if ($res.content -notmatch $reg) {
		# problem
		if ($InternalDebug) {
			write-host "Failed at 875"
			write-host $res
		}
		return ""
	}

	return $res.content.tostring()
}

function Pan-GetJob {
	[CmdletBinding()]
	param (
		[string]
		$User=$Env:PanUsername,

		[string]
		$JobType="commit",

		[string]
		$Description="",

		[string]
		$burl=$Env:PanBaseUrl,

		[string]
		$key=$Env:PanKey,

		[switch]
		$InternalDebug
	)

	if (!$burl -or $burl.Length -lt 4) {
		Pan-SetBaseUrl -InternalDebug:$InternalDebug
		$burl = $Env:PanBaseUrl
		if (!$burl -or $burl.Length -lt 4) {
			throw "missing_PanBaseUrl"
		}
	}

	try {
		[xml]$xdata = [xml](Pan-GetAllJobs -InternalDebug:$InternalDebug)
		$jobs = @($xdata.response.ChildNodes.job | where {$_.user -match "$User" -and $_.type -match "$JobType" -and $_.description -match "$Description"}) # should match the first job. empty description with match operator should be fine.
	
		return $jobs[0]
	} catch {
		throw "no_such_job"
	}

	return $false
}

function Pan-AwaitJob {
	[CmdletBinding()]
	param (
		[string]
		$Timeout=60, # secondsish

		[string]
		$User=$Env:PanUsername,

		[string]
		$JobType="commit",

		[string]
		$Description="",

		[string]
		$burl=$Env:PanBaseUrl,

		[string]
		$key=$Env:PanKey,

		[switch]
		$InternalDebug
	)

	if (!$burl -or $burl.Length -lt 4) {
		Pan-SetBaseUrl -InternalDebug:$InternalDebug
		$burl = $Env:PanBaseUrl
		if (!$burl -or $burl.Length -lt 4) {
			throw "missing_PanBaseUrl"
		}
	}

	if ($InternalDebug) {
		Write-Host "[.] Attempting to wait for job by user $User with type $JobType and description $Description"
	}
	while ($Timeout -gt 0) {
		try {
			$job = Pan-GetJob -User $User -JobType $JobType -Description $Description -InternalDebug:$InternalDebug
			$status = $job.status
			if ($status -eq "FIN") {
				return $true
			} elseif ($InternalDebug) {
				Write-Host "[.] Waiting on job to finish: $job"
			}
		} catch {
			if ($InternalDebug) {
				Write-Host "An Error was caught in Pan-AwaitJob: $_"
			}
			$job = ""
		}
		$Timeout=$Timeout-1
		sleep 1
	}
	throw "timeout_error"
}

function Pan-Ban {
	[CmdletBinding()]
	param (
		[Parameter(Position=0)]
		[string]
		$IpAddress="",

		[string[]]
		$IpAddresses,
		
		[string]
		$AddressBaseName="attack", # attack, c2, mal, scan all current values. No policy on this. 

		[string]
		$AddressGroupName="banned",
		
		[string]
		$AddressName="",
		
		[string[]]
		$AddressNames,

		[Int]
		$CommitTimeout = 60,

		[Int]
		$PushTimeout = 120,

		[string]
		$burl=$Env:PanBaseUrl,

		[string]
		$key=$Env:PanKey,

		[switch]
		$InternalDebug
	)

	try {
		$JobId = [guid]::NewGuid().ToString()
		$StartDate = Get-Date -Format "yyyyMMdd-HHmmss"

		Write-Host "[i] Job $JobId started on $StartDate"

		if (!$burl -or $burl.Length -lt 4) {
			Pan-SetBaseUrl -InternalDebug:$InternalDebug
			$burl = $Env:PanBaseUrl
			if (!$burl -or $burl.Length -lt 4) {
				throw "missing_PanBaseUrl"
			}
		}

		if (($IpAddresses -and $IpAddresses.Count -gt 0) -and ($IpAddress -and $IpAddress -ne "")) {
			Write-Error "Don't specify both IpAddress and IpAddresses in calling Pan-Ban"
			return $false
		}

		if (',' -in $IpAddress -and (!$IpAddresses -or $IpAddresses.Count -lt 1)) { # try to handle multiple comma delimited ips given at the command line
			$IpAddresses = ($IpAddress -replace ' ', '') -split ','
		}

		$validaddressnames=@()
		$validips=@()

		if ((!$IpAddresses -or $IpAddresses.Count -lt 1) -and $IpAddress -and $IpAddress -ne "") {
			if ($InternalDebug) {
				Write-Host "[.] Banning a single IP"
			}
			if ((!$AddressName -or $AddressName -eq "") -and ($IpAddress -and $IpAddress.Length -gt 6)) { # no address name, but have an ip
				if ($IpAddress -match "/\d{1,2}$") {
					$AddressName = $AddressBaseName + "-" + ($IpAddress -replace '/', '-')
				} else {
					$AddressName = $AddressBaseName + "-" + $IpAddress + "-32"
				}
			}
			if (!$AddressName -or $AddressName -eq "") { # no ip address or name was passed
				if ($InternalDebug) {
					Write-Host "[.] Insufficient information in Pan-Ban"
				}
				return $false
			}

			# We're assuming the IPs and the names are valid here. We'll let the AddAddressGroupMember function handle the object creation
			# TODO: add validation for ips and names here
			
			$validaddressnames += $AddressName
			$validips += $IpAddress

		} elseif ($IpAddresses -and $IpAddresses.Count -gt 0) {
			if ($InternalDebug) {
				Write-Host "[.] Banning multiple IPs"
			}
			for ($i = 0; $i -lt $IpAddresses.Count; $i++) {
				$ip = $IpAddresses[$i]
				try {
					$AddressName = $AddressNames[$i]
				} catch {
					$AddressName = ""
				}
				if ((!$AddressName -or $AddressName -eq "") -and ($ip -and $ip.Length -gt 6)) { # no address name, but have an ip
					if ($ip -match "/\d{1,2}$") {
						$AddressName = $AddressBaseName + "-" + ($ip -replace '/', '-')
					} else {
						$AddressName = $AddressBaseName + "-" + $ip + "-32"
					}
				}
				if (!$AddressName -or $AddressName -eq "" -or $AddressName -match "^-") { # no ip address or name was passed, or bad name produced
					if ($InternalDebug) {
						Write-Host "[.] Insufficient/Bad information in Pan-Ban"
					}
					return $false
				}

				# We're assuming the IPs and the names are valid here. We'll let the AddAddressGroupMember function handle the object creation
				# TODO: add validation for ips and names here

				$validaddressnames += $AddressName
				$validips += $ip
			}
		} else {
			Write-Error "I'm confused about what I'm supposed to do here"
			return $false
		}

		$name=""

		for ($i=0;$i -lt $validaddressnames.Count; $i++) {
			$name = $validaddressnames[$i]
			$ip = $validips[$i]
			if (!(Pan-AddAddressGroupMember -AddressName $name -IpAddress $ip -AddressGroupName $AddressGroupName -InternalDebug:$InternalDebug -SkipExistenceCheck:$SkipExistenceCheck)) {
				Write-Host "[!] Failed to ban $name ($ip)"
			} else {
				Write-Host "[-] Banned $name ($ip)"
			}
		}

		$CommitDescription = "Pan-Ban: $JobId"

		if (!(Pan-Commit -Name "$CommitDescription" -InternalDebug:$InternalDebug)) {
			throw "Unable to commit-all for job $JobId"
		}

		Write-Host "[i] Committing..."

		$timeout = $CommitTimeout

		if ($InternalDebug) {
			Write-Host "[.] Awaiting Job completion with $timeout second timeout"
		}

		$wait = $false

		try {
			$wait = Pan-AwaitJob -Description "$CommitDescription" -Timeout $timeout -InternalDebug:$InternalDebug
		} catch {
			if ($InternalDebug) {
				Write-Host "An Error was caught in while waiting for the last commit job: $_"
			}
			$wait = $false
		}

		if (!$wait) {
			Write-Host "[!] I was unable to await the last commit job!"
		}

		Write-Host "[i] Pushing..."

		if (!(Pan-CommitPush -Name "$CommitDescription" -InternalDebug:$InternalDebug)) {
			throw "Unable to commit-push"
		}

		$wait = $false

		$timeout = $PushTimeout

		Write-Host "[i] The push is underway. I will await this push for $timeout seconds(ish) unless you cancel this operation. You can safely cancel without losing progress."
		
		try {
			$wait = Pan-AwaitJob -Description "$CommitDescription" -InternalDebug:$InternalDebug
		} catch {
			if ($InternalDebug) {
				Write-Host "An Error was caught in while waiting for the last commit job: $_"
			}
			$wait = $false
		}

		if (!$wait) {
			Write-Host "[!] I was unable to await the last commit job!"
		}
	} finally {

		$EndDate = Get-Date -Format "yyyyMMdd-HHmmss"

		Write-Host "[i] Job $JobId Completed on $EndDate"
	}

	return $true
}

#Export-ModuleMember -Function Pan-Ban

